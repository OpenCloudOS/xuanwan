# 用户指导
## 部署 Master
当前玄湾OS 仅支持 Node 节点部署，部署前需要创建好 k8s 集群的 Master 节点。
本地部署可参考：[Kubernetes部署指南 - OpenCloudOS Documentation](https://docs.opencloudos.org/OCS/Virtualization_and_Containers_Guide/Kubernetes_UserGuide/)

## 部署 crd
创建 Node 节点前需要在 Master 节点部署自定义资源 nodesos 及 nodeinstance 的 crd，命令如下：
```
kubectl apply -f xuanwan.opencloudos.org_nodeos.yaml
kubectl apply -f xuanwan.opencloudos.org_nodeinstances.yaml
```

命令中使用的文件，可以从如下地址获取：

[https://gitee.com/OpenCloudOS/xuanwan/tree/master/os-update-operator/configs](https://gitee.com/OpenCloudOS/xuanwan/tree/master/os-update-operator/configs)

目录结构如下：
```bash
configs/
├── deploy
│   ├── admin-container.yaml
│   ├── nodeos-rollback.yaml
│   ├── nodeos-update.yaml
│   ├── operator
│   │   ├── up-manager.yaml
│   │   └── up-proxy.yaml
│   ├── rbac
│   │   ├── role.yaml
│   │   ├── role_binding.yaml
│   │   └── service_account.yaml
│   ├── xuanwan.opencloudos.org_nodeinstances.yaml
│   └── xuanwan.opencloudos.org_nodeos.yaml
└── systemd
    ├── up-executor.service
    └── up-proxy.service
```

玄湾OS的对接k8s双分区升级特性涉及到三个组件：up-executor、up-proxy、up-manager。
其中，up-executor 已通过rpm包的形式集成到 xbuildsys 中，构建镜像时会自动集成。Node 节点启动时，会通过 systemd 服务主动拉起 up-executor。
up-proxy 及 up-manager 均通过容器化部署，up-proxy 部署为 DaemonSet，up-manager 部署为 Deployment。

## 部署rbac权限控制规则
在 master 节点直接执行以下命令：
```
kubectl apply -f rbac/role.yaml
kubectl apply -f rbac/role_binding.yaml
kubectl apply -f rbac/service_account.yaml
```

rbac 用于控制 up-manager 及 up-proxy 对自定义资源 NodeOS、NodeInstance 的访问。

## 部署双分区升级的up-manager
up-manager 的 Dockerfile 参考如下：
```
FROM rust:1.81 AS builder

RUN apt-get update && apt-get install -y protobuf-compiler

WORKDIR /workspace

COPY . .

FROM builder AS manager-builder
RUN cargo build --release --package manager

FROM opencloudos/opencloudos:9.0 AS up-manager
COPY --from=manager-builder /workspace/target/release/up-manager /up-manager
ENTRYPOINT ["/up-manager"]
```

在master节点直接执行以下命令：
```
kubectl apply -f operator/up-manager.yaml
```
up-manager 组件将以 Pod 形式运行于 master 节点。

## 创建 Node
镜像中需要集成初始化服务（如 cloud-init 等），并配合特定云平台提供的 CVM 主机实例 metadata 机制完成节点初始化。xbuildsys 集成了 k8s-node-init.service 和 join-k8s.service 用于本地调测时对节点进行初始化。
节点启动时，服务从 /dev/sr0 设备中读取 ip 和 master 节点信息，虚拟机启动前需要提前生成 iso，并挂载到虚拟机。
iso 中包含 config 和 meta-data 两个文件。其中，config 来自 master 节点 /etc/kubernetes/admin.conf。

meta-data 文件需要手动生成，格式参考如下：
```
NODE_HOSTNAME=node-1120150329
NETDEV=eth0
NODE_IP=192.168.122.21
PREFIX=24
GATEWAY=192.168.122.1
MASTER_IP=192.168.122.138
MASTER_PORT=6443
MASTER_TOKEN=ibdnxh.yc360ryskzdyp45r
CA_HASH=sha256:fbe7e97e7c67ab9b6abb6b040ab7f473dc83f1228511206cb26295ca2d62f8aa

NODE_HOSTNAME：节点主机名
NETDEV：节点使用的网口
NODE_IP：节点ip
PREFIX：节点网络 prefix
GATEWAY：节点网关
MASTER_IP：master 节点 ip
MASTER_PORT：master apiserver 监听的端口号，默认为 6443
MASTER_TOKEN：master 节点 token
CA_HASH：master 节点 sha256
```

最后两项可以通过在 master 节点执行如下命令获取：
```
kubeadm token create --print-join-command
```

iso 可以通过如下命令生成：
```
genisoimage -input-charset utf-8 -output $isoname.iso -volid cidata -joliet -rock meta-data config
```

镜像默认大小为5G，如需扩容，可参考如下命令：
```
qemu-img resize oc9-6.6.6-2401.0.1-202411201906.qcow2 +10G
```

## 升级 Node OS
升级前需要将xbuildsys构建系统生成的 `产品名+kernel版本号+构建时间戳-boot.ext4.lz4` 文件解压为`产品名+kernel版本号+构建时间戳-boot.ext4`后，上传到 http 服务器。

### Node节点打标签
将待升级的Node节点打上升级标签，执行如下命令：
```
kubectl label nodes <node name> xuanwan.opencloudos.org/updater-interface-version="1.0.0"
```
依从将待升级的Node节点打上标签，下面的up-proxy组件将以DaemonSet的方式部署到这些Node节点上。

### Node节点部署up-proxy组件
up-proxy的Dockerfile参考如下：
```
FROM rust:1.81 AS builder

RUN apt-get update && apt-get install -y protobuf-compiler

WORKDIR /workspace

COPY . .

FROM builder AS proxy-builder
RUN cargo build --release --package proxy

FROM opencloudos/opencloudos:9.0 AS up-proxy
COPY --from=proxy-builder /workspace/target/release/up-proxy /up-proxy
ENTRYPOINT ["/up-proxy"]
```

部署up-proxy执行如下命令：
```
kubectl apply -f operator/up-proxy.yaml
```
至此，可以在master节点上面看到up-proxy的Pod，这些Pod实际运行于各个选中的Node:
```
kubectl get daemonsets.apps -A
NAMESPACE      NAME              DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system    up-proxy          2         2         2       2            2           <none>                   112m
```

### 编写升级配置文件
升级前需要编写升级 yaml 文件（nodeos-update.yaml），参考如下：
```yaml
apiVersion: xuanwan.opencloudos.org/v1
kind: NodeOS
metadata:
  name: node-os
  namespace: kube-system
spec:
  opstype: update
  osversion: "XuanwanOS 1.0 241118.0940"
  concurrent: 2
  downloadurl: "http://download-url/oc9-6.6.6-2401.0.1-202411180939-boot.ext4"
  checksum: null
  cacert: null
  clientcert: null
  clientkey: null
```
当前升级只需要设置 opstype、osversion、concurrent 和 downloadurl 四个字段。

- `opstype`：设置要执行的操作类型，当前支持 update、rollback 和 check 三种操作，执行升级流程设置为 update。

- `osversion`：设置要升级到的版本号，保存在 rootfs 中 /etc/xuanwan_version 文件中

- `concurrent`：设置节点升级的并发数量，示例为2；

- `downloadurl`：设置 rootfs 镜像的 http 地址，即前文 `产品名+kernel版本号+构建时间戳-boot.ext4` 的下载地址；

### 升级操作
执行升级操作命令如下：
```
kubectl apply -f nodeos-update.yaml
```
### 查看升级状态
可以通过如下命令观察升级状态变化：
```
kubectl get nodeinstances.xuanwan.opencloudos.org --watch
```

输出参考如下：
```
NAME        HOSTNAME    STATUS   VERSION                     UPVERSION
oc9-node4   oc9-node4   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node3   oc9-node3   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node7   oc9-node7   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node5   oc9-node5   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node6   oc9-node6   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node4   oc9-node4   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
oc9-node3   oc9-node3   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
oc9-node4   oc9-node4   idle       XuanwanOS 1.0 240730.1602   XuanwanOS 1.0 240730.1602
oc9-node7   oc9-node7   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
```
`VERSION` 和 `UPVERSION` 版本一致，均为升级前版本号，同时节点状态为 `idle`，则说明节点处于待升级状态。

`VERSION` 和 `UPVERSION` 版本一致，均为升级后版本号，同时节点状态为 `idle`，则说明节点已升级完成。

其他状态表明节点正在升级中。

### 节点升级失败处理
如果节点升级失败，节点的NodeInstance资源的status字段将被标记为`updatefailed`:
```
kubectl get nodeinstances.xuanwan.opencloudos.org -n kube-system
NAME        HOSTNAME       STATUS           VERSION                     UPVERSION                   UPCOUNT
oc9-node4   oc9-node4      updatefailed     XuanwanOS 1.0 240731.1658   XuanwanOS 1.0 240731.1658   3
oc9-node5   oc9-node5      updatefailed     XuanwanOS 1.0 240731.1658   XuanwanOS 1.0 240731.1658   3
```

此时可以先删除节点的升级标签:
```
kubectl label nodes <node name> xuanwan.opencloudos.org/updater-interface-version-
```

然后执行：
```
kubectl apply -f operator/up-proxy.yaml
```

这样升级失败的节点将暂时移出升级系统，由管理者介入处理。

## 回滚 Node OS
**回滚 Node OS 需要节点至少已成功执行过一次升级任务。**

### 回滚准备
回滚前需要编写回滚 yaml 文件（nodeos-rollback.yaml），参考如下：
```yaml
apiVersion: xuanwan.opencloudos.org/v1
kind: NodeOS
metadata:
  name: node-os
  namespace: kube-system
spec:
  opstype: rollback
  osversion: <version>
  concurrent: 5
  downloadurl: null
  checksum: null
  cacert: null
  clientcert: null
  clientkey: null
```
当前回滚只需要设置 opstype、osversion 和 concurrent 三个字段。

- `opstype`：设置要执行的操作类型，当前支持 update、rollback 和 check 三种操作，执行回滚流程设置为 rollback

- `osversion`：设置升级前的版本号

- `concurrent`：设置节点升级的并发数量

### 回滚操作
执行回滚操作命令如下：
```
kubectl apply -f nodeos-rollback.yaml
```

### 查看回滚状态
可以通过如下命令观察回滚状态变化：
```
kubectl get nodeinstances.xuanwan.opencloudos.org --watch
```
`VERSION` 和 `UPVERSION` 版本一致，均为升级前版本号，同时节点状态为 `idle`，则说明节点回滚成功。
`VERSION` 和 `UPVERSION` 版本一致，均为升级后版本号，同时节点状态为 idle，则说明节点处于待回滚状态。
其他状态表明节点正在回滚中。

## 启动运维容器
玄湾OS 未提供 sshd 服务，如果需要登录节点，可以通过运维容器登录。

### 构建运维容器镜像
dockerfile 参考如下：

```dockerfile
FROM opencloudos/opencloudos:9.0
  
USER root
WORKDIR /root

RUN dnf install openssh openssh-server iputils net-tools passwd procps-ng iproute -y && \
        dnf clean all

RUN /usr/libexec/openssh/sshd-keygen rsa && \
        /usr/libexec/openssh/sshd-keygen ecdsa && \
        /usr/libexec/openssh/sshd-keygen ed25519 && \
        echo "PermitRootLogin yes" >> /etc/ssh/sshd_config  && \
        echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config  && \
        echo "Port 2222" >> /etc/ssh/sshd_config  && \
        yes "Xuanwan@2024" | passwd root

EXPOSE 2222

ENTRYPOINT ["/usr/sbin/sshd", "-D"]

```
opencloudos9-minimal 镜像可从如下地址下载并导入：

[https://mirrors.tencent.com/opencloudos/9/images/x86_64/OpenCloudOS-Container-Minimal-9.2-20240617.0.x86_64.tar.xz](https://mirrors.tencent.com/opencloudos/9/images/x86_64/OpenCloudOS-Container-Minimal-9.2-20240617.0.x86_64.tar.xz)

可以根据需要自定义运维容器镜像。

### 启动准备
编写运维容器 yaml 文件（admin-container.yaml），参考如下：
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: admin-pod
spec:
  hostPID: true
  hostNetwork: true
  containers:
  - name: admin-container
    image: <admincontainer的容器镜像地址>
    imagePullPolicy: IfNotPresent
    securityContext:
      privileged: true
  nodeSelector:
    kubernetes.io/hostname: node1
```

关键参数说明如下：

`hostPID` 设置为 true，运维容器与 Node 共享 pid 命名空间，容器内可以查看节点上的进程。

`hostNetwork` 设置为 true，运维容器与 Node 共享网络命名空间，可以直接通过 node 节点 ip 访问运维容器

`image`：设置运维容器镜像地址, 如 "192.168.x.x:5000/admincontainer:1.0"

`privileged` 设置为 true，将运维容器设置为特权容器

`nodeSelector` 需要根据实际情况设置节点的选择器，用户选出需要登录的节点

### 启动运维容器
启动运维容器命令如下：

```
kubectl apply -f admin-container.yaml
```

### 登录运维容器
可以通过 ssh 命令直接登录运维容器
默认密码: Xuanwan@2024

```
ssh -p 2222 root@nodeip
```
### 进入主机环境
运维容器内执行如下命令进入主机 1 号进程命名空间，进而进入主机环境：
```
nsenter -t 1 -a
```
