# 编译准备
编译前需执行以下动作，满足环境要求：
```
# 安装rust
dnf install rust cargo
# 安装 protoc
dnf install protobuf-compiler

```

# 编译
up-manager、up-proxy容器化部署：
```
docker build --target up-manager -t up-manager .
docker build --target up-proxy -t up-proxy .
```
up-executor 获取；
```
cargo build --release 
mv target/release/up-executor ./
```

# 测试
cargo test
cargo test -- proxy

# 文档 
cargo doc

# 代码质量
cargo fmt --check
cargo clippy

