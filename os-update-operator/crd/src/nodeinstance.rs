use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Deserialize, Serialize, Debug, Clone, JsonSchema)]
// #[kube(group = "xuanwan.opencloudos.org", version = "v1", kind = "NodeInstance", namespaced)]
#[kube(
    group = "xuanwan.opencloudos.org",
    version = "v1",
    kind = "NodeInstance",
    plural = "nodeinstances",
    namespaced
)]
#[kube(status = "NodeInstanceStatus")]
pub struct NodeInstanceSpec {
    pub hostname: String,
    pub status: String,
    pub upversion: String,
    pub version: String,
    pub upcount: i32,
}

#[derive(Deserialize, Serialize, Debug, Clone, JsonSchema)]
pub struct NodeInstanceStatus {
    pub phase: Option<String>,
}
