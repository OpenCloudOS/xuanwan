use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Deserialize, Serialize, Debug, Clone, JsonSchema)]
// #[kube(group = "xuanwan.opencloudos.org", version = "v1", kind = "NodeOS", namespaced)]
#[kube(
    group = "xuanwan.opencloudos.org",
    version = "v1",
    kind = "NodeOS",
    plural = "nodeoss",
    namespaced
)]
#[kube(status = "NodeOSStatus")]
pub struct NodeOSSpec {
    pub opstype: String,
    pub osversion: String,
    pub concurrent: i32,
    pub downloadurl: String,
    pub checksum: String,
    pub cacert: String,
    pub clientcert: String,
    pub clientkey: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, JsonSchema)]
pub struct NodeOSStatus {
    pub phase: Option<String>,
}
