use std::process::{Command, Stdio};
use std::io;
use tracing::{info, error};
// use tokio::time::{sleep, Duration};

pub struct UpdateOps {
    pub version: String,
    pub url: String,
    pub checksum: String,
    pub cacert: String,
    pub clientcert: String,
    pub clientkey: String,
}

pub async fn run_update(ops: UpdateOps) -> io::Result<()> {
    info!("Running update with version: {}", ops.version);
    
    // // Sleep for 5 seconds
    // sleep(Duration::from_secs(15)).await;
    
    // Download the OS image
    run_cmd("curl", &["-o", "/persist/data/xuanwan.ext4", &ops.url])?;

    // Verify checksum
    // TODO: Add checksum verification logic

    // Find the next partition
    let part = find_next_part()?;

    // Write OS image
    run_cmd("dd", &["if=/persist/data/xuanwan.ext4", &format!("of={}", part), "bs=4M"])?;

    // Update system
    run_cmd("/usr/sbin/update.sh", &[])?;

    // Set flag of current partition
    run_cmd("system-update-set", &["setflg", "/dev/vda", "1"])?;

    // Switch to next partition (switch will reset flag of next partition)
    if part == "/dev/vda2" {
        run_cmd("system-update-set", &["switch", "/dev/vda", "a"])?;
    } else {
        run_cmd("system-update-set", &["switch", "/dev/vda", "b"])?;
    }

    // Sync file system buffers
    run_cmd("sync", &[])?;
    // Reboot
    run_cmd("reboot", &[])?;

    Ok(())
}

pub async fn run_rollback() -> io::Result<()> {
    info!("Running rollback");
    
    // // Sleep for 5 seconds
    // sleep(Duration::from_secs(15)).await;

    // Find the next partition
    let part = find_next_part()?;

    // Check whether next part flag is valid
    let res = if part == "/dev/vda2" {
        run_cmd("system-update-get", &["check_flg", "/dev/vda", "a"])?
    } else {
        run_cmd("system-update-get", &["check_flg", "/dev/vda", "b"])?
    };
    info!("check result of next part flag is:{}",res);

    // If res is not 0, it means the next partition is invalid/empty
    if res.trim() != "0" {
        error!("Next partition {} is empty, cannot rollback to it", part);
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("Next partition is invalid, stop rollback"),
        ));
    }

    // Set flag of current partition
    run_cmd("system-update-set", &["setflg", "/dev/vda", "1"])?;

    // Switch to next partition (switch will Reset flag of next partition)
    if part == "/dev/vda2" {
        run_cmd("system-update-set", &["switch", "/dev/vda", "a"])?;
    } else {
        run_cmd("system-update-set", &["switch", "/dev/vda", "b"])?;
    }

    // Sync file system buffers
    run_cmd("sync", &[])?;
    // Reboot
    run_cmd("reboot", &[])?;

    Ok(())
}

fn run_cmd(command: &str, args: &[&str]) -> io::Result<String> {
    info!("Running command: {} {:?}", command, args);
    let output = Command::new(command)
        .args(args)
        .stderr(Stdio::inherit())
        .output()?;

    if !output.status.success() {
        error!("Command {} failed with output: {:?}", command, output);
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("Command {} failed with output: {:?}", command, output),
        ));
    }

    let stdout = String::from_utf8_lossy(&output.stdout).trim().to_string();
    Ok(stdout)
}

fn find_next_part() -> io::Result<String> {
    let output = Command::new("lsblk")
        .args(["-no", "MOUNTPOINT", "/dev/vda2"])
        .output()?;

    let root = String::from_utf8_lossy(&output.stdout).trim().to_string();
    if root == "/" {
        Ok("/dev/vda3".to_string())
    } else {
        Ok("/dev/vda2".to_string())
    }
}
