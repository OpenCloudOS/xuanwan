mod server;
mod executor;

use tracing_subscriber::FmtSubscriber;
use tracing::{info, error, Level};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;

    info!("Starting gRPC server...");
    match server::run_server().await {
        Ok(_) => info!("gRPC server started successfully"),
        Err(e) => {
            error!("Failed to start gRPC server: {}", e);
            return Err(e);
        }
    }

    Ok(())
}