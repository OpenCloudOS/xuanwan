pub mod proto {
    tonic::include_proto!("executor");
    // reflection API
    pub const FILE_DESCRIPTOR_SET: &[u8] = tonic::include_file_descriptor_set!("proto_descriptor");
}

use std::path::Path;
use std::fs;
use std::os::unix::fs::PermissionsExt;
use tokio::net::UnixListener;
use tonic::{transport::Server, Request, Response, Status};
use tonic_reflection::server::Builder;
use tokio::sync::Mutex;
use tokio_stream::wrappers::UnixListenerStream;
use tracing::{info, warn, error};

use proto::{UpdateRequest, UpdateResponse, RollbackRequest, RollbackResponse};
use proto::xuanwan_agent_server::{XuanwanAgent, XuanwanAgentServer};
use crate::executor::{run_update, run_rollback, UpdateOps};

const SOCK_ADDR: &str = "/run/os-executor/os-executor.sock";
const VERSION_FILE: &str = "/etc/xuanwan_version";

#[derive(Debug)]
pub struct MyXuanwanAgent {
    xwmutex: Mutex<()>,
}

#[tonic::async_trait]
impl XuanwanAgent for MyXuanwanAgent {
    async fn update(
        &self,
        request: Request<UpdateRequest>,
    ) -> Result<Response<UpdateResponse>, Status> {
        info!("Ready to update os!");

        // lock() is asynchronous and waits until the lock is available.
        // let _guard = self.xwmutex.lock().await;
        // try_lock() is blocking and immediately returns the Result of taking the lock.
        let lock = self.xwmutex.try_lock();
        if lock.is_err() {
            error!("Another rollback/update operation is in progress");
            return Ok(Response::new(UpdateResponse { err: 2 }));
        }
        
        let req = request.into_inner();
        let ops = UpdateOps {
            version: req.version.clone(),
            url: req.url,
            checksum: req.checksum,
            cacert: req.cacert,
            clientcert: req.clientcert,
            clientkey: req.clientkey,
        };

        // check current version
        let current_version = tokio::fs::read_to_string(VERSION_FILE)
            .await
            .map_err(|e| {
                error!("Failed to read version file: {}", e);
                Status::internal("Failed to read version file!")
            })?
            .trim()
            .to_string();

        if current_version == req.version {
            warn!("Version is already up-to-date, stop run_update!");
            return Ok(Response::new(UpdateResponse { err: 0 }));
        }

        match run_update(ops).await {
            Ok(_) => Ok(Response::new(UpdateResponse { err: 0 })),
            Err(e) => {
                error!("Update failed: {}", e);
                Ok(Response::new(UpdateResponse { err: 1 }))
            }
        }
    }

    async fn rollback(
        &self,
        _request: Request<RollbackRequest>,
    ) -> Result<Response<RollbackResponse>, Status> {
        info!("Ready to rollback os!");

        // lock() is asynchronous and waits until the lock is available.
        // let _guard = self.xwmutex.lock().await;
        // try_lock() is blocking and immediately returns the Result of taking the lock.
        let lock = self.xwmutex.try_lock();
        if lock.is_err() {
            error!("Another rollback/update operation is in progress");
            return Ok(Response::new(RollbackResponse { err: 2 }));
        }

        match run_rollback().await {
            Ok(_) => Ok(Response::new(RollbackResponse { err: 0 })),
            Err(e) => {
                error!("Rollback failed: {}", e);
                Ok(Response::new(RollbackResponse { err: 1 }))
            }
        }
    }
}

pub async fn run_server() -> Result<(), Box<dyn std::error::Error>> {
    // let addr = "127.0.0.1:50051".parse()?;    
    let sock_path = Path::new(SOCK_ADDR);
    let sock_dir = sock_path.parent().ok_or("Failed to extract directory from SOCK_ADDR")?;

    // check and create sock_dir
    if !sock_dir.exists() {
        info!("Attempting to create dir: {}", sock_dir.display());
        fs::create_dir_all(sock_dir)?;
        // set directory permissions to 755
        // PermissionsExt trait for fs::Permissions::from_mode
        let permissions = fs::Permissions::from_mode(0o755);
        fs::set_permissions(sock_dir, permissions)?;
    }

    // remove old socket file
    if sock_path.exists() {
        fs::remove_file(sock_path)?;
    }

    let listener = UnixListener::bind(SOCK_ADDR)?;
    let incoming = UnixListenerStream::new(listener);
    
    let svc = MyXuanwanAgent {
        xwmutex: Mutex::new(()),
    };

    // enable reflection API for grpcurl test
    let reflection_service = Builder::configure()
        .register_encoded_file_descriptor_set(proto::FILE_DESCRIPTOR_SET)
        .build()
        .unwrap();
    info!("Enable reflection for grpcurl test");

    Server::builder()
        .add_service(reflection_service)
        .add_service(XuanwanAgentServer::new(svc))
        .serve_with_incoming(incoming)
        .await?;

    Ok(())
}