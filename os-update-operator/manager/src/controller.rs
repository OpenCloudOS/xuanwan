use kube::api::{DeleteParams, Patch, PatchParams};
use kube::runtime::controller::Action;
use kube::runtime::reflector::Store;
use kube::{Api, Client, ResourceExt};
use k8s_openapi::api::core::v1::Node;
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;
use tracing::{debug, error, info, warn};

use crd::nodeinstance::NodeInstance;
use crd::nodeos::NodeOS;

const REQUEUE_ERROR: Duration = Duration::from_secs(2);
const REQUEUE_TIME: Duration = Duration::from_secs(3);
// const REQUEUE_STABLE: Duration = Duration::from_secs(10);
pub const STATUS_IDLE: &str = "idle";
pub const STATUS_UPDATING: &str = "updating";
// const STATUS_ROLLBACK: &str = "rollbacking";
pub const STATUS_ROLLBACK: &str = "rollback";
pub const LABEL_UPDATE_INTERFACE_NAME: &str = "xuanwan.opencloudos.org/updater-interface-version";

/// Shared context for the controller, containing the Kubernetes client and the nodeinstance store.
#[derive(Clone)]
pub struct Data {
    pub client: Client,
    pub nodeinstance_store: Store<NodeInstance>,
    pub node_store: Store<Node>,
}

impl Data {
    /// Constructs a new instance of ContextData.
    ///
    /// # Arguments:
    /// - `client`: A Kubernetes client to make Kubernetes REST API requests with. Resources
    ///   will be created and deleted with this client.
    pub fn new(
        client: Client,
        nodeinstance_store: Store<NodeInstance>,
        node_store: Store<Node>,
    ) -> Self {
        Data {
            client,
            nodeinstance_store,
            node_store,
        }
    }
}

/// Custom error type for the controller.
#[derive(Debug, Error)]
pub enum Error {
    #[error("Kubernetes reported error: {0}")]
    KubeError(#[from] kube::Error),
    #[error("Namespace not found for NodeOS {0}")]
    NamespaceNotFound(String),
    #[error("Failed to count nodes: {0}")]
    CountNodesError(String),
    #[error("Failed to set value: {0}")]
    NodeValueError(String),
}

/// Reconcile function for the NodeOS resource.
///
/// This function is called by the controller whenever a NodeOS resource is created, updated, or deleted.
///
/// # Arguments:
/// - `nodeos`: The NodeOS resource being reconciled.
/// - `ctx`: The shared context for the controller, containing the Kubernetes client.
///
/// # Returns:
/// - `Ok(Action)`: The action to take after reconciliation, such as requeueing the resource.
/// - `Err(Error)`: An error occurred during reconciliation.
pub async fn reconcile(nodeos: Arc<NodeOS>, ctx: Arc<Data>) -> Result<Action, Error> {
    debug!("start reconcile");
    debug!("Reconciling NodeOS: {:?}", nodeos);
    let client = &ctx.client;
    let namespace = match nodeos.namespace() {
        Some(ns) => ns,
        None => {
            let nodeos_name = nodeos.name_any();
            error!("Namespace not found for NodeOS {}", nodeos_name);
            return Err(Error::NamespaceNotFound(nodeos_name));
        }
    };
    let nodeinstance_api: Api<NodeInstance> = Api::namespaced(client.clone(), &namespace);

    // debug!("My nodeinstance_api: {:?}!", nodeinstance_api);
    // debug!("My nodeinstance_store: {:?}!", ctx.nodeinstance_store);

    // count idle/updating/rollback nodes using the cached store
    let sum_idle_node = count_idle_node(&ctx.nodeinstance_store, &namespace);
    let sum_updating_node = count_updating_node(&ctx.nodeinstance_store, &namespace);
    let sum_rollback_node = count_rollback_node(&ctx.nodeinstance_store, &namespace);
    debug!("Print sum_idle_node:{}, sum_updating_node:{}, sum_rollback_node:{}", sum_idle_node, sum_updating_node, sum_rollback_node);

    if sum_idle_node == 0 {
        info!("No idle nodes available. Waiting...");
        // No idle nodes, good opportunity for cleanup invalid nodeinstances! So we do not return requeue.
        // return Ok(Action::requeue(REQUEUE_TIME));
    }

    if sum_updating_node > 0 && sum_rollback_node > 0 {
        error!("Both updating and rollback nodes present. Exiting...");
        error!("updating node sum:{}, rollback node sum:{}", sum_updating_node, sum_rollback_node);
        std::process::exit(1);
    }

    let concurrent = nodeos.spec.concurrent;
    if concurrent < 1 {
        warn!("Maximum number of parallel processing is:{}, please check!", concurrent);
        return Err(Error::NodeValueError("concurrent of NodeOS".to_string()));
    }

    debug!("My nodeos:{}, concurrent:{}!", nodeos.spec.opstype, concurrent);
    match nodeos.spec.opstype.as_str() {
        "update" => {
            update_node_instance(
                &nodeinstance_api,
                &ctx.nodeinstance_store,
                &nodeos.spec.osversion,
                concurrent - sum_updating_node,
                &namespace,
            )
            .await?
        }
        "rollback" => {
            rollback_node_instance(
                &nodeinstance_api,
                &ctx.nodeinstance_store,
                &nodeos.spec.osversion,
                concurrent - sum_rollback_node,
                &namespace,
            )
            .await?
        }
        "check" => info!("TODO: check!"),
        _ => info!("No operation needed"),
    }

    // Perform cleanup of NodeInstance resources
    cleanup_nodeinstances(
        &nodeinstance_api,
        &ctx.nodeinstance_store,
        &ctx.node_store,
        &namespace,
    )
    .await?;

    // Just for fault-tolerant
    // we have set the trigger conditions for reconcile precisely 
    Ok(Action::requeue(REQUEUE_TIME))
}

/// Counts the number of idle NodeInstance resources.
///
/// # Arguments:
/// - `api`: The Kubernetes API client for NodeInstance resources.
///
/// # Returns:
/// - `Ok(i32)`: The number of idle NodeInstance resources.
/// - `Err(Error)`: An error occurred while counting the nodes.
fn count_idle_node(nodeinstance_store: &Store<NodeInstance>, namespace: &str) -> i32 {
    let nodes = nodeinstance_store.state();
    let count = nodes
        .iter()
        .filter(|node| {
            node.metadata.namespace.as_deref() == Some(namespace)
                && node.spec.status == STATUS_IDLE
        })
        .count();
    count as i32
}

fn count_updating_node(nodeinstance_store: &Store<NodeInstance>, namespace: &str) -> i32 {
    let nodes = nodeinstance_store.state();
    let count = nodes
        .iter()
        .filter(|node| {
            node.metadata.namespace.as_deref() == Some(namespace)
                && node.spec.status == STATUS_UPDATING
        })
        .count();
    count as i32
}

fn count_rollback_node(nodeinstance_store: &Store<NodeInstance>, namespace: &str) -> i32 {
    let nodes = nodeinstance_store.state();
    let count = nodes
        .iter()
        .filter(|node| {
            node.metadata.namespace.as_deref() == Some(namespace)
                && node.spec.status == STATUS_ROLLBACK
        })
        .count();
    count as i32
}

/// Updates the NodeInstance resources to the desired version.
///
/// # Arguments:
/// - `api`: The Kubernetes API client for NodeInstance resources.
/// - `desired_version`: The desired OS version.
/// - `handle_num`: The maximum number of nodes to handle concurrently.
///
/// # Returns:
/// - `Result<(), Error>`: Ok if successful, or an error.
async fn update_node_instance(
    api: &Api<NodeInstance>,
    nodeinstance_store: &Store<NodeInstance>,
    desired_version: &str,
    handle_num: i32,
    namespace: &str,
) -> Result<(), Error> {
    let nodes = nodeinstance_store.state();
    let mut count = 0;

    for node in nodes.iter().filter(|node| {
        node.metadata.namespace.as_deref() == Some(namespace)
            && node.spec.status == STATUS_IDLE
    }) {
        if count >= handle_num {
            break;
        }

        debug!("Update node name:{}, current version:{}, next version:{}!", node.name_any(), node.spec.upversion, desired_version);
        debug!("Update node max handle num:{}, before handling its count is:{}!", handle_num, count);
        if node.spec.upversion != desired_version {
            let patch_node = serde_json::json!({
                "spec": {
                    "upversion": desired_version,
                    "status": STATUS_UPDATING,
                    "upcount": 0
                }
            });

            // kube::Api::patch is faster than kube::Api::replace, which is a full replacement
            // node.name_any is unique and is used to identify the object
            match api
                .patch(
                    &node.name_any(),
                    &PatchParams::default(),
                    &Patch::Merge(&patch_node),
                )
                .await
            {
                Ok(_) => {
                    count += 1;
                    info!("Update node name:{} to version:{} successfully!", node.name_any(), node.spec.upversion);
                    info!("Update node max handle num:{}, after handling its count is:{}!", handle_num, count);
                }
                Err(e) => {
                    error!("Failed to update NodeInstance {}: {:?}", node.name_any(), e);
                    return Err(Error::KubeError(e));
                }
            }
        }
    }
    
    Ok(())
}

/// Rolls back the NodeInstance resources to the desired version.
///
/// # Arguments:
/// - `api`: The Kubernetes API client for NodeInstance resources.
/// - `desired_version`: The desired OS version.
/// - `handle_num`: The maximum number of nodes to handle concurrently.
///
/// # Returns:
/// - `Result<(), Error>`: Ok if successful, or an error.
async fn rollback_node_instance(
    api: &Api<NodeInstance>,
    nodeinstance_store: &Store<NodeInstance>,
    desired_version: &str,
    handle_num: i32,
    namespace: &str,
) -> Result<(), Error> {
    let nodes = nodeinstance_store.state();
    let mut count = 0;

    for node in nodes.iter().filter(|node| {
        node.metadata.namespace.as_deref() == Some(namespace)
            && node.spec.status == STATUS_IDLE
    }) {
        if count >= handle_num {
            break;
        }

        debug!("Rollback node name:{}, current version:{}, next version:{}!", node.name_any(), node.spec.upversion, desired_version);
        debug!("Rollback node max handle num:{}, before handling its count is:{}!", handle_num, count);
        if node.spec.upversion != desired_version {
            let patch_node = serde_json::json!({
                "spec": {
                    "upversion": desired_version,
                    "status": STATUS_ROLLBACK,
                    "upcount": 0
                }
            });

            // kube::Api::patch is faster than kube::Api::replace, which is a full replacement
            // node.name_any is unique and is used to identify the object
            match api
                .patch(
                    &node.name_any(),
                    &PatchParams::default(),
                    &Patch::Merge(&patch_node),
                )
                .await
            {
                Ok(_) => {
                    count += 1;
                    info!("Rollback node name:{} to version:{} successfully!", node.name_any(), desired_version);
                    info!("Rollback node max handle num:{}, after handling its count is:{}!", handle_num, count);
                }
                Err(e) => {
                    error!("Failed to rollback NodeInstance {}: {:?}", node.name_any(), e);
                    return Err(Error::KubeError(e));
                }
            }
        }
    }
    
    Ok(())
}

async fn cleanup_nodeinstances(
    nodeinstance_api: &Api<NodeInstance>,
    nodeinstance_store: &Store<NodeInstance>,
    node_store: &Store<Node>,
    namespace: &str,
) -> Result<(), Error> {
    let nodeinstances = nodeinstance_store.state();

    for nodeinstance in nodeinstances {
        debug!("cleanup ni name:{:?}, status:{}!", nodeinstance.metadata.name.clone().unwrap_or_default(), nodeinstance.spec.status);
        if nodeinstance.metadata.namespace.as_deref() != Some(namespace) {
            continue; // Skip NodeInstances not in the target namespace
        }
        // The number of non-idle nodes <= concurrent, avoiding processing many nodes each time.
        // It will clean invalid NodeInstance only after update/rollback.
        // If you need to clean an invalid NodeInstance immediately, comment this if statement.
        if nodeinstance.spec.status == STATUS_IDLE {
            continue; // Skip processing if status is STATUS_IDLE
        }

        let nodeinstance_name = nodeinstance.metadata.name.clone().unwrap_or_default();

        // Create an ObjectRef for the Node
        let node_ref = kube::runtime::reflector::ObjectRef::new(&nodeinstance_name);

        // Try to get the Node from the store
        // node_store is a hash map, more efficient than node_store.state(Vec)
        if let Some(node) = node_store.get(&node_ref) {
            // Node exists, check if it has the required label
            let empty_labels = std::collections::BTreeMap::new();
            let labels = node.metadata.labels.as_ref().unwrap_or(&empty_labels);
            if !labels.contains_key(LABEL_UPDATE_INTERFACE_NAME) {
                // Label is missing, delete NodeInstance
                info!("Label is missing, try to delete NodeInstance '{}'", nodeinstance_name);
                // // Check Store and api.get for refresh delays
                // let nodeinstance_tmp = nodeinstance_api.get(&nodeinstance_name).await?;
                // println! ("The node meta:{:?} ", nodeinstance_tmp.meta());
                // println! ("The storemeta:{:?} ", nodeinstance.meta());
                delete_nodeinstance(nodeinstance_api, &nodeinstance_name).await?;
            }
        } else {
            // Node does not exist, delete NodeInstance
            info!("Node does not exist, try to delete NodeInstance '{}'", nodeinstance_name);
            delete_nodeinstance(nodeinstance_api, &nodeinstance_name).await?;
        }
    }

    Ok(())
}

async fn delete_nodeinstance(
    nodeinstance_api: &Api<NodeInstance>,
    nodeinstance_name: &str,
) -> Result<(), Error> {
    /* Store is not refreshed immediately, NodeInstance maybe is deleted but still exists in the Store.
    *  The result is an additional deletion record (does not affect the deletion result)
    *  If you don't want additional deletion, switch to nodeinstance_api.get(nodeinstance_name).
    *  such as: match nodeinstance_api.get(nodeinstance_name).await{...}
    */
    match nodeinstance_api
        .delete(nodeinstance_name, &DeleteParams::default())
        .await
    {
        Ok(_) => {
            info!("Deleted NodeInstance '{}'", nodeinstance_name);
        }
        Err(kube::Error::Api(e)) if e.code == 404 => {
            // Already deleted
            info!(
                "NodeInstance '{}' not found (already deleted)",
                nodeinstance_name
            );
        }
        Err(e) => {
            error!(
                "Failed to delete NodeInstance '{}': {:?}",
                nodeinstance_name, e
            );
            return Err(Error::KubeError(e));
        }
    }
    Ok(())
}

/// Error handling policy for the controller.
///
/// This function is called by the controller whenever an error occurs during reconciliation.
///
/// # Arguments:
/// - `_object`: The NodeOS resource that caused the error.
/// - `_error`: The error that occurred.
/// - `_ctx`: The shared context for the controller, containing the Kubernetes client.
///
/// # Returns:
/// - `Action`: The action to take after an error, such as requeueing the resource.
pub fn error_policy(_object: Arc<NodeOS>, _error: &Error, _ctx: Arc<Data>) -> Action {
    error!("Reconcile error:{}", _error.to_string());
    Action::requeue(REQUEUE_ERROR)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::Duration;
    use tokio::time::timeout;
    // Import necessary items from test_mock and crd crates
    use crate::test_mock::{NodeInstanceTestScenario, NodeInstanceExt, NodeOSExt};
    use crd::nodeinstance::{NodeInstance, NodeInstanceSpec};
    use kube::runtime::watcher::Event;
    use kube::core::ObjectMeta;

    #[tokio::test]
    async fn test_reconcile_update() {
        // Use Data::test() to get the test context, mock server verifier, store, and writer
        let (ctx, fakeserver, _nodeinstance_store, mut nodeinstance_writer, _node_store, mut node_writer) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));
    
        // Prepare our test NodeInstance
        let node_instance = NodeInstance::new_test_nodeinstance("node1");
        // Use writer.apply_watcher_event() to add the NodeInstance to the store
        nodeinstance_writer.apply_watcher_event(&Event::Apply(node_instance.clone()));
        
        // Prepare our test Node with the required label
        let mut node = Node {
            metadata: ObjectMeta {
                name: Some("node1".to_string()),
                labels: Some(std::collections::BTreeMap::new()),
                ..Default::default()
            },
            ..Default::default()
        };
        node.metadata.labels.as_mut().unwrap().insert(LABEL_UPDATE_INTERFACE_NAME.to_string(), "v1".to_string());
        // Apply the Node to the node_store
        node_writer.apply_watcher_event(&Event::Apply(node.clone()));
    
        // Prepare the scenario
        let scenario = NodeInstanceTestScenario::Update {
            node_name: "node1".to_string(),
            current_version: "v1.0".to_string(),
            desired_version: "v1.1".to_string(),
            status: STATUS_UPDATING.to_string(),
        };

        // Run the mock server in a separate task
        let verification = tokio::spawn(async move {
            fakeserver.run(scenario).await;
        });
        // Call the reconcile function
        let action = reconcile(nodeos, ctx).await.unwrap();

        assert_eq!(action, Action::requeue(REQUEUE_TIME));
        // Ensure the mock server completes without errors
        timeout(Duration::from_secs(1), verification)
            .await
            .expect("Mock server did not complete in time")
            .expect("Mock server task panicked");
    }
    
    #[tokio::test]
    async fn test_reconcile_rollback() {
        // Use Data::test() to get the test context and fakeserver verifier
        let (ctx, fakeserver, _nodeinstance_store, mut nodeinstance_writer, _node_store, mut node_writer) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("rollback", "v1.1"));
    
        // Prepare our test NodeInstance
        let mut node_instance = NodeInstance::new_test_nodeinstance("node1");
        node_instance.spec.upversion = "v1.2".to_string(); // Set current version if needed
        // node_instance.spec.status = STATUS_IDLE.to_string(); // Set status to STATUS_IDLE firstly, then update/rollback can be executed
        // Use writer.apply_watcher_event() to add the NodeInstance to the store
        nodeinstance_writer.apply_watcher_event(&Event::Apply(node_instance.clone()));
    
        // Prepare our test Node with the required label
        let mut node = Node {
            metadata: ObjectMeta {
                name: Some("node1".to_string()),
                labels: Some(std::collections::BTreeMap::new()),
                ..Default::default()
            },
            ..Default::default()
        };
        node.metadata.labels.as_mut().unwrap().insert(LABEL_UPDATE_INTERFACE_NAME.to_string(), "v1".to_string());
        // Apply the Node to the node_store
        node_writer.apply_watcher_event(&Event::Apply(node.clone()));
    
        // Prepare the scenario
        let scenario = NodeInstanceTestScenario::Rollback {
            node_name: "node1".to_string(),
            current_version: "v1.2".to_string(),
            rollback_version: "v1.1".to_string(),
            status: STATUS_ROLLBACK.to_string(),
        };

        // Run the mock server in a separate task
        let verification = tokio::spawn(async move {
            fakeserver.run(scenario).await;
        });
        // Call the reconcile function
        let action = reconcile(nodeos, ctx).await.unwrap();

        // Check the action
        assert_eq!(action, Action::requeue(REQUEUE_TIME));
        // Ensure the mock server completes without errors
        timeout(Duration::from_secs(1), verification)
            .await
            .expect("Mock server did not complete in time")
            .expect("Mock server task panicked");
    }

    use kube::runtime::reflector;
    #[test]
    fn test_count_idle_node_in_store() {
        // Create a Store and Writer
        let (store, mut writer) = reflector::store::<NodeInstance>();
    
        // Prepare test NodeInstances
        let node1 = NodeInstance::new_test_nodeinstance("node1");
        let node2 = NodeInstance::new_test_nodeinstance("node2").with_spec(NodeInstanceSpec {
            hostname: "node2".to_string(),
            status: STATUS_UPDATING.to_string(),
            upversion: "v1.0".to_string(),
            version: "v1.0".to_string(),
            upcount: 0,
        });
        let node3 = NodeInstance::new_test_nodeinstance("node3");
    
        // Apply the nodes to the store using writer
        writer.apply_watcher_event(&Event::Apply(node1.clone()));
        writer.apply_watcher_event(&Event::Apply(node2.clone()));
        writer.apply_watcher_event(&Event::Apply(node3.clone()));
    
        // Now count the idle nodes using the store
        let idle_count = store
            .state()
            .iter()
            .filter(|node| node.spec.status == STATUS_IDLE)
            .count() as i32;
    
        assert_eq!(idle_count, 2);
    }

    #[tokio::test]
    async fn test_reconcile_cleanup_nodeinstance_deleted() {
        let (ctx, fakeserver, _nodeinstance_store, mut nodeinstance_writer, _node_store, mut node_writer) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        // Prepare our test NodeInstance
        // NodeInstance1 without corresponding Node, should be deleted.
        let mut node_instance1 = NodeInstance::new_test_nodeinstance("node1");
        node_instance1.spec.status = STATUS_UPDATING.to_string();
        nodeinstance_writer.apply_watcher_event(&Event::Apply(node_instance1.clone()));

        // NodeInstance2 with a labeled Node, should not be deleted.
        let node_instance2 = NodeInstance::new_test_nodeinstance("node2");
        nodeinstance_writer.apply_watcher_event(&Event::Apply(node_instance2.clone()));
        // Node2 for node_instance2, and labels Node2
        let mut node2 = Node {
            metadata: ObjectMeta {
                name: Some("node2".to_string()),
                labels: Some(std::collections::BTreeMap::new()),
                ..Default::default()
            },
            ..Default::default()
        };
        node2.metadata.labels.as_mut().unwrap().insert(
            LABEL_UPDATE_INTERFACE_NAME.to_string(),
            "v1".to_string()
        );
        node_writer.apply_watcher_event(&Event::Apply(node2.clone()));

        // Do NOT add the Node to node_store to simulate the Node not existing
        // Prepare the scenario
        let scenario = NodeInstanceTestScenario::Delete {
            node_name: "node1".to_string(),
        };

        // Run the mock server in a separate task
        let verification = tokio::spawn(async move {
            fakeserver.run(scenario).await;
        });
        // Call the reconcile function
        let action = reconcile(nodeos, ctx).await.unwrap();

        // Assert that the action is as expected
        assert_eq!(action, Action::requeue(REQUEUE_TIME));
        // Ensure the mock server completes without errors
        timeout(Duration::from_secs(1), verification)
            .await
            .expect("Mock server did not complete in time")
            .expect("Mock server task panicked");
    }
}