mod controller;
#[cfg(test)]
mod test_mock;

use futures::{future, StreamExt};
use kube::runtime::{reflector, watcher, WatchStreamExt};
use kube::runtime::watcher::Config;
use kube::runtime::Controller;
use kube::{Api, Client, ResourceExt};
use k8s_openapi::api::core::v1::Node;
use std::sync::Arc;
use tracing::{debug, error, info, warn, Level};
use tracing_subscriber::FmtSubscriber;

use controller::Data;
use crd::nodeinstance::NodeInstance;
use crd::nodeos::NodeOS;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;

    let client = Client::try_default().await?;
    info!("Start manager!");

    // Create a store and reflector for NodeInstance
    let nodeinstance_api: Api<NodeInstance> = Api::all(client.clone());
    let (nodeinstance_reader, nodeinstance_writer) = reflector::store();
    let nodeinstance_reflector = reflector( nodeinstance_writer, watcher(nodeinstance_api.clone(), Config::default()) )
        .default_backoff() // Optional: adds backoff strategy
        .touched_objects()
        .for_each(|result| {
            future::ready(match result {
                Ok(node_instance) => {
                    debug!(
                        "Store NodeInstance {} in namespace {}",
                        node_instance.name_any(),
                        node_instance.namespace().unwrap_or_default()
                    );
                }
                Err(e) => warn!("NodeInstance watcher error: {:?}", e),
            })
        });
    tokio::spawn(nodeinstance_reflector); // Run the reflector for NodeInstance

    // Create a store and reflector for Node
    let node_api: Api<Node> = Api::all(client.clone());
    let (node_store, node_writer) = reflector::store();
    let node_reflector = reflector( node_writer, watcher(node_api.clone(), Config::default()) )
        .default_backoff() // Optional: adds backoff strategy
        .touched_objects()
        .for_each(|result| {
            future::ready(match result {
                Ok(node) => {
                    debug!("Store Node {}", node.name_any());
                }
                Err(e) => warn!("Node watcher error: {:?}", e),
            })
        });
    tokio::spawn(node_reflector); // Run the reflector for Node

    let context = Arc::new(Data::new(client.clone(), nodeinstance_reader, node_store));

    let nodeos_api: Api<NodeOS> = Api::all(client.clone());
    info!("Operator Api object created!");
    let controller = Controller::new(nodeos_api, Config::default())
        .run(controller::reconcile, controller::error_policy, context);

    controller
        .for_each(|res| async move {
            match res {
                Ok(o) => info!("Operator manager reconciled {:?}", o),
                Err(e) => error!("Operator manager reconcile failed: {:?}", e),
            }
        })
        .await;
    info!("Controller of operator manager for_each end!");

    Ok(())
}