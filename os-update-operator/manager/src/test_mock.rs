use http::{Method, Request, Response};
use kube::{client::Body, Client};
use kube::runtime::reflector;
use kube::runtime::reflector::store::{Store, Writer};
use k8s_openapi::api::core::v1::Node;
use std::sync::Arc;
use tower_test::mock;
use tracing::info;

use crate::controller::{Data, STATUS_IDLE};
use crd::nodeinstance::{NodeInstance, NodeInstanceSpec};
use crd::nodeos::{NodeOS, NodeOSSpec};
// use kube::core::{ListMeta, ObjectList, TypeMeta};
use serde::Deserialize;

#[derive(Deserialize)]
struct PatchedSpec {
    upversion: String,
    status: String,
    upcount: i32,
}

pub trait NodeOSExt {
    fn new_test_nodeos(opstype: &str, osversion: &str) -> Self;
}

impl NodeOSExt for NodeOS {
    fn new_test_nodeos(opstype: &str, osversion: &str) -> Self {
        NodeOS {
            metadata: kube::api::ObjectMeta {
                name: Some("test-nodeos".to_string()),
                namespace: Some("default".to_string()),
                ..Default::default()
            },
            spec: NodeOSSpec {
                opstype: opstype.to_string(),
                osversion: osversion.to_string(),
                concurrent: 1,
                downloadurl: "http://example.com/os".to_string(),
                checksum: "checksum123".to_string(),
                cacert: "cacert_content".to_string(),
                clientcert: "clientcert_content".to_string(),
                clientkey: "clientkey_content".to_string(),
            },
            status: None,
        }
    }
}

// Extension trait for NodeInstance
pub trait NodeInstanceExt {
    fn new_test_nodeinstance(name: &str) -> Self;
    fn with_spec(self, spec: NodeInstanceSpec) -> Self;
}

impl NodeInstanceExt for NodeInstance {
    fn new_test_nodeinstance(name: &str) -> Self {
        NodeInstance {
            metadata: kube::api::ObjectMeta {
                name: Some(name.into()),
                namespace: Some("default".into()),
                ..Default::default()
            },
            spec: NodeInstanceSpec {
                hostname: name.into(),
                status: STATUS_IDLE.into(),
                upversion: "v1.0".into(),
                version: "v1.0".into(),
                upcount: 0,
            },
            status: None,
        }
    }

    fn with_spec(mut self, spec: NodeInstanceSpec) -> Self {
        self.spec = spec;
        self
    }
}

// Type alias for the mock handle
type MockServerHandle = mock::Handle<Request<Body>, Response<Body>>;

// Struct wrapping the handle
pub struct MockServerVerifier(MockServerHandle);

impl MockServerVerifier {
    pub async fn run(mut self, scenario: NodeInstanceTestScenario) {
        match scenario {
            NodeInstanceTestScenario::Update { node_name, desired_version, status, .. } => {
                // Handle the patch request
                self.handle_patch_node(&node_name, &desired_version, &status).await;
                // Optionally handle DELETE requests if necessary
                // self.maybe_handle_delete_nodeinstance(&node_name).await;
            },
            NodeInstanceTestScenario::Rollback { node_name, rollback_version, status, .. } => {
                // Handle the patch request
                self.handle_patch_node(&node_name, &rollback_version, &status).await;
                // Optionally handle DELETE requests if necessary
                // self.maybe_handle_delete_nodeinstance(&node_name).await;
            },
            NodeInstanceTestScenario::Delete { node_name } => {
                // Handle the DELETE request for NodeInstance
                self.handle_delete_nodeinstance(&node_name).await;
            },
            // Add other scenarios as needed
        }
    }

    // async fn handle_list_nodes(&mut self, node_name: &str) {
    //     let (request, send) = self.0.next_request().await.expect("Service not called");
    //     println!("handle_list_nodes request: {}, {}", request.method(), request.uri().path().to_string());
    //     assert_eq!(request.method(), Method::GET);
    //     assert!(request.uri().path().contains("/nodeinstances"));

    //     let nodes = vec![NodeInstance::new_test_nodeinstance(node_name)];
    
    //     // Create TypeMeta instance
    //     let type_meta = TypeMeta {
    //         api_version: "xuanwan.opencloudos.org/v1".to_string(), // actual group and version
    //         kind: "NodeInstanceList".to_string(), // The kind should be the plural of your resource kind + "List"
    //     };
    //     let response_body = serde_json::to_vec(&ObjectList {
    //         types: type_meta,
    //         metadata: ListMeta::default(),
    //         items: nodes,
    //     })
    //     .unwrap();

    //     send.send_response(
    //         Response::builder()
    //             .status(200)
    //             .body(Body::from(response_body))
    //             .unwrap(),
    //     );
    // }

    async fn handle_patch_node(&mut self, node_name: &str, desired_version: &str, status: &str) {
        let (request, send) = self.0.next_request().await.expect("Service not called");
        println!("handle_patch_node request: {}, {}", request.method(), request.uri().path());
        assert_eq!(request.method(), Method::PATCH);
        assert!(request.uri().path().ends_with(&format!("/nodeinstances/{}", node_name)));
        
        let req_body = request.into_body().collect_bytes().await.unwrap();
        let json: serde_json::Value = serde_json::from_slice(&req_body).expect("patch object is json");
        // Extract the "spec" field
        let spec_json = json.get("spec").expect("spec object").clone();
        // Deserialize into PatchedSpec
        let patched_spec: PatchedSpec = serde_json::from_value(spec_json).expect("valid spec");
        assert_eq!(patched_spec.upversion, desired_version);
        assert_eq!(patched_spec.status, status);
        // Create an updated NodeInstance
        let mut updated_node = NodeInstance::new_test_nodeinstance(node_name);
        updated_node.spec.upversion = patched_spec.upversion;
        updated_node.spec.status = patched_spec.status;
        updated_node.spec.upcount = patched_spec.upcount;
        let response_body = serde_json::to_vec(&updated_node).unwrap();
        send.send_response(
            Response::builder()
                .status(200)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_delete_nodeinstance(&mut self, node_name: &str) {
        // Expect a DELETE request
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::DELETE);
        assert!(request.uri().path().ends_with(&format!("/nodeinstances/{}", node_name)));

        // Create a correct delete response
        // Note: A valid Status object needs to be returned
        let status = serde_json::json!({
            "apiVersion": "v1",
            "kind": "Status",
            "metadata": {},
            "status": "Success",
            "details": {
                "name": node_name,
                "kind": "nodeinstances",
                "uid": "test-uid"
            }
        });

        send.send_response(
            Response::builder()
                .status(200)
                .header("content-type", "application/json")
                .body(Body::from(serde_json::to_vec(&status).unwrap()))
                .unwrap(),
        );
        info!("Handled DELETE request for NodeInstance '{}'", node_name);
    }

    // async fn maybe_handle_delete_nodeinstance(&mut self, node_name: &str) {
    //     // Try to receive a DELETE request with a timeout
    //     if let Ok(Some((request, send))) = tokio::time::timeout(std::time::Duration::from_secs(1), self.0.next_request()).await {
    //         if request.method() == Method::DELETE && request.uri().path().ends_with(&format!("/nodeinstances/{}", node_name)) {
    //             // Handle the DELETE request
    //             send.send_response(
    //                 Response::builder()
    //                     .status(200)
    //                     .body(Body::empty())
    //                     .unwrap(),
    //             );
    //             info!("Handled DELETE request for NodeInstance '{}'", node_name);
    //         } else {
    //             panic!("Unexpected request: {:?}", request);
    //         }
    //     } else {
    //         // No DELETE request received; possibly acceptable depending on the test scenario
    //         info!("No DELETE request received for NodeInstance '{}'", node_name);
    //     }
    // }
}

pub enum NodeInstanceTestScenario {
    Update {
        node_name: String,
        current_version: String,
        desired_version: String,
        status: String,
    },
    Rollback {
        node_name: String,
        current_version: String,
        rollback_version: String,
        status: String,
    },
    Delete {
        node_name: String,
    },
    // Add more scenarios as needed
}

impl Data {
    /// Create a test context with a mocked kube client, `Store`s, and `Writer`s
    pub fn test() -> (
        Arc<Self>,
        MockServerVerifier,
        Store<NodeInstance>,
        Writer<NodeInstance>,
        Store<Node>,
        Writer<Node>,
    ) {
        // Create the Stores and Writers
        let (nodeinstance_store, nodeinstance_writer) = reflector::store::<NodeInstance>();
        let (node_store, node_writer) = reflector::store::<Node>();

        let (mock_service, handle) = mock::pair::<Request<Body>, Response<Body>>();
        let client = Client::new(mock_service, "default");

        let ctx = Data {
            client,
            nodeinstance_store: nodeinstance_store.clone(),
            node_store: node_store.clone(),
        };

        (
            Arc::new(ctx),
            MockServerVerifier(handle),
            nodeinstance_store,
            nodeinstance_writer,
            node_store,
            node_writer,
        )
    }
}