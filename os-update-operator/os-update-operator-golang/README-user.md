# 用户指导
## 部署 Master
当前玄湾OS 仅支持 Node 节点部署，部署前需要创建好 k8s 集群的 Master 节点。
本地部署可参考：[Kubernetes部署指南 - OpenCloudOS Documentation](https://docs.opencloudos.org/OCS/Virtualization_and_Containers_Guide/Kubernetes_UserGuide/)

## 部署 crd
创建 Node 节点前需要在 Master 节点部署自定义资源 nodesos 及 nodeinstance 的 crd，命令如下：
```
kubectl apply -f xuanwan.opencloudos.org_nodeos.yaml
kubectl apply -f xuanwan.opencloudos.org_nodeinstances.yaml
```

命令中使用的文件，可以从如下地址获取：

[https://gitee.com/OpenCloudOS/xuanwan/tree/master/xuanwan-operator/myconfig](https://gitee.com/OpenCloudOS/xuanwan/tree/master/xuanwan-operator/myconfig)

目录结构如下：
```bash
myconfig/
├── admin-container.yaml
├── operator
│   ├── up-manager.yaml
│   └── up-proxy.yaml
├── nodeos.yaml
├── noderollback.yaml
├── rbac
│   ├── role_binding.yaml
│   ├── role.yaml
│   └── service_account.yaml
├── xuanwan.opencloudos.org_nodeinstances.yaml
└── xuanwan.opencloudos.org_nodeos.yaml
```

玄湾OS的对接k8s双分区升级特性涉及到三个组件：up-executor、up-proxy、up-manager。
其中，up-executor 已通过rpm包的形式集成到 xbuildsys 中，构建镜像时会自动集成。Node 节点启动时，会通过 systemd 服务主动拉起 up-executor。
up-proxy及up-manager均通过容器化部署，up-proxy部署为DaemonSet，up-manager部署为Deployment。

## 部署rbac权限控制规则
在master节点直接执行以下命令：
```
kubectl apply -f rbac/*
```
rbac用于控制up-manager及up-proxy对自定义资源NodeOS、NodeInstance的访问。

## 部署双分区升级的up-manager
up-manager的Dockerfile参考如下：
```
FROM golang:1.22 AS builder
ARG TARGETOS
ARG TARGETARCH

WORKDIR /workspace

COPY go.mod go.mod
COPY go.sum go.sum
COPY cmd/ cmd/
COPY api/ api/
COPY internal/ internal/
COPY vendor/ vendor/

RUN CGO_ENABLED=0 GOOS=${TARGETOS:-linux} GOARCH=${TARGETARCH} go build -mod=vendor -a -o up-manager cmd/manager/main.go

FROM gcr.io/distroless/static:nonroot AS up-manager
WORKDIR /
COPY --from=builder /workspace/up-manager .
USER root

ENTRYPOINT ["/up-manager"]
```
在master节点直接执行以下命令：
```
kubectl apply -f operator/up-manager.yaml
```
up-manager组件将以Pod形式运行于master节点。

## 创建 Node
镜像中需要集成初始化服务（如 cloud-init 等），并配合特定云平台提供的 CVM 主机实例 metadata 机制完成节点初始化。xbuildsys 集成了 k8s-node-init.service 和 join-k8s.service 用于本地调测时对节点进行初始化。

## 升级 Node OS
升级前需要将 `产品名+kernel版本号+构建时间戳-boot.ext4.lz4` 文件解压上传到 http 服务器。

### Node节点打标签
将待升级的Node节点打上升级标签，执行如下命令：
```
kubectl label nodes <node name> xuanwan.opencloudos.org/updater-interface-version="1.0.0"
```
依从将待升级的Node节点打上标签，下面的up-proxy组件将以DaemonSet的方式部署到这些Node节点上。

### Node节点部署up-proxy组件
up-proxy的Dockerfile参考如下：
```
FROM golang:1.22 AS builder
ARG TARGETOS
ARG TARGETARCH

WORKDIR /workspace

COPY go.mod go.mod
COPY go.sum go.sum
COPY cmd/ cmd/
COPY api/ api/
COPY internal/ internal/
COPY vendor/ vendor/

RUN CGO_ENABLED=0 GOOS=${TARGETOS:-linux} GOARCH=${TARGETARCH} go build -mod=vendor -a -o up-proxy cmd/proxy/main.go

FROM gcr.io/distroless/static:nonroot AS up-proxy
WORKDIR /
COPY --from=builder /workspace/up-proxy .
USER root

ENTRYPOINT ["/up-proxy"]
```
部署up-proxy执行如下命令：
```
kubectl apply -f operator/up-proxy.yaml
```
至此，可以在master节点上面看到up-proxy的Pod，这些Pod实际运行于各个选中的Node:
```
kubectl get daemonsets.apps -A
NAMESPACE      NAME              DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system    up-proxy          2         2         2       2            2           <none>                   112m
```

### 编写升级配置文件
升级前需要编写升级 yaml 文件（nodeos-update.yaml），参考如下：
```yaml
apiVersion: xuanwan.opencloudos.org/v1
kind: NodeOS
metadata:
  name: node-os
spec:
  opstype: <update>
  osversion: <up-version>
  concurrent: <2>
  downloadurl: <download-url>
  checksum: null
  cacert: null
  clientcert: null
  clientkey: null
```
当前升级只需要设置 opstype、osversion、concurrent 和 downloadurl 四个字段。

`opstype` 设置要执行的操作类型，当前支持 update、rollback 和 check 三种操作，执行升级流程设置为 update。

`osversion` 设置要升级到的版本号，保存在 rootfs 中 /etc/xuanwan_version 文件中

`concurrent` 设置节点升级的并发数量

`downloadurl` 设置 rootfs 镜像的 http 地址

### 升级操作
执行升级操作命令如下：
```
kubectl apply -f nodeos-update.yaml
```
### 查看升级状态
可以通过如下命令观察升级状态变化：
```
kubectl get nodeinstances.xuanwan.opencloudos.org --watch
```

输出参考如下：
```
NAME        HOSTNAME    STATUS   VERSION                     UPVERSION
oc9-node4   oc9-node4   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node3   oc9-node3   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node7   oc9-node7   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node5   oc9-node5   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node6   oc9-node6   idle     XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1541
oc9-node4   oc9-node4   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
oc9-node3   oc9-node3   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
oc9-node4   oc9-node4   idle       XuanwanOS 1.0 240730.1602   XuanwanOS 1.0 240730.1602
oc9-node7   oc9-node7   updating   XuanwanOS 1.0 240730.1541   XuanwanOS 1.0 240730.1602
```
`VERSION` 和 `UPVERSION` 版本一致，均为升级前版本号，同时节点状态为 `idle`，则说明节点处于待升级状态。

`VERSION` 和 `UPVERSION` 版本一致，均为升级后版本号，同时节点状态为 `idle`，则说明节点已升级完成。

其他状态表明节点正在升级中。

## 回滚 Node OS
回滚 Node OS 需要节点至少已成功执行过一次升级任务。

### 回滚准备
回滚前需要编写回滚 yaml 文件（nodeos-rollback.yaml），参考如下：
```yaml
apiVersion: xuanwan.opencloudos.org/v1
kind: NodeOS
metadata:
  name: node-os
spec:
  opstype: <rollback>
  osversion: <version>
  concurrent: <2>
  downloadurl: null
  checksum: null
  cacert: null
  clientcert: null
  clientkey: null
```
当前回滚只需要设置 opstype、osversion 和 concurrent 三个字段。

`opstype` 设置要执行的操作类型，当前支持 update、rollback 和 check 三种操作，执行回滚流程设置为 rollback。

`osversion` 设置升级前的版本号

`concurrent` 设置节点升级的并发数量

### 回滚操作
执行回滚操作命令如下：
```
kubectl apply -f nodeos-rollback.yaml
```

### 查看回滚状态
可以通过如下命令观察回滚状态变化：
```
kubectl get nodeinstances.xuanwan.opencloudos.org --watch
```
`VERSION` 和 `UPVERSION` 版本一致，均为升级前版本号，同时节点状态为 `idle`，则说明节点回滚成功。
`VERSION` 和 `UPVERSION` 版本一致，均为升级后版本号，同时节点状态为 idle，则说明节点处于待回滚状态。
其他状态表明节点正在回滚中。

### 节点升级失败处理
如果节点升级失败，节点的NodeInstance资源的status字段将被标记为`upfailed`:
```
kubectl get nodeinstances.xuanwan.opencloudos.org -n kube-system
NAME        HOSTNAME       STATUS       VERSION                     UPVERSION                   UPCOUNT
oc9-node4   oc9-node4      upfailed     XuanwanOS 1.0 240731.1658   XuanwanOS 1.0 240731.1658   3
oc9-node5   oc9-node5      upfailed     XuanwanOS 1.0 240731.1658   XuanwanOS 1.0 240731.1658   3
```
此时可以先删除节点的升级标签:
```
kubectl label nodes <node name> xuanwan.opencloudos.org/updater-interface-version-
```
然后执行：
```
kubectl apply -f operator/up-proxy.yaml
```
这样升级失败的节点将暂时移出升级系统，由管理者介入处理。

## 启动运维容器
玄湾OS 未提供 sshd 服务，如果需要登录节点，可以通过运维容器登录。

### 构建运维容器镜像
dockerfile 参考如下：

```dockerfile
FROM opencloudos9-minimal:9.2-v20240617
  
USER root
WORKDIR /root

RUN dnf install openssh openssh-server iputils net-tools passwd procps-ng iproute -y && \
        dnf clean all

RUN /usr/libexec/openssh/sshd-keygen rsa && \
        /usr/libexec/openssh/sshd-keygen ecdsa && \
        /usr/libexec/openssh/sshd-keygen ed25519 && \
        echo "PermitRootLogin yes" >> /etc/ssh/sshd_config  && \
        echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config  && \
        echo "Port 2222" >> /etc/ssh/sshd_config  && \
        yes "Xuanwan@2024" | passwd root

EXPOSE 2222

ENTRYPOINT ["/usr/sbin/sshd", "-D"]

```
opencloudos9-minimal 镜像可从如下地址下载并导入：

[https://mirrors.tencent.com/opencloudos/9/images/x86_64/OpenCloudOS-Container-Minimal-9.2-20240617.0.x86_64.tar.xz](https://mirrors.tencent.com/opencloudos/9/images/x86_64/OpenCloudOS-Container-Minimal-9.2-20240617.0.x86_64.tar.xz)

可以根据需要自定义运维容器镜像。

### 启动准备
编写运维容器 yaml 文件（admin-container.yaml），参考如下：
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: admin-pod
spec:
  hostPID: true
  hostNetwork: true
  containers:
  - name: admin-container
    image: 192.168.122.1:5000/admincontainer:1.0
    imagePullPolicy: IfNotPresent
    securityContext:
      privileged: true
  nodeSelector:
    kubernetes.io/hostname: node1
```
关键参数说明如下：

`hostPID` 设置为 true，运维容器与 Node 共享 pid 命名空间，容器内可以查看节点上的进程。

`hostNetwork` 设置为 true，运维容器与 Node 共享网络命名空间，可以直接通过 node 节点 ip 访问运维容器
`image`：设置运维容器镜像地址

`privileged` 设置为 true，将运维容器设置为特权容器

`nodeSelector` 需要根据实际情况设置节点的选择器，用户选出需要登录的节点

### 启动运维容器
启动运维容器命令如下：

```
kubectl apply -f admin-container.yaml
```

### 登录运维容器
可以通过 ssh 命令直接登录运维容器

```
ssh -p 2222 root@nodeip
```
### 进入主机环境
运维容器内执行如下命令进入主机 1 号进程命名空间，进而进入主机环境：
```
nsenter -t 1 -a
```

# 注意事项

1.当编译过程中，如果出现空间不足，则需要用如下命令，清理一下docker磁盘：
```
docker system prune -a
```

2.在编译C文件的时候,如果有依赖第三方库,此时的第三方库是被安装到%{_cross_sysroot}目录,因此需要在编译的时候添加CFLAGS/LDFLAGS,如下:
```
CFLAGS="-I%{_cross_sysroot}/usr/include -L%{_cross_sysroot}/usr/lib64"
```

3.当在镜像内新增安装包或者文件时，需要修改targets/<product>/Cargo.toml调整镜像大小，否则会出现镜像空间不足的情况。
```
打开Cargo.toml，找到image-size， 并修改为对应的大小。
```

4.修改packages/< package >/Cargo.toml文件后，需要执行
```
cd packages; cargo update; cd packages

注： 不执行，会出现error: the lock file packages/Cargo.lock needs to be updated but --locked was passed to prevent this
```

5.出现错误 ERROR: failed to do request: Head "https://registry-1.docker.io/v2/docker/dockerfile/manifests/1.1.3-experimental": net/http: TLS handshake timeout
```
打开/etc/docker/daemon.json文件,修改为如下(如果不存在,需要创建一个新的)
{
    "experimental": false,
    "insecure-registries": [
        "mirrors.tencent.com", // 或者这里修改成其他可用mirrors
    ]
}
修改后执行systemctl restart docker,再重新执行cargo make即可.
```

