/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// NodeInstanceSpec defines the desired state of NodeInstance
type NodeInstanceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Hostname  string `json:"hostname"`
	Status    string `json:"status"`
	Upversion string `json:"upversion"`
	Version   string `json:"version"`
	Upcount   int    `json:"upcount"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// NodeInstance is the Schema for the nodeinstances API
type NodeInstance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec NodeInstanceSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// NodeInstanceList contains a list of NodeInstance
type NodeInstanceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []NodeInstance `json:"items"`
}

func init() {
	SchemeBuilder.Register(&NodeInstance{}, &NodeInstanceList{})
}
