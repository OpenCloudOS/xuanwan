package factory

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	ctrl "sigs.k8s.io/controller-runtime"
	"strings"
)

const (
	imagePath string = "/persist/data/xuanwan.ext4"
	partA     string = "/dev/vda2"
	partB     string = "/dev/vda3"
)

var log = ctrl.Log.WithName("executor").WithName("factory")

type Upops struct {
	Version    string
	Url        string
	Checksum   string
	Cacaert    string
	Clientcert string
	Clientkey  string
}

func RunUpdate(ops *Upops) error {
	//get os image
	if err := runCmd("curl", "-o", imagePath, ops.Url); err != nil {
		return err
	}

	if _, err := os.Stat(imagePath); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return err
		}
	}

	//find the next part
	part, err := findPart()
	if err != nil {
		return err
	}

	//writer os image
	if err := runCmd("dd", "if="+imagePath, "of="+part, "bs=4M"); err != nil {
		return err
	}

	if err := runCmd("/usr/sbin/update.sh"); err != nil {
		return err
	}

	//set flag
	if err := runCmd("system-update-set", "setflg", "/dev/vda", "1"); err != nil {
		return err
	}

	if part == partA {
		if err := runCmd("system-update-set", "switch", "/dev/vda", "a"); err != nil {
			return err
		}
	} else {
		if err := runCmd("system-update-set", "switch", "/dev/vda", "b"); err != nil {
			return err
		}
	}

	if err := runCmd("reboot"); err != nil {
		return err
	}

	return nil
}

func RunRollback() error {
	//find the next part
	part, err := findPart()
	if err != nil {
		return err
	}
	//set flag
	if err := runCmd("system-update-set", "setflg", "/dev/vda", "1"); err != nil {
		return err
	}

	if part == partA {
		if err := runCmd("system-update-set", "switch", "/dev/vda", "a"); err != nil {
			return err
		}
	} else {
		if err := runCmd("system-update-set", "switch", "/dev/vda", "b"); err != nil {
			return err
		}
	}

	if err := runCmd("reboot"); err != nil {
		return err
	}

	return nil
}

func runCmd(name string, opts ...string) error {
	out, err := exec.Command(name, opts...).CombinedOutput()
	if err != nil {
		return fmt.Errorf("run command:%s %v out:%s err:%s", name, opts, out, err)
	}
	return nil
}

func findPart() (string, error) {
	out, err := exec.Command("lsblk", "-no", "MOUNTPOINT", "/dev/vda2").CombinedOutput()
	if err != nil {
		log.Error(err, "run lsblk failed")
		return "", err
	}

	root := strings.TrimSpace(string(out))
	if root == "/" {
		return partB, nil
	}
	return partA, nil
}
