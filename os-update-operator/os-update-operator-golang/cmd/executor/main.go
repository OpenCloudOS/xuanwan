package main

import (
	"flag"
	"google.golang.org/grpc"
	executorclient "opencloudos.org/m/cmd/executor/api"
	"opencloudos.org/m/cmd/executor/server"
	constant "opencloudos.org/m/internal/constants"
	"os"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
)

var log = ctrl.Log.WithName("executor")

func main() {
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	log.Info("Start executor")
	l, err := server.NewListener(constant.SockDir, constant.SockName)
	if err != nil {
		log.Error(err, "New executor server listener failed")
		os.Exit(1)
	}

	s := &server.Server{}
	grpcServer := grpc.NewServer()
	executorclient.RegisterXuanwanExecutorServer(grpcServer, s)
	if err := grpcServer.Serve(l); err != nil {
		log.Error(err, "up-executor server start failed")
	}

	log.Info("up-executor start serving")
}
