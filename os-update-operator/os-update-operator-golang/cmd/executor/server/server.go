package server

import (
	"context"
	"net"
	executorclient "opencloudos.org/m/cmd/executor/api"
	"opencloudos.org/m/cmd/executor/factory"
	constant "opencloudos.org/m/internal/constants"
	"os"
	"path/filepath"
	ctrl "sigs.k8s.io/controller-runtime"
	"strings"
)

const (
	SockDir  = "/run/os-executor"
	SockName = "os-executor.sock"
)

var log = ctrl.Log.WithName("executor")

type Lock struct {
	state uint32
}

type Server struct {
	executorclient.UnimplementedXuanwanExecutorServer
	mutex         Lock
	disableReboot bool
}

func NewListener(dir, name string) (l net.Listener, err error) {
	if err := os.MkdirAll(dir, 0755); err != nil {
		return nil, err
	}

	addr := filepath.Join(dir, name)
	if err := os.Remove(addr); err != nil && !os.IsNotExist(err) {
		return nil, err
	}
	l, err = net.Listen("unix", addr)
	return l, err
}

func (s *Server) Update(_ context.Context, req *executorclient.UpdateRequest) (*executorclient.UpdateResponse, error) {
	log.Info("Ready to update os")
	updateReq := factory.Upops{
		Version:    req.Version,
		Url:        req.Url,
		Checksum:   req.Checksum,
		Cacaert:    req.Cacaert,
		Clientcert: req.Clientcert,
		Clientkey:  req.Clientkey,
	}

	version, err := readFile(constant.VersionFile)
	if err != nil {
		return nil, err
	}

	if version == req.Version {
		return &executorclient.UpdateResponse{}, nil
	}

	if err := factory.RunUpdate(&updateReq); err != nil {
		return nil, err
	}
	return &executorclient.UpdateResponse{}, nil
}

func (s *Server) Rollback(_ context.Context, req *executorclient.RollbackRequest) (*executorclient.RollbackResponse, error) {
	log.Info("Ready to rollback os")

	if err := factory.RunRollback(); err != nil {
		return nil, err
	}
	return &executorclient.RollbackResponse{}, nil
}

func readFile(file string) (string, error) {
	content, err := os.ReadFile(file)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(content)), err
}
