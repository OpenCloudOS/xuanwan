package constant

import "time"

const (
	RequeTime = 10 * time.Second
)

const (
	FieldHostname string = "hostname"
	FieldStatus   string = "status"
)

const (
	StatusIdle     string = "idle"
	StatusUpdating string = "updating"
	StatusRollback string = "rollback"
	StatusFailed   string = "upfailed"
)

const (
	SockAddr     = "/run/os-executor/os-executor.sock"
	SockDir      = "/run/os-executor"
	SockName     = "os-executor.sock"
	HostnameFile = "/etc/hostname"
	NodeOSName   = "node-os"
	VersionFile  = "/etc/xuanwan_version"
)
