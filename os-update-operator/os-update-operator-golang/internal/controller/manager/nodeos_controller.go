/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/workqueue"
	xuanwanv1 "opencloudos.org/m/api/v1"
	constant "opencloudos.org/m/internal/constants"
	"os"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
)

// NodeOSReconciler reconciles a NodeOS object
type NodeOSReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	ready  bool
}

const (
	MaxConcurrent int = 2
)

var log = ctrl.Log.WithName("manager").WithName("NodeOS")

//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeos,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeos/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeos/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the NodeOS object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.3/pkg/reconcile
func (r *NodeOSReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	nodeOS := xuanwanv1.NodeOS{}
	if err := r.Get(ctx, req.NamespacedName, &nodeOS); err != nil {
		if client.IgnoreNotFound(err) != nil {
			log.Error(err, "get NodeOS failed")
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	concurrent := min(MaxConcurrent, nodeOS.Spec.Concurrent)
	sumOfIdleNode, err := countIdleNode(ctx, r)
	if err != nil || sumOfIdleNode == 0 {
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	sumOfUpdatingNode, err := countUpdatingNode(ctx, r)
	if err != nil || sumOfUpdatingNode >= concurrent {
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	sumOfRollbackNode, err := countRollbackNode(ctx, r)
	if err != nil || sumOfRollbackNode >= concurrent {
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}
	if sumOfUpdatingNode > 0 && sumOfRollbackNode > 0 {
		log.Error(nil, "fatal error, can't update and rollback concurrently")
		os.Exit(1)
	}
	if err := checkStatus(nodeOS.Spec.Opstype, sumOfUpdatingNode, sumOfRollbackNode); err != nil {
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	switch nodeOS.Spec.Opstype {
	case "update":
		return UpdateNodeInstance(ctx, r, nodeOS.Spec.Osversion, concurrent-sumOfUpdatingNode)
	case "rollback":
		return RollbackNodeInstance(ctx, r, nodeOS.Spec.Osversion, concurrent-sumOfRollbackNode)
	case "check":
		return CheckNodeInstance(ctx, r, nodeOS.Spec.Osversion, sumOfIdleNode)
	default:
		log.Error(nil, "not support opts type")
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *NodeOSReconciler) SetupWithManager(mgr ctrl.Manager) error {
	ctx := context.Background()
	indexer := mgr.GetFieldIndexer()
	if err := indexer.IndexField(ctx, &xuanwanv1.NodeInstance{}, constant.FieldHostname,
		func(obj client.Object) []string {
			nodeInstance, ok := obj.(*xuanwanv1.NodeInstance)
			if !ok {
				log.Error(nil, "set field indexer failed")
				return []string{}
			}
			return []string{nodeInstance.Spec.Hostname}
		}); err != nil {
		return err
	}
	if err := indexer.IndexField(ctx, &xuanwanv1.NodeInstance{}, constant.FieldStatus,
		func(obj client.Object) []string {
			nodeInstance, ok := obj.(*xuanwanv1.NodeInstance)
			if !ok {
				log.Error(nil, "set field indexer failed")
				return []string{}
			}
			return []string{nodeInstance.Spec.Status}
		}); err != nil {
		return err
	}
	return ctrl.NewControllerManagedBy(mgr).
		For(&xuanwanv1.NodeOS{}).
		Watches(&corev1.Node{}, handler.Funcs{DeleteFunc: r.DeleteNodeInstance}).
		Complete(r)
}

func (r *NodeOSReconciler) DeleteNodeInstance(ctx context.Context, e event.DeleteEvent, q workqueue.RateLimitingInterface) {
	hostname := e.Object.GetName()
	nodeInstance := xuanwanv1.NodeInstance{}
	if err := r.Get(ctx, types.NamespacedName{Namespace: e.Object.GetNamespace(), Name: hostname}, &nodeInstance); err != nil {
		log.Error(err, "unable to get node instance")
		return
	}

	if err := r.Delete(ctx, &nodeInstance); err != nil {
		log.Error(err, "delete node instance failed")
	}
	log.Info("Delete NodeInstance in work node", "work node name", hostname)
}

func countIdleNode(ctx context.Context, r *NodeOSReconciler) (int, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	if err := r.List(ctx, &nodeInstanceList, client.MatchingFields{constant.FieldStatus: constant.StatusIdle}); err != nil {
		log.Error(err, "List idle node instance failed")
		return 0, err
	}
	return len(nodeInstanceList.Items), nil
}

func countUpdatingNode(ctx context.Context, r *NodeOSReconciler) (int, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	if err := r.List(ctx, &nodeInstanceList, client.MatchingFields{constant.FieldStatus: constant.StatusUpdating}); err != nil {
		log.Error(err, "List updating node instance failed")
		return 0, err
	}
	return len(nodeInstanceList.Items), nil
}

func countRollbackNode(ctx context.Context, r *NodeOSReconciler) (int, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	if err := r.List(ctx, &nodeInstanceList, client.MatchingFields{constant.FieldStatus: constant.StatusRollback}); err != nil {
		log.Error(err, "List rollback node instance failed")
		return 0, err
	}
	return len(nodeInstanceList.Items), nil
}

func checkStatus(ops string, update, rollback int) error {
	if ops == constant.StatusUpdating && rollback > 0 {
		return errors.New("can't update for rollback is doing")
	}

	if ops == constant.StatusRollback && update > 0 {
		return errors.New("can't rollback for update is doing")
	}
	return nil
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func UpdateNodeInstance(ctx context.Context, r *NodeOSReconciler, version string, need int) (ctrl.Result, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	//todo here this may list not enough
	if err := r.List(ctx, &nodeInstanceList, client.MatchingFields{constant.FieldStatus: constant.StatusIdle}); err != nil {
		log.Error(err, "List idle node instance failed")
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	for i, j := 0, 0; i < need; {
		if j == len(nodeInstanceList.Items) {
			break
		}
		nodeInstance := nodeInstanceList.Items[j]
		if nodeInstance.Spec.Upversion == version {
			j++
			continue
		}

		nodeInstance.Spec.Status = constant.StatusUpdating
		nodeInstance.Spec.Upversion = version
		nodeInstance.Spec.Upcount = 0
		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "update node instance failed", constant.FieldHostname, nodeInstance.Spec.Hostname)
		}
		log.Info("update node instance", constant.FieldHostname, nodeInstance.Spec.Hostname)
		i++
		j++
	}

	return ctrl.Result{RequeueAfter: constant.RequeTime}, nil
}

func RollbackNodeInstance(ctx context.Context, r *NodeOSReconciler, version string, need int) (ctrl.Result, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	if err := r.List(ctx, &nodeInstanceList, client.MatchingFields{constant.FieldStatus: constant.StatusIdle}); err != nil {
		log.Error(err, "List idle node instance failed")
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	for i := 0; i < need; i++ {
		if i == len(nodeInstanceList.Items) {
			break
		}
		nodeInstance := nodeInstanceList.Items[i]
		if nodeInstance.Spec.Upversion == version {
			continue
		}
		nodeInstance.Spec.Status = constant.StatusRollback
		nodeInstance.Spec.Upversion = version
		nodeInstance.Spec.Upcount = 0
		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "rollback node instance failed", constant.FieldHostname, nodeInstance.Spec.Hostname)
		}
	}
	return ctrl.Result{RequeueAfter: constant.RequeTime}, nil
}

func CheckNodeInstance(ctx context.Context, r *NodeOSReconciler, version string, sum int) (ctrl.Result, error) {
	nodeInstanceList := xuanwanv1.NodeInstanceList{}
	if err := r.List(ctx, &nodeInstanceList); err != nil {
		log.Error(err, "List idle node instance failed")
		return ctrl.Result{RequeueAfter: constant.RequeTime}, err
	}

	if sum < len(nodeInstanceList.Items) {
		log.Info("check failed, update or rollback is doing")
	}

	for _, nodeInstance := range nodeInstanceList.Items {
		if nodeInstance.Spec.Version != version {
			log.Info("check failed, instance version not the same", constant.FieldHostname, nodeInstance.Spec.Hostname)
		}
	}
	return ctrl.Result{RequeueAfter: constant.RequeTime}, nil
}
