/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package proxy

import (
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	constant "opencloudos.org/m/internal/constants"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/kubectl/pkg/drain"
	xuanwanv1 "opencloudos.org/m/api/v1"
	executorclient "opencloudos.org/m/cmd/executor/api"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

var log = ctrl.Log.WithName("proxy").WithName("NodeInstance")

// NodeInstanceReconciler reconciles a NodeInstance object
type NodeInstanceReconciler struct {
	client.Client
	Scheme        *runtime.Scheme
	Connection    executorclient.XuanwanExecutorClient
	kubeclientset kubernetes.Interface
	hostName      string
}

func NewNodeInstanceReconciler(mgr manager.Manager) *NodeInstanceReconciler {
	set := kubernetes.NewForConfigOrDie(mgr.GetConfig())
	timeout := 3 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	conn, err := grpc.DialContext(ctx, "unix://"+constant.SockAddr, grpc.WithInsecure(), grpc.WithTimeout(timeout), grpc.WithBlock())
	if err != nil {
		log.Error(err, "grpc dial failed")
		os.Exit(1)
	}
	executorclient := executorclient.NewXuanwanExecutorClient(conn)
	name, err := readFile(constant.HostnameFile)
	if err != nil || name == "" {
		log.Error(nil, "get hostname failed")
		os.Exit(1)
	}

	return &NodeInstanceReconciler{
		Client:        mgr.GetClient(),
		Scheme:        mgr.GetScheme(),
		Connection:    executorclient,
		kubeclientset: set,
		hostName:      name,
	}
}

//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeinstances,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeinstances/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=xuanwan.opencloudos.org,resources=nodeinstances/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the NodeInstance object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.3/pkg/reconcile
func (r *NodeInstanceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	version, err := readFile(constant.VersionFile)
	if err != nil {
		log.Error(err, "get Node OS version failed")
		return ctrl.Result{RequeueAfter: time.Second * 10}, err
	}
	nodeInstance := xuanwanv1.NodeInstance{}
	if err := r.Get(ctx, types.NamespacedName{Namespace: req.Namespace, Name: r.hostName}, &nodeInstance); err != nil {
		if errors.IsNotFound(err) {
			log.Info("Create NodeInstance")
			nodeInstance = xuanwanv1.NodeInstance{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: req.Namespace,
					Name:      r.hostName,
				},
				Spec: xuanwanv1.NodeInstanceSpec{
					Hostname:  r.hostName,
					Status:    "idle",
					Upversion: version,
					Version:   version,
				},
			}
			if err = r.Create(ctx, &nodeInstance); err != nil {
				log.Error(err, "create Node Instance failed")
				return ctrl.Result{RequeueAfter: time.Second * 10}, err
			}
		} else {
			log.Error(err, "get NodeInstance failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, err
		}
	}

	nodeOS := xuanwanv1.NodeOS{}
	if err := r.Get(ctx, types.NamespacedName{Namespace: req.Namespace, Name: constant.NodeOSName}, &nodeOS); err != nil {
		if errors.IsNotFound(err) {
			log.Info("get NodeOS failed")
			return ctrl.Result{}, nil
		}
		log.Error(err, "get NodeOS failed")
		return ctrl.Result{}, nil
	}
	log.Info("Get nodeOS success")

	var node corev1.Node
	if err := r.Get(ctx, client.ObjectKey{Name: r.hostName}, &node); err != nil {
		log.Error(err, "get Node failed")
		return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
	}

	// after reboot, we update status and version
	if nodeInstance.Spec.Status == "updating" && nodeInstance.Spec.Upversion == version {
		// now the node is cordon, we should uncordon it
		if err := runUncordon(ctx, r, &node); err != nil {
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}

		//now everything is OK, we set the node status to idle
		nodeInstance.Spec.Status = "idle"
		nodeInstance.Spec.Version = version
		nodeInstance.Spec.Upcount = 0
		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "update node instance failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}
		return ctrl.Result{}, nil
	}

	if nodeInstance.Spec.Status == "updating" && nodeInstance.Spec.Upversion != version {
		if nodeInstance.Spec.Upcount >= 3 {
			nodeInstance.Spec.Status = "upfailed"
			if err := r.Update(ctx, &nodeInstance); err != nil {
				log.Error(err, "update node instance failed")
				return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
			}
			log.Error(nil, "update failed after 3 retries")
			return ctrl.Result{}, nil
		}
		if err := evictPod(ctx, r, &node); err != nil {
			log.Error(err, "evict pod failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}

		nodeInstance.Spec.Upcount += 1
		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "update node instance failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}
		request := executorclient.UpdateRequest{
			Version:    nodeOS.Spec.Osversion,
			Url:        nodeOS.Spec.Downloadurl,
			Checksum:   nodeOS.Spec.Checksum,
			Cacaert:    nodeOS.Spec.Cacert,
			Clientcert: nodeOS.Spec.Clientcert,
			Clientkey:  nodeOS.Spec.Clientkey,
		}
		if _, err := r.Connection.Update(ctx, &request); err != nil {
			defer runUncordon(ctx, r, &node)
			log.Error(err, "os update throw grpc failed")

			time.Sleep(10 * time.Second)
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	if nodeInstance.Spec.Status == "rollback" && nodeInstance.Spec.Upversion != version {
		if nodeInstance.Spec.Upcount >= 3 {
			nodeInstance.Spec.Status = "rollfailed"
			if err := r.Update(ctx, &nodeInstance); err != nil {
				log.Error(err, "update node instance failed")
				return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
			}
			log.Error(nil, "rollback failed after 3 retries")
			return ctrl.Result{}, nil
		}
		if err := evictPod(ctx, r, &node); err != nil {
			log.Error(err, "evict pod failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}

		nodeInstance.Spec.Upcount += 1
		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "update node instance failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}
		if _, err := r.Connection.Rollback(ctx, &executorclient.RollbackRequest{}); err != nil {
			defer runUncordon(ctx, r, &node)
			log.Error(err, "os rollback throw grpc failed")

			time.Sleep(10 * time.Second)
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	if nodeInstance.Spec.Status == "rollback" && nodeInstance.Spec.Upversion == version {
		if err := runUncordon(ctx, r, &node); err != nil {
			return ctrl.Result{RequeueAfter: 10 * time.Second}, nil
		}
		nodeInstance.Spec.Status = "idle"
		nodeInstance.Spec.Version = version
		nodeInstance.Spec.Upcount = 0

		if err := r.Update(ctx, &nodeInstance); err != nil {
			log.Error(err, "update node instance failed")
			return ctrl.Result{RequeueAfter: 10 * time.Second}, err
		}
		return ctrl.Result{}, nil
	}

	if nodeOS.Spec.Opstype == "update" && nodeOS.Spec.Osversion != version {
		return ctrl.Result{RequeueAfter: time.Second * 10}, nil
	}
	if nodeOS.Spec.Opstype == "rollback" && nodeOS.Spec.Osversion != version {
		return ctrl.Result{RequeueAfter: time.Second * 10}, nil
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *NodeInstanceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&xuanwanv1.NodeOS{}).
		Complete(r)
}

func newDrainer(ctx context.Context, r *NodeInstanceReconciler) *drain.Helper {
	return &drain.Helper{
		Ctx:                 ctx,
		Client:              r.kubeclientset,
		GracePeriodSeconds:  -1,
		DeleteEmptyDirData:  true,
		IgnoreAllDaemonSets: true,
		Force:               true,
		Out:                 os.Stdout,
		ErrOut:              os.Stderr,
	}
}

func evictPod(ctx context.Context, r *NodeInstanceReconciler, node *corev1.Node) error {
	drainer := newDrainer(ctx, r)

	if node.Spec.Unschedulable {
		return nil
	}
	log.Info("Evicting pod in node", "nodeName", node.Name)

	if err := drain.RunCordonOrUncordon(drainer, node, true); err != nil {
		log.Error(err, "cordon node failed")
		return err
	}

	if err := drain.RunNodeDrain(drainer, node.Name); err != nil {
		log.Error(err, "drain node failed")
		if err := drain.RunCordonOrUncordon(drainer, node, false); err != nil {
			log.Error(err, "uncordon node failed")
			return err
		}
		return err
	}

	return nil
}

func runUncordon(ctx context.Context, r *NodeInstanceReconciler, node *corev1.Node) error {
	drainer := newDrainer(ctx, r)
	if !node.Spec.Unschedulable {
		return nil
	}

	log.Info("Uncordon node")
	if err := drain.RunCordonOrUncordon(drainer, node, false); err != nil {
		log.Error(err, "uncordon node failed")
		return err
	}
	return nil
}

func readFile(file string) (string, error) {
	content, err := os.ReadFile(file)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(content)), err
}
