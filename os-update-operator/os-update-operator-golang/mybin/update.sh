#!/bin/bash

orig_partuuid=`cat /boot/grub2/grub.cfg | grep PARTUUID | sed 's/.*PARTUUID=\(.*\)/\1/' | awk '{print $1}'`
echo $orig_partuuid

orig_hostname=`cat /etc/hostname`
echo $orig_hostname

part=`echo ${orig_partuuid: -1}`
echo $part

prefix=`echo ${orig_partuuid} | sed 's/.$//'`
echo $prefix

if [ "${part}" == "2" ]; then
    next_partuuid=${prefix}3
    disk="/dev/vda"3
else
    next_partuuid=${prefix}2
    disk="/dev/vda"2
fi
echo $next_partuuid
echo $disk

mkdir -p /persist/mnt
mount ${disk} /persist/mnt
tmp_uuid=`cat /persist/mnt/boot/grub2/grub.cfg | grep PARTUUID | sed 's/.*PARTUUID=\(.*\)/\1/' | awk '{print $1}'`
echo $tmp_uuid

sed -i s/root=PARTUUID=${tmp_uuid}/root=PARTUUID=${next_partuuid}/g /persist/mnt/boot/grub2/grub.cfg
echo $orig_hostname > /persist/mnt/etc/hostname
cat /persist/mnt/boot/grub2/grub.cfg
cat /persist/mnt/etc/hostname

umount /persist/mnt
rm -f /persist/data/xuanwan.ext4
