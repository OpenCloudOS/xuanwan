// executor/build.rs and proxy/build.rs are build scripts in different crates and do not affect each other.
// The former generates the server and the latter generates the client

use std::{env, path::PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // reflection API: set file descriptor
    let descriptor_path = PathBuf::from(env::var("OUT_DIR").unwrap()).join("proto_descriptor.bin");
    tonic_build::configure()
        .build_client(true)     // Generate client code!
        .file_descriptor_set_path(&descriptor_path)
        .compile(&["../executor/proto/executor.proto"], &["../executor/proto"])
        .unwrap();
    Ok(())
}