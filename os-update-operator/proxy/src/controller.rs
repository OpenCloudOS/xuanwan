use kube::api::{EvictParams, ListParams, Patch, PatchParams};
use kube::runtime::controller::Action;
use kube::{Api, Client, ResourceExt};
use k8s_openapi::api::core::v1::{Node, Pod};
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;
use tokio::fs::File;
use tokio::io::{self, AsyncReadExt};
use tonic::transport::{Channel, Endpoint, Uri};
use tonic::Request;
use tracing::{debug, error, info, warn};

use crd::nodeinstance::{NodeInstance, NodeInstanceSpec};
use crd::nodeos::NodeOS;

pub mod proto {
    tonic::include_proto!("executor");
}

use proto::xuanwan_agent_client::XuanwanAgentClient;
use proto::{RollbackRequest, UpdateRequest};

const REQUEUE_ERROR: Duration = Duration::from_secs(2);
const REQUEUE_TIME: Duration = Duration::from_secs(10);
const STATUS_IDLE: &str = "idle";
const STATUS_UPDATING: &str = "updating";
const STATUS_ROLLBACK: &str = "rollback";
const STATUS_UPFAILED: &str = "updatefailed";
const STATUS_RBFAILED: &str = "rollbackfailed";
const MAX_RETRY_COUNT: i32 = 5;
const REBOOT_SLEEP_INTERVAL: u64 = 180;
pub const LABEL_UPDATE_INTERFACE_NAME: &str = "xuanwan.opencloudos.org/updater-interface-version";

const SOCK_ADDR: &str = "/run/os-executor/os-executor.sock";
const VERSION_FILE: &str = "/etc/xuanwan_version";
const HOSTNAME_FILE: &str = "/etc/hostname";
// const NODEOS_NAME: &str = "node-os";

/// Shared context for the controller, containing the Kubernetes client.
#[derive(Clone)]
pub struct Data {
    pub client: Client,
    pub agent_client: XuanwanAgentClient<Channel>,
    pub host_name: String,
}

impl Data {
    /// Create a new Data instance with the given Kubernetes client.
    ///
    /// # Arguments:
    /// - `client`: A Kubernetes client to make Kubernetes REST API requests with. Resources
    ///   will be created and deleted with this client.
    pub async fn new(client: Client) -> Self {
        let sock_dir = std::path::Path::new(SOCK_ADDR).parent().expect("Failed to extract directory from SOCK_ADDR");
        // check sock_dir
        if !sock_dir.exists() {
            error!("Directory {} does not exist, please check!", sock_dir.display());
        }

        // tonic requires a valid URL as a parameter to the Endpoint.
        // http://[::]:50051 is a placeholder and will not be used during the actual connection.
        // The actual connection address is the Unix socket in the connect_with_connector below.
        // let endpoint = Endpoint::try_from("http://localhost")
        let endpoint = Endpoint::try_from("http://[::]:50051".to_string())
            .unwrap()
            .connect_with_connector(tower::service_fn(|_: Uri| {
                tokio::net::UnixStream::connect(SOCK_ADDR)
            }))
            .await
            .expect("Failed to connect to gRPC server");
        let agent_client = XuanwanAgentClient::new(endpoint);
        let host_name = read_file(HOSTNAME_FILE).await.expect("Failed to read host name");
        Data { client, agent_client, host_name }
    }
}

/// Custom error type for the controller.
#[derive(Debug, Error)]
pub enum Error {
    #[error("Kubernetes reported error: {0}")]
    Kube(#[from] kube::Error),
    #[error("Namespace not found for NodeInstance {0}")]
    NamespaceNotFound(String),
    #[error("Failed to read file: {0}")]
    FileReadError(io::Error),
    #[error("Failed to set value: {0}")]
    NodeValueError(String),
    #[error("gRPC error: {0}")]
    GrpcError(tonic::Status),
}

async fn read_file(path: &str) -> Result<String, Error> {
    let mut file = File::open(path).await.map_err(Error::FileReadError)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).await.map_err(Error::FileReadError)?;
    Ok(contents.trim().to_string())
}

/// Reconcile, create ,update NodeInstance resource.
///
/// This function is called by the controller whenever a NodeInstance resource is created, updated, or deleted.
///
/// # Arguments:
/// - `nodeos`: The NodeOS resource being reconciled.
/// - `ctx`: The shared context for the controller, containing the Kubernetes client.
///
/// # Returns:
/// - `Ok(Action)`: The action to take after reconciliation, such as requeueing the resource.
/// - `Err(Error)`: An error occurred during reconciliation.
pub async fn reconcile(nodeos: Arc<NodeOS>, ctx: Arc<Data>) -> Result<Action, Error> {
    debug!("Start reconcile for NodeOS {:?}", nodeos.name_any());
    debug!("Reconciling NodeOS: {:?}", nodeos);

    // Read desired version from file
    let local_version = match read_file(VERSION_FILE).await {
        Ok(version) => version,
        Err(e) => {
            error!("Failed to read desired version: {}", e);
            return Ok(Action::requeue(REQUEUE_TIME));
        }
    };

    // Get NodeInstance object (create if it doesn't exist)
    let namespace = nodeos.namespace().ok_or_else(|| Error::NamespaceNotFound(nodeos.name_any()))?;
    let nodeinstance_api: Api<NodeInstance> = Api::namespaced(ctx.client.clone(), &namespace);
    let ni_name = ctx.host_name.clone();

    // Find the current node
    let node_api: Api<Node> = Api::all(ctx.client.clone());
    // let node = node_api.get(&node_instance.spec.hostname).await?;
    let node = match node_api.get(&ni_name).await {
        Ok(node) => node,
        Err(e) => {
            error!("Failed to get Node {}: {}", ni_name, e);
            return Ok(Action::requeue(REQUEUE_TIME));
        }
    };
    // debug!("Get node: {:?}", node);
    
    // Get or create NodeInstance object
    let node_instance = match nodeinstance_api.get(&ni_name).await {
        Ok(ni) => ni,
        Err(kube::Error::Api(api_err)) if api_err.code == 404 => {
            // Before creating, check if the Node has the required label
            // If there is no label, manager will delete the NodeInstance. 
            // Therefore, proxy should not create NodeInstance.
            let empty_labels = std::collections::BTreeMap::new();
            let labels = node.metadata.labels.as_ref().unwrap_or(&empty_labels);
            if !labels.contains_key(LABEL_UPDATE_INTERFACE_NAME) {
                warn!("Node '{}' has no label '{}'. Skipping NodeInstance creation.",ni_name, LABEL_UPDATE_INTERFACE_NAME);
                return Ok(Action::requeue(REQUEUE_TIME));
            }

            // NodeInstance doesn't exist, create it
            info!("Create NodeInstance!");
            let new_ni = NodeInstance {
                metadata: kube::api::ObjectMeta {
                    name: Some(ni_name.clone()),
                    namespace: Some(namespace.clone()),
                    ..Default::default()
                },
                spec: NodeInstanceSpec {
                    hostname: ctx.host_name.clone(),
                    status: STATUS_IDLE.to_string(),
                    upversion: local_version.clone(),
                    version: local_version.clone(),
                    upcount: 0,
                },
                status: None,
            };
            match nodeinstance_api.create(&Default::default(), &new_ni).await {
                Ok(ni) => ni,
                Err(e) => {
                    error!("Failed to create NodeInstance: {}", e);
                    return Ok(Action::requeue(REQUEUE_TIME));
                }
            }
        }
        Err(e) => {
            error!("Failed to get NodeInstance: {:?}", e);
            return Err(Error::Kube(e));
        }
    };
    // debug!("Get ni: {:?}", node_instance);

    debug!("NodeInstance name:{}, Status:{}, upcount:{}", ni_name, node_instance.spec.status, node_instance.spec.upcount);
    debug!("NodeInstance Version:{}, local etc Version:{}", node_instance.spec.upversion, local_version);
    debug!("NodeOS       Version:{}", nodeos.spec.osversion);
    // Handle NodeInstance status and upversion
    match node_instance.spec.status.as_str() {
        STATUS_UPDATING if node_instance.spec.upversion == local_version => {
            // Case 1: Upgrading and versions match
            info!("Node is updating and versions match. Uncordoning node.");
            node_api.uncordon(&node.name_any()).await?;
            let patch = serde_json::json!({
                "spec": {
                    "status": STATUS_IDLE,
                    "version": local_version,
                    "upcount": 0,
                }
            });
            nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await?;
        },
        STATUS_UPDATING if node_instance.spec.upversion != local_version => {
            // Case 2: Upgrading and versions do not match
            // Check if upcount >= MAX_RETRY_COUNT
            if node_instance.spec.upcount >= MAX_RETRY_COUNT {
                error!("Update failed after {} retries!", MAX_RETRY_COUNT);
                let patch = serde_json::json!({
                    "spec": {
                        "status": STATUS_UPFAILED,
                    }
                });
                if let Err(err) = nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await {
                    error!("Failed to modify nodeinstance status to '{}': {:?}", STATUS_UPFAILED, err);
                }
                return Ok(Action::requeue(REQUEUE_TIME));
            }

            // Evict pods
            info!("Node is updating and versions not match. Evicting pods and updating.");
            evict_pods(ctx.client.clone(), &node_api, &node).await?;
            
            // Increment upcount and modify NodeInstance
            let new_upcount = node_instance.spec.upcount + 1;
            debug!("NodeInstance Upcount +1, now upcount is: {}", new_upcount);
            let patch = serde_json::json!({
                "spec": {
                    "upcount": new_upcount,
                }
            });
            if let Err(err) = nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await {
                error!("Failed to modify nodeinstance upcount: {:?}", err);
                return Ok(Action::requeue(REQUEUE_TIME));
            }

            // Proceed with the update request, call grpc
            info!("Call grpc to update, waiting...");
            let request = UpdateRequest {
                version: nodeos.spec.osversion.clone(),
                url: nodeos.spec.downloadurl.clone(),
                checksum: nodeos.spec.checksum.clone(),
                cacert: nodeos.spec.cacert.clone(),
                clientcert: nodeos.spec.clientcert.clone(),
                clientkey: nodeos.spec.clientkey.clone(),
            };
            let response = ctx.agent_client.clone().update(Request::new(request)).await.map_err(Error::GrpcError)?;
            if response.get_ref().err != 0 {
                return Ok(Action::requeue(REQUEUE_TIME));
            }
            // Sleep to avoid entering k8s reconcile
            std::thread::sleep(Duration::from_secs(REBOOT_SLEEP_INTERVAL));
            debug!("### Sleep {}s, waitting fo reboot! ###", REBOOT_SLEEP_INTERVAL);
        },
        STATUS_ROLLBACK if node_instance.spec.upversion == local_version => {
            // Case 3: Rolling back and versions match
            info!("Node is rolling back and versions match. Uncordoning node.");
            node_api.uncordon(&node.name_any()).await?;
            let patch = serde_json::json!({
                "spec": {
                    "status": STATUS_IDLE,
                    "version": local_version,
                    "upcount": 0,
                }
            });
            nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await?;
        },
        STATUS_ROLLBACK if node_instance.spec.upversion != local_version => {
            // Case 4: Rolling back and versions do not match
            // Check if upcount >= MAX_RETRY_COUNT
            if node_instance.spec.upcount >= MAX_RETRY_COUNT {
                error!("Rollback failed after {} retries!", MAX_RETRY_COUNT);
                let patch = serde_json::json!({
                    "spec": {
                        "status": STATUS_RBFAILED,
                    }
                });
                if let Err(err) = nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await {
                    error!("Failed to modify nodeinstance status to '{}': {:?}", STATUS_RBFAILED, err);
                }
                return Ok(Action::requeue(REQUEUE_TIME));
            }

            // Evict pods
            info!("Node is rolling back and versions not match. Evicting pods and rolling back.");
            evict_pods(ctx.client.clone(), &node_api, &node).await?;

            // Increment upcount and modify NodeInstance
            let new_upcount = node_instance.spec.upcount + 1;
            debug!("NodeInstance Upcount +1, now upcount is: {}", new_upcount);
            let patch = serde_json::json!({
                "spec": {
                    "upcount": new_upcount,
                }
            });
            if let Err(err) = nodeinstance_api.patch(&node_instance.name_any(), &PatchParams::default(), &Patch::Merge(&patch)).await {
                error!("Failed to modify nodeinstance upcount: {:?}", err);
                return Ok(Action::requeue(REQUEUE_TIME));
            }

            // Proceed with the request, call grpc
            info!("Call grpc to rollback, waiting...");
            let response = ctx.agent_client.clone().rollback(Request::new(RollbackRequest {})).await.map_err(Error::GrpcError)?;
            if response.get_ref().err != 0 {
                return Ok(Action::requeue(REQUEUE_TIME));
            }
            // Sleep to avoid entering k8s reconcile
            std::thread::sleep(Duration::from_secs(REBOOT_SLEEP_INTERVAL));
            debug!("### Sleep {}s, waitting fo reboot! ###", REBOOT_SLEEP_INTERVAL);
        },
        _ => {
            debug!("NodeInstance status is '{}', nothing to do.", node_instance.spec.status);
        }
    }

    Ok(Action::requeue(REQUEUE_TIME))
}

/// Error handling policy for the controller.
///
/// This function is called by the controller whenever an error occurs during reconciliation.
///
/// # Arguments:
/// - `_object`: The NodeOS resource.
/// - `_error`: The error that occurred.
/// - `_ctx`: The shared context for the controller, containing the Kubernetes client.
///
/// # Returns:
/// - `Action`: The action to take after an error, such as requeueing the resource.
pub fn error_policy(_object: Arc<NodeOS>, _error: &Error, _ctx: Arc<Data>) -> Action {
    error!("Reconcile error:{}", _error.to_string());
    Action::requeue(REQUEUE_ERROR)
}

/// Evicts all evictable pods from a specified node.
///
/// This function is designed to be used in a Kubernetes environment to manage pod eviction.
/// It isolates the node by cordoning it and then proceeds to evict all pods that are not static.
/// Static pods, which are managed directly by the kubelet, are skipped as they cannot be evicted.
///
/// # Arguments:
/// - `client`: The Kubernetes client used for interacting with the API server.
/// - `node_api`: The API instance for Node operations.
/// - `node`: The Node object representing the node from which pods will be evicted.
///
/// # Returns:
/// - `Result<(), Error>`: Returns Ok(()) if all pods are evicted successfully, otherwise returns an error.
async fn evict_pods(client: Client, node_api: &Api<Node>, node: &Node) -> Result<(), Error> {
    let node_name = node.name_any();

    // Cordon the node to make it unschedulable
    node_api.cordon(&node_name).await.map_err(Error::Kube)?;
    info!("Node {} cordoned", node_name);

    // Get the list of pods on the node
    let pods_api: Api<Pod> = Api::all(client.clone());
    let pods = pods_api
        .list(
            &ListParams::default().fields(&format!("spec.nodeName={}", node_name)),
        )
        .await
        .map_err(Error::Kube)?;

    // Evict each pod
    for pod in pods {
        let pod_name = pod.name_any();
        let namespace = pod.namespace().unwrap_or_default();

        // ship mirror Pod (static pods)
        if pod
            .metadata
            .annotations
            .as_ref()
            .map_or(false, |annotations| {
                annotations.contains_key("kubernetes.io/config.mirror")
            })
        {
            continue;
        }

        // skip DaemonSet manage Pod
        if let Some(owner_refs) = &pod.metadata.owner_references {
            if owner_refs.iter().any(|owner| owner.kind == "DaemonSet") {
                continue;
            }
        }

        let pod_api: Api<Pod> = Api::namespaced(client.clone(), &namespace);

        match pod_api.evict(&pod_name, &EvictParams::default()).await {
            Ok(_) => {
                info!("Evicted pod {} from namespace {}", pod_name, namespace);
            }
            Err(e) => {
                // Uncordon the node if eviction fails
                node_api.uncordon(&node_name).await.map_err(Error::Kube)?;
                error!("Failed to evict pod {} on node {}: {:?}", pod_name, node_name, e);
                return Err(Error::Kube(e));
            }
        }
    }

    info!("All evictable pods on node {} have been evicted", node_name);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_mock::{NodeOSExt, ProxyTestScenario};

    #[tokio::test]
    async fn test_reconcile_nodeinstance_not_found_label_missing() {
        setup_environment_and_files("v1.0", "test-hostname").expect("Failed to set up environment");

        let (ctx, mock_server) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        let reconcile_future = reconcile(nodeos, ctx.clone());
        let mock_future = mock_server.run(ProxyTestScenario::NodeInstanceNotFoundLabelMissing);
        let result = tokio::join!(reconcile_future, mock_future);
        match result.0 {
            Ok(action) => {
                assert_eq!(action, Action::requeue(REQUEUE_TIME));
            },
            Err(e) => {
                error!("Reconcile error: {}", e);
            }
        }

        cleanup_environment_and_files().expect("Failed to clean up environment");
    }

    #[tokio::test]
    async fn test_reconcile_nodeinstance_not_found_label_present() {
        setup_environment_and_files("v1.0", "test-hostname").expect("Failed to set up environment");

        // Set up the test context and mock server verifier
        let (ctx, mock_server) = Data::test();
        // Create a NodeOS object
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        // Run the reconcile function
        let reconcile_future = reconcile(nodeos, ctx.clone());
        // Run the mock server interactions in a separate future
        let mock_future = mock_server.run(ProxyTestScenario::NodeInstanceNotFoundLabelPresent);
        // Run both futures concurrently
        let result = tokio::join!(reconcile_future, mock_future);
        // Check the result of the reconcile function
        match result.0 {
            Ok(action) => {
                assert_eq!(action, Action::requeue(REQUEUE_TIME));
            },
            Err(e) => {
                // The reconcile function returned an error
                error!("Reconcile error: {}", e);
            }
        }

        // Cleanup the test files
        cleanup_environment_and_files().expect("Failed to clean up environment");
    }

    #[tokio::test]
    async fn test_reconcile_nodeinstance_exists_idle() {
        setup_environment_and_files("v1.0", "test-hostname").expect("Failed to set up environment");
        
        let (ctx, mock_server) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        let reconcile_future = reconcile(nodeos, ctx.clone());
        let mock_future = mock_server.run(ProxyTestScenario::NodeInstanceExistsIdle);
        let _result = tokio::join!(reconcile_future, mock_future);

        cleanup_environment_and_files().expect("Failed to clean up environment");
    }

    #[tokio::test]
    async fn test_reconcile_nodeinstance_updating_version_match() {
        setup_environment_and_files("v1.0", "test-hostname").expect("Failed to set up environment");

        let (ctx, mock_server) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        let reconcile_future = reconcile(nodeos, ctx.clone());
        let mock_future = mock_server.run(ProxyTestScenario::NodeInstanceExistsUpdatingVersionMatch);
        let _result = tokio::join!(reconcile_future, mock_future);

        cleanup_environment_and_files().expect("Failed to clean up environment");
    }

    #[tokio::test]
    async fn test_reconcile_nodeinstance_updating_version_mismatch() {
        setup_environment_and_files("v1.0", "test-hostname").expect("Failed to set up environment");

        let (ctx, mock_server) = Data::test();
        let nodeos = Arc::new(NodeOS::new_test_nodeos("update", "v1.1"));

        let reconcile_future = reconcile(nodeos, ctx.clone());
        let mock_future = mock_server.run(ProxyTestScenario::NodeInstanceExistsUpdatingVersionMismatch);
        let _result = tokio::join!(reconcile_future, mock_future);

        cleanup_environment_and_files().expect("Failed to clean up environment");
    }

    fn setup_environment_and_files(version: &str, hostname: &str) -> std::io::Result<()> {
        // Set up the environment variables to override file paths
        std::env::set_var("VERSION_FILE", "/tmp/test_xuanwan_version");
        std::env::set_var("HOSTNAME_FILE", "/tmp/test_hostname");

        // Write test data to the files
        std::fs::write("/tmp/test_xuanwan_version", version)?;
        std::fs::write("/tmp/test_hostname", hostname)?;

        Ok(())
    }

    fn cleanup_environment_and_files() -> std::io::Result<()> {
        // Remove the temporary files if they exist
        let _ = std::fs::remove_file("/tmp/test_xuanwan_version");
        let _ = std::fs::remove_file("/tmp/test_hostname");
        
        // Unset the environment variables
        std::env::remove_var("VERSION_FILE");
        std::env::remove_var("HOSTNAME_FILE");

        Ok(())
    }
}