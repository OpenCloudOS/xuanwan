mod controller;
#[cfg(test)]
mod test_mock;

use futures::stream::StreamExt;
use kube::runtime::watcher::Config;
use kube::runtime::Controller;
use kube::{Api, Client};
use std::sync::Arc;
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

use controller::Data;
use crd::nodeos::NodeOS;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;

    let client = Client::try_default().await?;
    let context: Arc<Data> = Arc::new(Data::new(client.clone()).await);
    info!("start proxy!");

    let nodeos_api: Api<NodeOS> = Api::all(client.clone());
    info!("Proxy Api object created!");
    let controller = Controller::new(nodeos_api , Config::default())
        .run(controller::reconcile, controller::error_policy, context);

    controller.for_each(|res| async move {
        match res {
            Ok(o) => tracing::info!("proxy reconciled {:?}", o),
            Err(e) => tracing::error!("proxy reconcile failed: {:?}", e),
        }
    }).await;
    info!("Controller of proxy for_each end!");

    Ok(())
}
