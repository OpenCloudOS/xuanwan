// proxy/src/test_mock.rs

use http::{Method, Request, Response, StatusCode};
use k8s_openapi::api::core::v1::{Node, NodeSpec, Pod};
use kube::core::{ListMeta, ObjectList, ObjectMeta, TypeMeta};
use kube::{client::Body, Client};
use std::collections::BTreeMap;
use std::sync::Arc;
use tower_test::mock;

use crate::controller::proto::xuanwan_agent_client::XuanwanAgentClient;
use crate::controller::Data;
use crate::controller::LABEL_UPDATE_INTERFACE_NAME;
use crd::nodeinstance::{NodeInstance, NodeInstanceSpec};
use crd::nodeos::{NodeOS, NodeOSSpec};

// Implement extension traits for NodeOS and NodeInstance
pub trait NodeOSExt {
    fn new_test_nodeos(opstype: &str, osversion: &str) -> Self;
}

impl NodeOSExt for NodeOS {
    fn new_test_nodeos(opstype: &str, osversion: &str) -> Self {
        NodeOS {
            metadata: ObjectMeta {
                name: Some("test-nodeos".to_string()),
                namespace: Some("default".to_string()),
                ..Default::default()
            },
            spec: NodeOSSpec {
                opstype: opstype.to_string(),
                osversion: osversion.to_string(),
                concurrent: 1,
                downloadurl: "http://example.com/os".to_string(),
                checksum: "checksum123".to_string(),
                cacert: "cacert_content".to_string(),
                clientcert: "clientcert_content".to_string(),
                clientkey: "clientkey_content".to_string(),
            },
            status: None,
        }
    }
}

pub trait NodeInstanceExt {
    fn new_test_nodeinstance(name: &str) -> Self;
    fn with_spec(self, spec: NodeInstanceSpec) -> Self;
}

impl NodeInstanceExt for NodeInstance {
    fn new_test_nodeinstance(name: &str) -> Self {
        NodeInstance {
            metadata: ObjectMeta {
                name: Some(name.into()),
                namespace: Some("default".into()),
                ..Default::default()
            },
            spec: NodeInstanceSpec {
                hostname: name.into(),
                status: "idle".into(),
                upversion: "v1.0".into(),
                version: "v1.0".into(),
                upcount: 0,
            },
            status: None,
        }
    }

    fn with_spec(mut self, spec: NodeInstanceSpec) -> Self {
        self.spec = spec;
        self
    }
}

type MockServerHandle = mock::Handle<Request<Body>, Response<Body>>;

pub struct MockServerVerifier(MockServerHandle);

impl MockServerVerifier {
    pub async fn run(mut self, scenario: ProxyTestScenario) {
        match scenario {
            ProxyTestScenario::NodeInstanceNotFoundLabelMissing => {
                // Handle GET /nodes/{name} without the required label
                self.handle_get_node(None).await;
                // Since the Node lacks the required label, the controller should not attempt to create NodeInstance
                // No further interactions are expected
            },
            ProxyTestScenario::NodeInstanceNotFoundLabelPresent => {
                // Handle GET /nodes/{name} with the required label
                let mut labels = BTreeMap::new();
                labels.insert(LABEL_UPDATE_INTERFACE_NAME.to_string(), "eth0".to_string());
                self.handle_get_node(Some(labels)).await;
                // Handle GET /nodeinstances/{name} returning 404 Not Found
                self.handle_get_nodeinstance_not_found().await;
                // Handle POST /nodeinstances to create a new NodeInstance
                self.handle_create_nodeinstance().await;
            },
            ProxyTestScenario::NodeInstanceExistsIdle => {
                self.handle_get_node(None).await;
                self.handle_get_nodeinstance_idle().await;
                // Additional interactions as needed
            },
            ProxyTestScenario::NodeInstanceExistsUpdatingVersionMatch => {
                self.handle_get_node(None).await;
                self.handle_get_nodeinstance_updating(true).await; // Version matches local version
                self.handle_uncordon_node().await;
                self.handle_list_pods_on_node().await;
                // self.handle_patch_nodeinstance_status_idle().await;
            },
            ProxyTestScenario::NodeInstanceExistsUpdatingVersionMismatch => {
                self.handle_get_node(None).await;
                self.handle_get_nodeinstance_updating(false).await; // Version does not match
                // The reconcile function might attempt to evict pods and proceed with update
                self.handle_cordon_node().await;
                self.handle_list_pods_on_node().await;
                // Further interactions as needed
                // Since gRPC calls will fail, the function may requeue or handle the error
            },
            // Add other scenarios...
        }
    }

    async fn handle_get_nodeinstance_not_found(&mut self) {
        // Mock GET /nodeinstances/{name} returning 404 Not Found
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::GET);
        assert!(request.uri().path().contains("/nodeinstances/"));
        // Respond with 404 Not Found
        send.send_response(
            Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())
                .unwrap(),
        );
    }

    async fn handle_create_nodeinstance(&mut self) {
        // Mock POST /nodeinstances to create a new NodeInstance
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::POST);
        assert!(request.uri().path().contains("/nodeinstances"));
        // Respond with 201 Created
        let created_nodeinstance = NodeInstance::new_test_nodeinstance("test-hostname");
        let response_body = serde_json::to_vec(&created_nodeinstance).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::CREATED)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_get_node(&mut self, labels: Option<BTreeMap<String, String>>) {
        // Mock GET /nodes/{name} to get Node information
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::GET);
        assert!(request.uri().path().contains("/nodes/"));
        // Create the Node with the specified labels
        let node = Node {
            metadata: ObjectMeta {
                name: Some("test-hostname".to_string()),
                labels,
                ..Default::default()
            },
            spec: None,
            status: None,
        };
        let response_body = serde_json::to_vec(&node).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_get_nodeinstance_idle(&mut self) {
        // Mock GET /nodeinstances/{name} returning existing NodeInstance with idle status
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::GET);
        assert!(request.uri().path().contains("/nodeinstances/"));
        // Respond with existing NodeInstance
        let nodeinstance = NodeInstance::new_test_nodeinstance("test-hostname");
        let response_body = serde_json::to_vec(&nodeinstance).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_get_nodeinstance_updating(&mut self, version_matches: bool) {
        // Mock GET /nodeinstances/{name} returning existing NodeInstance with updating status
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::GET);
        assert!(request.uri().path().contains("/nodeinstances/"));
    
        // Determine upversion based on whether it matches local_version
        let upversion = if version_matches { "v1.0" } else { "v1.1" }; // Assuming local_version is "v1.0"
    
        // Respond with existing NodeInstance
        let nodeinstance = NodeInstance::new_test_nodeinstance("test-hostname").with_spec(NodeInstanceSpec {
            hostname: "test-hostname".to_string(),
            status: "updating".to_string(),
            upversion: upversion.to_string(),
            version: "v0.9".to_string(),
            upcount: 0,
        });
        let response_body = serde_json::to_vec(&nodeinstance).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_cordon_node(&mut self) {
        // Mock PATCH /nodes/{name}
        let (request, send) = self.0.next_request().await.expect("Service not called");
        assert_eq!(request.method(), Method::PATCH);
        assert!(request.uri().path().contains("/nodes/"));
    
        // For simplicity, we're not verifying the patch content here
    
        // Respond with the updated Node
        let node = Node {
            metadata: ObjectMeta {
                name: Some("test-hostname".to_string()),
                ..Default::default()
            },
            spec: None,
            status: None,
        };
        let response_body = serde_json::to_vec(&node).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_uncordon_node(&mut self) {
        // Mock PATCH /nodes/{name} to uncordon the node
        let (request, send) = self.0.next_request().await.expect("Service not called");
        println!("handle_uncordon_node request: {}, {}", request.method(), request.uri().path());
        assert_eq!(request.method(), Method::PATCH);
        assert!(request.uri().path().contains("/nodes/"));
        
        // Respond with the updated Node
        let updated_node = Node {
            metadata: ObjectMeta {
                name: Some("test-hostname".to_string()),
                // Include necessary fields for the uncordon operation
                ..Default::default()
            },
            spec: Some(NodeSpec {
                unschedulable: Some(false), // Node is uncordoned
                ..Default::default()
            }),
            status: None,
        };
        let response_body = serde_json::to_vec(&updated_node).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    async fn handle_list_pods_on_node(&mut self) {
        // Send the response
        let (request, send) = self.0.next_request().await.expect("Service not called");
        println!("handle_list_pods_on_node request: {}, {}", request.method(), request.uri().path());
        assert_eq!(request.method(), Method::GET);
        assert!(request.uri().path().contains("/pods"));
    
        // Create TypeMeta instance for Pods
        let type_meta = TypeMeta {
            api_version: "v1".to_string(), // API version for core resources like Pods
            kind: "PodList".to_string(),   // The kind should be "PodList" for a list of Pods
        };
        // Respond with an empty list or a list of pods as needed
        let pod_list = ObjectList::<Pod> {
            metadata: ListMeta::default(),
            items: vec![], // Empty list for simplicity
            types: type_meta,
        };
        let response_body = serde_json::to_vec(&pod_list).unwrap();
        send.send_response(
            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(response_body))
                .unwrap(),
        );
    }

    // add more interaction handlers if necessary
}


pub enum ProxyTestScenario {
    NodeInstanceNotFoundLabelMissing,
    NodeInstanceNotFoundLabelPresent,
    NodeInstanceExistsIdle,
    NodeInstanceExistsUpdatingVersionMatch,
    NodeInstanceExistsUpdatingVersionMismatch,
    // Add more scenarios as needed
}

// Implement a function to create a test context with a mocked kube client
impl Data {
    pub fn test() -> (Arc<Self>, MockServerVerifier) {
        let (mock_service, handle) =  mock::pair::<Request<Body>, Response<Body>>();
        let client = Client::new(mock_service, "default");

        // For testing, we'll set the host_name to a test value
        let host_name = "test-hostname".to_string();

        // Create a lazy channel (note that the gRPC calls will fail, and we accept that)
        let channel = tonic::transport::Channel::from_static("http://[::]:50051").connect_lazy();

        // Create an instance of XuanwanAgentClient with the lazy channel
        let agent_client = XuanwanAgentClient::new(channel);

        let ctx = Data {
            client,
            agent_client,
            host_name,
        };

        (Arc::new(ctx), MockServerVerifier(handle))
    }
}