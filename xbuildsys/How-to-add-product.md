自定义产品
=================

# 添加自定义产品

在玄湾中添加自定义产品，涉及修改内容如下:

## 1.环境变量

新增产品所需修改变量统一定义于 target-config.env 文件，字段示例如下：

```
# 产品名称，新增产品为 oc9
TENCENT_NAME="oc"
TENCENT_TARGET="oc9"
TENCENT_PRETTY_NAME="OpenCloudOS Instance OS"
# 本地image名称
BUILD_MIRROR="oc9_ximg_generator"
# Docker镜像地址
TENCENT_MIRROR_BASE="docker.io/opencloudos/opencloudos"
# Docker镜像tag，通常为'latest'
TENCENT_MIRROR_TAG="9.0"
# 编译器dist，如 x86_64-TencentOS-linux-ld
COMPILER_DIST="OpenCloudOS"
```

## 2.targets 目录

在targets目录下面创建一个玄湾产品，比如叫example(可参考oc9)。
详细指导见 ${topdir}/targets/README.md

注：新增产品后，targets/Cargo.toml中image-size大小需进行适配调整，避免空间不足。

## 3.packages 目录 (可选)

如果有软件包需要自定义编译，而不是从 Yum 源下载，则需要在 packages 目录新增该软件包。
详细指导见 ${topdir}/packages/README.md 

## 4.rootfs 目录

制作出的镜像将会采用 rootfs 中的配置项，各目录作用如下：
- rootfs/common 为通用配置项；
- rootfs/example 为产品example专用配置项；
详细指导见 ${topdir}/rootfs/README.md 

## 5.etc 目录

该目录定义了制作镜像时相关配置，各目录作用如下：
- etc/common 为通用配置文件；
- etc/example 为产品example专用配置文件，待修改文件如下：
    - rpmconfig/example-arch：待下载RPM包列表；
    - yum.repos.d/中repo文件：Yum源；
    - dnf/vars/releasever：Yum源中releasever变量；
    - builder/Dockerfile：生成镜像所用Dockerfile（按需修改）；
    - download/Dockerfile：下载RPM包所用Dockerfile（按需修改）；

详细指导见 ${topdir}/etc/README.md 

注：rpmconfig 中增加待下载RPM包后，targets/Cargo.toml 中 image-size 大小需进行适配调整，避免空间不足。

