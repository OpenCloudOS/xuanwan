# 玄湾OS构建指导

部署环境 OS 版本：OpenCloudOS 9.2

## 获取代码

1.安装 git 和 git lfs
```
dnf -y install git git-lfs
```
2.初始化 git lfs
```
git lfs install
```
3.下载代码
```
git clone https://gitee.com/OpenCloudOS/xuanwan.git
```

## 环境准备

1.配置 epol 软件源
```
dnf -y install epol-release
```
2.安装 docker-ce 和 docker-buildx
```
dnf -y install docker-ce docker-buildx
```
3.启动 docker 服务
```
systemctl enable docker
systemctl start docker
```
注意：`docker ps` 命令可以成功执行，则启动成功

4.安装 cargo
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
5.cargo安装路径添加到PATH
```
. $HOME/.cargo/env
```
6.安装 cargo-make 和 toml-cli
```
dnf -y install gcc
cargo install cargo-make
cargo install toml-cli
```
7.安装 jq、lz4
```
dnf -y install jq lz4
```
8.加载镜像
玄湾OS采用容器化构建方式，构建过程中需要从 docker.io 拉取镜像，有些环境可能无法访问 docker.io，可以提前加载镜像文件，命令如下：
```
wget https://mirrors.opencloudos.tech/opencloudos-stream/releases/ContainerOS/third-party-images/moby-buildkit-buildx-stable-1.tar
wget https://mirrors.opencloudos.tech/opencloudos-stream/releases/ContainerOS/third-party-images/registry.tar
docker load -i moby-buildkit-buildx-stable-1.tar
docker load -i registry.tar
```

## 构建指导
1.构建镜像
```
cargo make
```
注：

（1）如果构建环境无法访问 docker.io，可以通过设置变量 TENCENT_MIRROR_BASE 改变镜像仓库。例如，使用腾讯云的镜像站，则命令如下：
```
cargo make -e TENCENT_MIRROR_BASE="mirror.ccs.tencentyun.com/opencloudos/opencloudos"
```
另外，还需要将修改为 xbuildsys/etc/oc9/builder/Dockerfile 首行 `# syntax=mirror.ccs.tencentyun.com/docker/dockerfile:1.1.3-experimental`。

（2）如果需要构建其他目标，可以设置环境变量 TENCENT_TARGET，默认为 oc9。例如，构建目标为 rhel，则命令如下：
```
cargo make -e TENCENT_TARGET=rhel
```
构建前需要提前添加目标，可参考 [How-to-add-product.md](https://gitee.com/OpenCloudOS/xuanwan/blob/master/xbuildsys/How-to-add-product.md)。

（3）如果需要构建服务器版本，可以设置环境变量 BUILD_ISO，命令如下：
```
cargo make -e BUILD_ISO=true
```
可以通过添加多个 -e 选项同时设置多个环境变量，环境变量说明，可参考 [How-to-add-product.md](https://gitee.com/OpenCloudOS/xuanwan/blob/master/xbuildsys/How-to-add-product.md)。


2.编译生成的镜像的命名规则为
```
产品名+kernel版本号+构建时间戳-boot.ext4.lz4
产品名+kernel版本号+构建时间戳.qcow2
产品名+kernel版本号+构建时间戳.img.lz4
产品名+kernel版本号+构建时间戳.iso
```
各个文件的说明如下：

`产品名+kernel版本号+构建时间戳-boot.ext4.lz4` 是用于升级的 rootfs;

`产品名+kernel版本号+构建时间戳.qcow2` 是用于部署虚拟机的镜像（qcow2 格式）;

`产品名+kernel版本号+构建时间戳.img.lz4` 是用于部署虚拟机的镜像（raw 格式）；

`产品名+kernel版本号+构建时间戳.iso` 是用于部署服务器的镜像。

3.清除镜像
```
cargo make clean
```

# 自定义产品

玄湾可以通过修改targets目录下的文件,来增加自定义产品,具体请参考
[How-to-add-product.md](https://gitee.com/OpenCloudOS/xuanwan/blob/master/xbuildsys/How-to-add-product.md)

# 自定义应用包

玄湾可以通过修改packages目录下的文件,来增加自定义应用包,具体请参考
[packages/README.md](https://gitee.com/OpenCloudOS/xuanwan/blob/master/xbuildsys/packages/README.md)


# 注意事项

1.当编译过程中，如果出现空间不足，则需要用如下命令，清理一下docker磁盘：
```
docker system prune -a
```

2.在编译C文件的时候,如果有依赖第三方库,此时的第三方库是被安装到%{_cross_sysroot}目录,因此需要在编译的时候添加CFLAGS/LDFLAGS,如下:
```
CFLAGS="-I%{_cross_sysroot}/usr/include -L%{_cross_sysroot}/usr/lib64"
```

3.当在镜像内新增安装包或者文件时，需要修改targets/<product>/Cargo.toml调整镜像大小，否则会出现镜像空间不足的情况。
```
打开Cargo.toml，找到image-size， 并修改为对应的大小。
```

4.修改packages/< package >/Cargo.toml文件后，需要执行
```
cd packages; cargo update; cd packages

注： 不执行，会出现error: the lock file packages/Cargo.lock needs to be updated but --locked was passed to prevent this
```

5.出现错误 ERROR: failed to do request: Head "https://registry-1.docker.io/v2/docker/dockerfile/manifests/1.1.3-experimental": net/http: TLS handshake timeout
```
打开/etc/docker/daemon.json文件,修改为如下(如果不存在,需要创建一个新的)
{
    "experimental": false,
    "insecure-registries": [
        "mirrors.tencent.com", // 或者这里修改成其他可用mirrors
    ]
}
修改后执行systemctl restart docker,再重新执行cargo make即可.
```

