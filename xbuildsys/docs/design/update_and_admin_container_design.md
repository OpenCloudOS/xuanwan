# xuanwanOS双分区升级及运维容器方案
## 一、xuanwanOS拟开发主要特性

* 云原生OS作为k8s work节点host
* 根分区只读
* 支持/etc配置目录读写
* 支持持久化分区读写
* 支持双分区升级
* 支持运维容器

## 二、双分区升级及运维容器总方案
### 2.1 方案草图
![](./figures/image.1.png)

**k8s升级流程：kubectl  -> kube-apiserver -> up-operator -> kube-apiserver  -> up-proxy ->  up-agent （优先开发）**

**k8s运维容器流程：kubectl -> kube-apiserver -> kubelet -> container-runtime -> admin-container         （优先开发）**

命令行升级流程：Apiclient -> Apiserver -> Job ：Up-agent （后期开发）

命令行运维容器流程：Apiclient -> Apiserver -> Admin-container (后期开发)

配置设置流程：Apiclient -> Apiserver -> Job：Setting （后期开发）

**说明** 优先实现k8s升级方案主要有三方面考虑:

* 通过k8s双分区升级是主要的升级场景
* 通过k8s双分区升级对用户来说更友好
* 易于实现原型验证

### 2.2 组件列表
![](./figures/image.2.png)



## 三、k8s双分区升级方案流程
### 3.1 代码架构
双分区升级对接k8s架构图
![](./figures/image.3.png)
如图所示，双分区升级主要组件包括up-operator和up-agent
* up-operator：全局的容器 OS 管理器，持续查看所有work节点的容器 OS 版本信息，并根据用户配置的信息控制同时进行升级的节点个数，并标记准备升级的节点。主要实现了调谐(Reconciler)逻辑，以deplyment形式运行于master节点


* up-agent：单节点的OS管理器，以daemonset形式运行于work节点。它主要有2个异步线程，主线程与kube-apiserver通信，接收apiserver查看当前节点的容器 OS 版本信息。如果当前节点被 up-operator 标记为准备升级的节点后，锁定节点并驱逐 pod，同时启动异步线程执行升级任务，下载容器OS镜像，配置升级config并重启当前work节点

### 3.2 实现分析
* operator为自定义控制器，实现Reconciler接口，服务注册接口，调用controller-runtime的Get、List、Delete、Update方法下发升级任务
![](./figures/image.4.png)

当用户执行deplyment时，apiserver更新work节点中OS的期望状态，这样同步循环(sync loop)会检测到work节点实际状态与期望状态不一致，因此触发控制器下发状态同步信息，实际上时通过k8s内部的订阅-发布机制实现。

![](./figures/image.5.png)

* agent对接master并实现work节点的具体逻辑。需要实现Reconcile回调、注册控制器和实现server逻辑

主线程实现实现Reconcile回调：获取OSinstance，验证OSinstance，匹配OSinstance，刷新k8s cluster，启动异步线程执行升级

异步线程实现具体的升级：配置升级参数，下载容器OS镜像、调用reboot syscall
