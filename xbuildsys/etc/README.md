自定义产品
=================

# 添加自定义产品配置

新增一个玄湾产品, 比如叫 example, 涉及如下 etc 配置:

- etc/common 为通用配置文件；
- etc/example 为产品example专用配置文件，待修改文件如下：

1. 新增 etc/example 文件夹，具体内容可参照 oc9;

2. 修改 etc/example/rpmconfig/example-arch文件, 比如 oc9-x86_64. 该文件用于配置本地yum源的RPM包列表, 可参考 etc/rpmconfig/README.md 
注：增加待下载RPM包后, targets/Cargo.toml中image-size大小需进行适配调整, 避免空间不足.

3. 在etc/example/yum.repos.d/路径新增repo文件, 将从 repo 文件指定的Yum源下载 example-arch 中的RPM包.

4. 修改 etc/example/dnf/vars/releasever值, 该值对应repo文件中的 $releasever 变量(若有).

此后, 工程配置文件就已经被成功添加到玄湾.
