#!/bin/bash

function umount_dir()
{
    local _path=$1

    [ -z "$_path" ] && return

    findmnt $_path >/dev/null 2>&1 || return
    for i in `seq 1 6`; do
        umount $_path && break
        sleep 1
    done

    findmnt $_path >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        log_error "Failed to umount $_path."
        do_exit 1
    fi
}

function detach_dev()
{
    local _path=$1
    local _loop_dev

    [ -z "$_path" ] && return

    _loop_dev=$(losetup -j $_path 2>/dev/null | awk -F':' '{print $1}')
    if [ ! -z "$_loop_dev" ]; then
        for i in `seq 1 6`; do
            losetup --detach $_loop_dev && break
            sleep 1
        done

        _loop_dev=$(losetup -j $_path 2>/dev/null | awk -F':' '{print $1}')
        if [ ! -z "$_loop_dev" ]; then
            log_error "Failed to detach $_path from $_loop_dev."
            do_exit 1
        fi
    fi
}

function remove_path()
{
    local path="$1"

    [[ -z "$path" || "$path" == "/" ]] && return

    [[ ! -d "$path" && ! -f "$path" ]] && return

    eval rm -rf "$path"
    if [[ $? -ne 0 ]]; then
        log_warn2 "Failed to remove path [$path]."
    fi
}

function do_clean()
{
    umount_dir $ROOTFS_SRC
    umount_dir $ROOTFS_DST
    detach_dev $ROOTFS_PATH

    remove_path "$ROOTFS_SRC"
    remove_path "$ROOTFS_DST"
    remove_path "$ROOTFS_IMG"
}

function do_exit()
{
    local _ret=$1
    do_clean

    while true; do
        read -p "Failed to install OS (type 'exit' to restart): " cmd
        if [ "$cmd" == "exit" ]; then
            log_warn "$(date +'%F %T') Restarting installer..."
            exit $_ret
        fi
        echo "$(date +'%F %T') You entered: $cmd"
    done
}

function check_ret()
{
    local _msg=$1
    local _ret=$2

    if [ 0"$2" -ne 0 ]; then
        log_error "Failed to $_msg."
        do_exit 1
    fi
}

function check_format()
{
	local _format=$1

    case $_format in
        application/octet-stream | application/x-lz4)
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

function get_rootfs_size()
{
    if [ -z "$ROOTFS_PATH" ] && [ ! -f "$ROOTFS_PATH" ]; then
        log_error "Failed to get rootfs."
        do_exit 1
    fi

    remove_path "$ROOTFS_IMG"

    case $ROOTFS_FMT in
        application/x-lz4)
            log_info2 "Decompress $ROOTFS_PATH to $ROOTFS_IMG."
            lz4 -d $ROOTFS_PATH $ROOTFS_IMG
            if [ $? -ne 0 ]; then
                log_error "Failed to decompress $ROOTFS_PATH, ret $?."
                do_exit 1
            fi
            ;;
        application/octet-stream)
            log_info2 "Copy $ROOTFS_PATH to $ROOTFS_IMG."
            cp -ap $ROOTFS_PATH $ROOTFS_IMG
            if [ $? -ne 0 ]; then
                log_error "Failed to copy $ROOTFS_PATH, ret $?."
                do_exit 1
            fi
            ;;
        *)
            log_error "Invalid rootfs $ROOTFS_PATH."
            do_exit 1
            ;;
    esac

    case $ROOTFS_LOCATION in
        http://* | https://*)
            rm -f $ROOTFS_PATH
            ;;
        *)
            ;;
    esac

    if [ ! -f "$ROOTFS_IMG" ]; then
        log_error "$ROOTFS_IMG not found."
        do_exit 1
    fi

    ROOTFS_PATH=$ROOTFS_IMG
    ROOTFS_SIZE=$(stat --format "%s" $ROOTFS_PATH)
    # 6M for boot, 2M for gap, 512M at least for persist
    MINIMAL_DISK_SIZE=$(($ROOTFS_SIZE*2+520*1024*1024))
    log_info2 "The size of $ROOTFS_PATH is $(($ROOTFS_SIZE/1024/1024))M."
    echo ""
}

function setup_boot_gap()
{
    local _index=$1
    local _gap_offs

    _gap_offs=$(fdisk -l $TARGET_DISK | grep ${DISKPART_PREFIX}${_index} | awk '{print ($3+1)/2048}')
    (   echo -e -n "\x34\x23\x24\x59"; \
        echo -e -n "\xaa\x55\x55\xaa"; \
    ) | dd of=$TARGET_DISK bs=1M seek=$_gap_offs count=1 conv=sync

    check_ret "setup boot gap $index" $?
}

function create_partition()
{
    local _rootpart_size_mib=$(($ROOTPART_SIZE/1024/1024))
    local _disklabel=msdos

    log_info "Begin to create partition."
    log_info2 $DIVIDER
    log_info2 "***************************** Create Partition ********************************"

    parted --script "$TARGET_DISK" mklabel $_disklabel
    check_ret "make label" $?
    parted --script "$TARGET_DISK" mkpart primary fat16 1MiB 6MiB
    check_ret "create partition 1" $?
    parted --script "$TARGET_DISK" mkpart primary ext4  6MiB $(($_rootpart_size_mib+6))MiB
    check_ret "create partition 2" $?
    parted --script "$TARGET_DISK" mkpart primary ext4  $(($_rootpart_size_mib+7))MiB $(($_rootpart_size_mib*2+7))MiB
    check_ret "create partition 3" $?
    parted --script "$TARGET_DISK" mkpart primary ext4  $(($_rootpart_size_mib*2+8))MiB 100%
    check_ret "create partition 4" $?
    log_info "Partitions created successfully."
    echo ""

    log_info "Begin to setup boot gap."
    case $TARGET_DISK in
        /dev/nvme* | /dev/md*)
            DISKPART_PREFIX="${TARGET_DISK}p"
            ;;
        *)
            DISKPART_PREFIX="${TARGET_DISK}"
            ;;
    esac

    # setup boot gapa and gapb
    setup_boot_gap 2
    setup_boot_gap 3
    log_info "Boot gap data set successfully."
    echo ""
}

function make_filesystem()
{
    log_info "Begin to make filesystem."
    log_info2 $DIVIDER
    log_info2 "***************************** Make Filesystem *********************************"

    # make filesystem for root partition
    mkfs.ext4 -O ^orphan_file -O ^metadata_csum_seed -F -m 0 -b 4096 -L ROOTA   ${DISKPART_PREFIX}2
    check_ret "make ext4 for root partition" $?

    # make filesystem for persistent partition
    mkfs.ext4 -O ^orphan_file -O ^metadata_csum_seed -F -m 0 -b 4096 -L PERSIST ${DISKPART_PREFIX}4
    check_ret "make ext4 for persistent partition" $?

    log_info "Filesystem created successfully."
    echo ""
}

function install_rootfs()
{
    local _loop_dev
    local _uuid
    local _partuuid

    log_info "Begin to install rootfs."
    log_info2 $DIVIDER
    log_info2 "****************************** Install Rootfs *********************************"

    # Attach rootfs.img to loop device
    _loop_dev=$(losetup --find --show $ROOTFS_PATH)
    for i in `seq 1 6`; do
        udevadm settle --timeout 300
        _loop_dev_attach=$(losetup -j $ROOTFS_PATH | awk -F':' '{print $1}')
        [ "${loop_dev_attach}" == "${loop_dev}" ] && break
        sleep 1
    done

    if [ "$_loop_dev_attach" != "$_loop_dev" ]; then
        log_error "Failed to attach $ROOTFS_PATH to loop device."
        do_exit 1
    fi

    mount $_loop_dev $ROOTFS_SRC
    check_ret "mount $_loop_dev to $ROOTFS_SRC" $?

    mount ${DISKPART_PREFIX}2 $ROOTFS_DST
    check_ret "mount ${DISKPART_PREFIX}2 to $ROOTFS_DST" $?
    rm -rf "$ROOTFS_DST/lost+found"

    # Copy rootfs to target disk root partition
    log_info2 "Copy $ROOTFS_SRC/. to $ROOTFS_DST."
    cp -arp $ROOTFS_SRC/. $ROOTFS_DST
    check_ret "copy $ROOTFS_SRC/. to $ROOTFS_DST" $?

    _root_uuid=$(blkid -s UUID -o value ${DISKPART_PREFIX}2)
    _root_partuuid=$(blkid -s PARTUUID -o value ${DISKPART_PREFIX}2)

    # setup bootloader
    log_info2 "Setup uuid and partuuid in grub.cfg."
    sed -i "s/\(--set=root \).*/\1$_root_uuid/g" $ROOTFS_DST/boot/grub2/grub.cfg
    sed -i "s/\(root=PARTUUID=\)[^ ]*/\1$_root_partuuid/g" $ROOTFS_DST/boot/grub2/grub.cfg

    log_info2 "Install bootloader to $TARGET_DISK."
    grub2-install --modules="normal ext2 echo linux bootswitch" \
        --target=i386-pc --directory="$ROOTFS_DST/usr/lib/grub/i386-pc" \
        --boot-directory="$ROOTFS_DST/boot" --root-directory="$ROOTFS_DST" "$TARGET_DISK"
    check_ret "setup bootloader" $?

    # setup fstab
    sed -i "s/\(UUID=\)[^ ]*/\1$_root_uuid/g" $ROOTFS_DST/etc/fstab

    umount_dir $ROOTFS_SRC
    umount_dir $ROOTFS_DST
    detach_dev $ROOTFS_PATH
    sync
    log_info "Rootfs installed on $TARGET_DISK successfully."
    echo ""
}

function setup_persist_data()
{
    log_info "Begin to setup persist data."
    mount ${DISKPART_PREFIX}4 $ROOTFS_DST
    check_ret "mount ${DISKPART_PREFIX}4 to $ROOTFS_DST" $?
    rm -rf "$ROOTFS_DST/lost+found"

    mkdir -p $ROOTFS_DST/{usr,var/lib,etc,usrwork,etcwork,varwork,data,containerd}
    ln -sf /persist/containerd $ROOTFS_DST/var/lib/containerd

    umount_dir $ROOTFS_DST
    sync
    log_info "Persist data set successfuly."
    echo ""
}

function create_file()
{
    local _conf=$1
    local _dir=$(dirname $_conf)

    [ -f "$_conf" ] && rm -f $_conf
    [ ! -d "$_dir" ] && mkdir -p $_dir

    touch $_conf

    return $?
}

function set_network()
{
    local _interface=$1
    local _bootproto=$2
    local _address_netmask=$3
    local _gateway=$4
    local _dnslist=$5
    local _conf=$ROOTFS_DST/etc/sysconfig/network-scripts/ifcfg-$_interface

    create_file $_conf
    check_ret "create $_conf" $?

    echo "DEVICE=$_interface" >> $_conf
    echo "BOOTPROTO=$_bootproto" >> $_conf
    echo "ONBOOT=yes" >> $_conf

    if [ "$_bootproto" == "static" ]; then
        echo "IPADDR=$(ipcalc -a $_address_netmask 2>/dev/null | awk -F'=' '{print $2}')" >> $_conf
        echo "NETMASK=$(ipcalc -m $_address_netmask 2>/dev/null | awk -F'=' '{print $2}')" >> $_conf
        echo "GATEWAY=$_gateway" >> $_conf
    fi

    _dns_cnt=1
    for _dns in $_dnslist; do
        echo "DNS$_dns_cnt=$_dns" >> $_conf
        _dns_cnt=$(($_dns_cnt+1))
    done
}

function set_systemd_networkd()
{
    local _interface=$1
    local _bootproto=$2
    local _address_netmask=$3
    local _gateway=$4
    local _dnslist=$5
    local _index=$6
    local _conf=$ROOTFS_DST/etc/systemd/network/${_index}0-$_interface.network

    create_file $_conf
    check_ret "create $_conf" $?

    echo "[Match]" >> $_conf
    echo "Name=$_interface" >> $_conf
    echo "" >> $_conf
    echo "[Network]" >> $_conf

    if [ "$_bootproto" == "static" ]; then
        echo "Address=$_address_netmask" >> $_conf
        echo "Gateway=$_gateway" >> $_conf
    else
        echo "DHCP=yes" >> $_conf
    fi

    for _dns in $_dnslist; do
        echo "DNS=$_dns" >> $_conf
    done
}

function set_networkmanager()
{
    local _interface=$1
    local _bootproto=$2
    local _address_netmask=$3
    local _gateway=$4
    local _dnslist=$5

    nmcli connection delete $_bootproto-${_interface} >/dev/null 2>&1
    if [ "$_bootproto" == "static" ]; then
        nmcli connection add type ethernet \
            ifname $_interface \
            con-name $_bootproto-$_interface \
            ipv4.addresses $_address_netmask \
            ipv4.gateway $_gateway \
            ipv4.dns "$_dnslist" \
            ipv4.method manual \
            autoconnect yes
    else
        nmcli connection add type ethernet \
            ifname $_interface \
            con-name $_bootproto-$_interface \
            ipv4.dns "$_dnslist" \
            ipv4.method auto \
            autoconnect yes
    fi
    check_ret "run command nmcli for $_interface" $?

    for i in `seq 1 6`; do
        if [ -f /etc/NetworkManager/system-connections/$_bootproto-${_interface}.nmconnection ]; then
            break
        fi
        sleep 1
    done

    [ ! -d $ROOTFS_DST/etc/NetworkManager/system-connections ] && mkdir -p $ROOTFS_DST/etc/NetworkManager/system-connections

    mv /etc/NetworkManager/system-connections/$_bootproto-${_interface}.nmconnection \
        $ROOTFS_DST/etc/NetworkManager/system-connections
    check_ret "create networkmanager file for $_interface" $?

    nmcli connection delete $_bootproto-${_interface} >/dev/null 2>&1
}

function check_ip()
{
    local _ip="$1"
    ipcalc -c $_ip 2>/dev/null
    if [ $? -ne 0 ]; then
        log_warn2 "Invalid ip [$_ip]."
        return 1
    fi

    return 0
}

function check_netinfo()
{
    local _interface=$1
    local _bootproto=$2
    local _address_netmask=$3
    local _gateway=$4
    local _dnslist=$5
    declare -A _dnsmap

    if [ -z "$_interface" ]; then
        log_warn2 "Interface is empty"
        return 1
    fi

    ip link show $_interface >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        log_warn2 "The interface [$_interface] does not exist."
    fi

    case $_bootproto in
        static)
            check_ip "$_address_netmask" || return 1
            check_ip "$_gateway" || return 1
            ;;
        dhcp)
            if [[ ! -z "$_address_netmask" || ! -z "$_gateway" ]]; then
                log_warn2 "The address_netmask and gateway should be empty in dhcp mode."
                return 1
            fi
            ;;
        *)
            log_warn2 "Invalid bootproto [$_bootproto]."
            return 1
            ;;
    esac

    for _dns in $_dnslist; do
        check_ip "$_dns"
        if [ $? -ne 0 ]; then
            log_warn2 "Invalid dns [$_dns] in [$_dnslist]."
            return 1
        fi
        if [[ -n "${_dnsmap["$_dns"]}" ]]; then
            log_warn2 "Duplicate dns [$_dns]."
            return 1
        fi
        _dnsmap["$_dns"]=1
    done

    return 0
}
