#!/bin/bash

function get_target_disk_interactive()
{
    local all_disks=($(lsblk -d -o NAME,TYPE | awk '$2 == "disk" {print $1}'))
    local _target_disk
	local _disk_size

    log_info "Begin to get installation disk."
    log_info2 $DIVIDER
    log_info2 "**************************** Installation Disk ********************************"

    while true; do
        log_info2 "All available disks are as follows:"
        log_info2 "[${all_disks[*]}]"

        read -p "Please select a disk from above to install OS (default: ${all_disks[0]}): " _target_disk
        if [ -z "$_target_disk" ]; then
            _target_disk=${all_disks[0]}
            log_warn2 "Empty input, using default disk /dev/${all_disks[0]}."
        fi

        if [ -b /dev/$_target_disk ]; then
            _disk_size=$(lsblk -b -d -o SIZE /dev/$_target_disk | grep -v SIZE)
            if [ 0"$_disk_size" -ge 0"$MINIMAL_DISK_SIZE" ]; then
                TARGET_DISK="/dev/$_target_disk"
                DISK_SIZE=$_disk_size
                break
            else
                log_warn2 "The size of /dev/$_target_disk ($(($_disk_size/1024/1024))M) is less than the minimum requirement ($(($MINIMAL_DISK_SIZE/1024/1024))M)."
            fi
        else
            log_warn2 "The disk /dev/$_target_disk not found."
        fi

        echo ""
    done
    log_info "The OS will be installed on $TARGET_DISK."
    echo ""
}

function get_rootfs_location_interactive()
{
    local _rootfs_location
    local _file_format

    log_info "Begin to get rootfs location."
    log_info2 $DIVIDER
    log_info2 "***************************** Rootfs Location *********************************"

    while true; do
        read -p "Please give rootfs location (default: $DEFAULT_ROOTFS_LOCATION): " _rootfs_location
        if [ -z "$_rootfs_location" ]; then
            _rootfs_location=$DEFAULT_ROOTFS_LOCATION
            log_warn2 "Empty input, using default rootfs location: $DEFAULT_ROOTFS_LOCATION."
        fi

        case $_rootfs_location in
            file:///*)
                if [ -f ${_rootfs_location#file://} ]; then
                    _file_format=$(file -L --brief --mime-type "${_rootfs_location#file://}")
                    check_format $_file_format
                    if [ $? -eq 0 ]; then
                        ROOTFS_PATH="${_rootfs_location#file://}"
                        ROOTFS_FMT="$_file_format"
                        ROOTFS_LOCATION=$_rootfs_location
                        break
                    fi
                    log_warn2 "Invalid file format ($_file_format), $_rootfs_location."
                else
                    log_warn2 "File ${_rootfs_location#file://} not found."
                fi
                echo ""
                ;;
            http://* | https://*)
                [ -f /home/${_rootfs_location##*/} ] && rm -f /home/${_rootfs_location##*/}
                curl --insecure --output /home/${_rootfs_location##*/} --fail "$_rootfs_location"
                if [ -f /home/${_rootfs_location##*/} ]; then
                    _file_format=$(file -L --brief --mime-type "/home/${_rootfs_location##*/}")
                    check_format $_file_format
                    if [ $? -eq 0 ]; then
                        ROOTFS_PATH="/home/${_rootfs_location##*/}"
                        ROOTFS_FMT="$_file_format"
                        ROOTFS_LOCATION=$_rootfs_location
                        break
                    fi
                    log_warn2 "Invalid file format ($_file_format), $_rootfs_location."
                    rm -f /home/${_rootfs_location##*/}
                else
                    log_warn2 "Failed to download remote rootfs file from $_rootfs_location."
                fi
                echo ""
                ;;
            *)
                log_warn2 "Invalid protocol, please use 'file:///', 'http://' or 'https://'."
                echo ""
                ;;
        esac
    done
    log_info "Rootfs location: $_rootfs_location, rootfs path: $ROOTFS_PATH."
    echo ""
}

function get_rootpart_size_interactive()
{
    local _rootpart_size
    local _unit
    local _size

    log_info "Begin to get root partition size."
    log_info2 $DIVIDER
    log_info2 "*************************** Root Partition Size *******************************"

    while true; do
        read -p "Please set root part size (at least $((${ROOTFS_SIZE}/1024/1024))M, unit: B/K/M/G): " _rootpart_size
        if [ -z "$_rootpart_size" ]; then
            log_warn2 "Empty input, using default size $((${ROOTFS_SIZE}/1024/1024))M."
            ROOTPART_SIZE=${ROOTFS_SIZE}
            break
        fi

        echo "$_rootpart_size" | grep -q -E "^[0-9]+[bBkKmMgG]?$"
        if [ $? -ne 0 ]; then
            log_warn2 "Invalid input, please try again."
            continue
        fi

        _unit=${_rootpart_size//[0-9]/}
        _size=${_rootpart_size//[!0-9]/}
        case $_unit in
            K | k)
                _rootpart_size=$((${_size}*1024))
                ;;
            M | m)
                _rootpart_size=$((${_size}*1024*1024))
                ;;
            G | g)
                _rootpart_size=$((${_size}*1024*1024*1024))
                ;;
            *)
                _rootpart_size=${_size}
                ;;
        esac

        if [ "$(echo "0$_rootpart_size < 0$ROOTFS_SIZE" | bc)" -eq 1 ]; then
            log_warn2 "Input size $(($_rootpart_size/1024/1024))M is less than $(($ROOTFS_SIZE/1024/1024))M."
            echo ""
            continue
        fi

        _size=$(($_rootpart_size*2+520*1024*1024))
        if [ "$(echo "0$_size > 0$DISK_SIZE" | bc)" -eq 1 ]; then
            log_warn2 "Input size ($((_rootpart_size/1024/1024))M * 2 + 520M) is greater than the disk size $(($DISK_SIZE/1024/1024))M."
            echo ""
            continue
        fi
        ROOTPART_SIZE=$_rootpart_size
        break

    done
    log_info "Root partition size is set to $(($ROOTPART_SIZE/1024/1024))M."
    echo ""
}

function user_setting_interactive()
{
    local _first_passwd
    local _second_passwd
    local _username
    local _confirm

    log_info "Begin to set user."
    log_info2 $DIVIDER
    log_info2 "******************************* User Setting **********************************"

    mount ${DISKPART_PREFIX}2 $ROOTFS_DST
    check_ret "mount ${DISKPART_PREFIX}2 to $ROOTFS_DST" $?

    while true; do
        read -sp "Please input password for root: " _first_passwd
        echo ""
        [ -z "$_first_passwd" ] && continue
        read -sp "Comfirm your password: " _second_passwd
        echo ""

        if [ "$_first_passwd" != "$_second_passwd" ]; then
            log_warn2 "Not matched, please try again."
            continue
        fi

        chroot $ROOTFS_DST /bin/bash -c "echo 'root:'$_first_passwd | chpasswd"
        if [ $? -ne 0 ]; then
            log_warn2 "Failed to set password for root, please try again."
            continue
        fi
        log_info2 "Password for root set successfully."
        break
    done

    while true; do
        read -p "Continue to add user? (yes/no): " _confirm
        [ "$_confirm" == "no" ] && break

        read -p "Please input username: " _username
        if [ ! -z "$_username" ]; then
            read -p "Add new user [$_username]? (yes/no): " _confirm
            [ "$_confirm" == "no" ] && continue

            chroot $ROOTFS_DST /bin/bash -c "useradd -m $_username"
            if [ $? -ne 0 ]; then
                log_warn2 "Failed to add user [$_username], please try again."
                continue
            fi

            while true; do
                read -sp "Please input password for [$_username]: " _first_passwd
                echo ""
                [ -z "$_first_passwd" ] && continue
                read -sp "Comfirm your password: " _second_passwd
                echo ""

                if [ "$_first_passwd" != "$_second_passwd" ]; then
                    log_warn2 "Not matched, please try again."
                    continue
                fi

                chroot $ROOTFS_DST /bin/bash -c "echo $_username:$_first_passwd | chpasswd"
                if [ $? -ne 0 ]; then
                    log_warn2 "Failed to set password for [$_username], please try again."
                    continue
                fi
                log_info2 "Password for [$_username] set successfully."
                break
            done

            log_info2 "New user [$_username] added successfully."
        else
            log_warn2 "Empty input, please try again."
        fi
    done
    echo ""

    umount $ROOTFS_DST
    sync
}

function network_setting_interactive()
{
    local all_interfaces=($(ip -o link show | awk -F': ' '{print $2}' | grep -v "^lo$"))
    local _confirm
    local _network_manager_method
    local _interface
    local _bootproto
    local _address_netmask
    local _address
    local _netmask
    declare -A _dnslist

    log_info "Begin to set network."
    log_info2 $DIVIDER
    log_info2 "***************************** Network Setting *********************************"

    while true; do
        log_info2 "Valid network manager method: n(network), sn(systemd-networkd), nm(NetworkManager)."
        read -p "Please set network manager method: " _network_manager_method
        case $_network_manager_method in
            n)
                NETWORK_MANAGER_METHOD="network"
                ;;
            sn)
                NETWORK_MANAGER_METHOD="systemd-networkd"
                ;;
            nm)
                NETWORK_MANAGER_METHOD="networkmanager"
                ;;
            *)
                log_warn2 "Invalid network manager method [$_network_manager_method]."
                continue
        esac
        break
    done

    while true; do
        read -p "Continue to set network? (yes/no): " _confirm
        [ "$_confirm" == "no" ] && break

        log_warn2 "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        log_warn2 "********************************* NOTICE *************************************"
        log_warn2 "Please confirm that the cmdline of final installation rootfs cmdline includes "
        log_warn2 "'biosdevname=0 net.ifnames=0'. The installation ISO image cmdline needs to be "
        log_warn2 "consistent with the final rootfs; otherwise, the network interface names in the" 
        log_warn2 "configuration files may not match, leading to invalid network configurations."
        log_warn2 "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

        log_info2 "All available network interfaces are as follows:"
        log_info2 "[${all_interfaces[*]}]"
        read -p "Please select the network interface to set: " _interface
        [ -z "$_interface" ] && continue
        ip link show $_interface
        if [ $? -ne 0 ]; then
            log_warn2 "The interface [$_interface] does not exist."
            continue
        fi

        if [[ -n "${INTERFACES_ALREADY_SET[$_interface]}" ]]; then
            log_warn2 "The interface [$_interface] has already been set."
            continue
        fi

        # set network for selected interface, include bootproto, address, netmask, gateway and dns
        while true; do
            read -p "Please set bootproto (dhcp or static): " _bootproto
            case $_bootproto in
                static)
                    # 1. get address and netmask
                    while true; do
                        read -p "Please set address and netmask (e.g, 192.168.122.101/24): " _address_netmask
                        check_ip "$_address_netmask" || continue
                        break
                    done

                    # 2. get gateway
                    while true; do
                        read -p "Please set gateway (e.g, 192.168.122.1): " _gateway
                        check_ip "$_gateway" || continue
                        break
                    done

                    # 3. get dns
                    while true; do
                        read -p "Continue to add dns? (yes/no): " _confirm
                        [ "$_confirm" == "no" ] && break

                        read -p "Please set dns (e.g, 8.8.8.8): " _dns
                        check_ip "$_dns" || continue

                        if [[ -n "${_dnslist[$_dns]}" ]]; then
                            log_warn2 "Duplicate dns $_dns, please try again."
                            continue
                        fi

                        _dnslist["$_dns"]=1
                    done

                    log_info2 "  proto: $_bootproto"
                    log_info2 "address: $_address_netmask"
                    log_info2 "gateway: $_gateway"
                    log_info2 "    dns: ${!_dnslist[*]}"
                    echo ""
                    ;;
                dhcp)
                    # get dns
                    while true; do
                        read -p "Continue to add dns? (yes/no): " _confirm
                        [ "$_confirm" == "no" ] && break

                        read -p "Please set dns (e.g, 8.8.8.8): " _dns
                        check_ip "$_dns" || continue

                        if [[ -n "${_dnslist[$_dns]}" ]]; then
                            log_warn2 "Duplicate dns $_dns, please try again."
                            continue
                        fi

                        _dnslist["$_dns"]=1
                    done

                    log_info2 "  proto: $_bootproto"
                    log_info2 "    dns: ${!_dnslist[*]}"
                    echo ""
                    ;;
                *)
                    log_warn2 "Invalid bootproto [$_bootproto]."
                    continue
                    ;;
            esac

            mount ${DISKPART_PREFIX}2 $ROOTFS_DST
            check_ret "mount ${DISKPART_PREFIX}2 to $ROOTFS_DST" $?

            case $NETWORK_MANAGER_METHOD in
                network)
                    set_network "$_interface" "$_bootproto" "$_address_netmask" "$_gateway" "${!_dnslist[*]}"
                    ;;
                systemd-networkd)
                    set_systemd_networkd "$_interface" "$_bootproto" "$_address_netmask" "$_gateway" "${!_dnslist[*]}" "$((${#INTERFACES_ALREADY_SET[@]}+1))"
                    ;;
                networkmanager)
                    set_networkmanager "$_interface" "$_bootproto" "$_address_netmask" "$_gateway" "${!_dnslist[*]}"
                    ;;
                *)
                    log_warn2 "Invalid network manager method [$NETWORK_MANAGER_METHOD]."
                    break
                    ;;
            esac
            umount_dir $ROOTFS_DST
            sync

            # add interface to already_set list
            INTERFACES_ALREADY_SET["$_interface"]=$((${#INTERFACES_ALREADY_SET[@]}+1))
            break
        done
    done

}

function post_install_interactive()
{
    local _cmd

    while true; do
        read -p "Please type 'reboot' to finish installation: " _cmd
        if [ "$_cmd" == "reboot" ]; then
            reboot -f
            if [ $? -ne 0 ]; then
                log_error "Failed to reboot."
            fi
            break
        fi
        echo "$(date +'%F %T') You entered [$_cmd]"
    done
}

function interactive_mode()
{
    get_rootfs_location_interactive
    get_rootfs_size
    get_target_disk_interactive
    get_rootpart_size_interactive
    create_partition
    make_filesystem
    install_rootfs
    setup_persist_data
    user_setting_interactive
    network_setting_interactive
    do_clean
    post_install_interactive
}
