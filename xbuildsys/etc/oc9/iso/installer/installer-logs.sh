#!/bin/bash

LOGFILE=/tmp/installer.log

function log_init()
{
	if [ ! -f $LOGFILE ]; then
		touch $LOGFILE
	fi

	echo "---------------------------- $(date +'%F %T') Starting to log for installer ---------------------------" >> $LOGFILE
}

function log_info()
{
	local _msg=$1

	if [ ! -z "$_msg" ]; then
		echo -e "$(date +'%F %T') [info] $_msg" | tee -a $LOGFILE
	fi
}

function log_info2()
{
	local _msg=$1

	if [ ! -z "$_msg" ]; then
	    echo "$_msg"
		echo -e "$(date +'%F %T') [info] $_msg" >> $LOGFILE
	fi
}


function log_warn()
{
	local _msg=$1

	if [ ! -z "$_msg" ]; then
		echo -e "$(date +'%F %T') [\033[1;33mwarn\033[0m] $_msg" | tee -a $LOGFILE
	fi
}

function log_warn2()
{
	local _msg=$1

	if [ ! -z "$_msg" ]; then
		echo -e "\033[1;33m$_msg\033[0m"
		echo -e "$(date +'%F %T') [\033[1;33mwarn\033[0m] $_msg" >> $LOGFILE
	fi
}

function log_error()
{
	local _msg=$1

	if [ ! -z "$_msg" ]; then
		echo -e "$(date +'%F %T') [\033[1;31merror\033[0m] $_msg" | tee -a $LOGFILE
	fi
}

log_init
