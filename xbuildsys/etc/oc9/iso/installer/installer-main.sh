#!/bin/bash

INTERACTIVE_MODE=""
TARGET_DISK=""
DISK_SIZE=""
DISKPART_PREFIX=""
MINIMAL_DISK_SIZE=""
ROOTPART_SIZE=""
ROOTFS_LOCATION=""
ROOTFS_FMT=""
ROOTFS_PATH=""
ROOTFS_SIZE=""
NETWORK_MANAGER_MEHTOD=""

DEFAULT_ROOTFS_LOCATION="file:///run/install/repo/rootfs.ext4.lz4"
DEFAULT_NETWORK_METHOD="systemd-networkd"

INSTALL_CFG="/tmp/install.cfg"
ROOTFS_IMG="/tmp/rootfs.img"
CMDLINE="/proc/cmdline"

declare -A INTERFACES_ALREADY_SET

DIVIDER="==============================================================================="
ROOTFS_SRC=$(mktemp -d)
ROOTFS_DST=$(mktemp -d)

WORKDIR=$(cd $(dirname $0); pwd)

# Ignore 'ctrl+C' and 'ctr+Z'
trap '' SIGINT
trap '' SIGTSTP

source $WORKDIR/installer-logs.sh
source $WORKDIR/installer-functions.sh
source $WORKDIR/installer-interactive.sh
source $WORKDIR/installer-non-interactive.sh

function main()
{
    log_info "Starting XuanwanOS Installer..."

	# Disable printing messages to console
	dmesg -D

    cat $CMDLINE 2>/dev/null | grep -q "inst.interactive"
    if [ $? -eq 0 ]; then
        INTERACTIVE_MODE="true"
        log_info "Entering interactive mode..."
        interactive_mode
    else
        INTERACTIVE_MODE="false"
        log_info "Entering non-interactive mode..."
        non_interactive_mode
    fi

    while true; do
        read -p "Enter command (type 'exit' to quit): " cmd
        if [ "$cmd" == "exit" ]; then
            echo "$(date +'%F %T') Exiting script."
            break
        fi
        echo "$(date +'%F %T') You entered: $cmd"
    done
}

main
