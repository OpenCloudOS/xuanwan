#!/bin/bash

function get_install_cfg()
{
    local _install_cfg

    [ -f "$INSTALL_CFG" ] && rm -f $INSTALL_CFG

    log_info "Begin to get install configuration file."
    log_info2 $DIVIDER
    log_info2 "************************** Install Configuration ******************************"

    # get install.cfg from cmdline
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.cfg= ]]; then
            _install_cfg=${param#inst.cfg=}
        fi
    done

    if [ -z "$_install_cfg" ]; then
        log_warn2 "Installation configuration file not set in $CMDLINE."
    else
        case $_install_cfg in
            file:///*)
                if [ -f ${_install_cfg#file://} ]; then
                    cp -arp ${_install_cfg#file://} $INSTALL_CFG
                    log_info2 "Copy install configuration file ${_install_cfg#file://} to $INSTALL_CFG successfully."
                else
                    log_warn2 "File ${_install_cfg#file://} not found."
                fi
                ;;
            http://* | https://*)
                curl --insecure --output $INSTALL_CFG --fail "$_install_cfg"
                if [ -f $INSTALL_CFG ]; then
                    log_info2 "Download install configuration file from $_install_cfg successfully."
                else
                    log_warn2 "Failed to downaload install configuration file from $_install_cfg."
                fi
                ;;
            *)
                log_warn2 "Invalid install configuration file location [$_install_cfg], valid protocol: 'file:///', 'http://' or 'https://'."
                ;;
        esac
    fi
}

function get_rootfs_location()
{
    local _rootfs_location
    local _file_format
    local old_IFS

    log_info "Begin to get rootfs location."
    log_info2 $DIVIDER
    log_info2 "***************************** Rootfs Location *********************************"

    # get rootfs location from install.cfg
    log_info2 "Getting rootfs location from [$INSTALL_CFG]..."
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^rootfs_location= ]]; then
            _rootfs_location=${param#rootfs_location=}
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    # get rootfs location from cmdline
    log_info2 "Getting rootfs location from [$CMDLINE]..."
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.rootfs_location= ]]; then
            _rootfs_location=${param#inst.rootfs_location=}
        fi
    done

    # use default rootfs location
    if [ -z "$_rootfs_location" ]; then
        _rootfs_location=$DEFAULT_ROOTFS_LOCATION
        log_warn2 "Failed to get rootfs location, using default rootfs location [$DEFAULT_ROOTFS_LOCATION]."
    else
        log_info2 "Rootfs location is [$_rootfs_location]."
    fi

    case $_rootfs_location in
        file:///*)
            if [ -f ${_rootfs_location#file://} ]; then
                _file_format=$(file -L --brief --mime-type "${_rootfs_location#file://}")
                check_format $_file_format
                if [ $? -eq 0 ]; then
                    ROOTFS_PATH="${_rootfs_location#file://}"
                    ROOTFS_FMT="$_file_format"
                    ROOTFS_LOCATION=$_rootfs_location
                else
                    log_error "Invalid file format ($_file_format), $_rootfs_location."
                    do_exit 1
                fi
            else
                log_error "File ${_rootfs_location#file://} not found."
                do_exit 1
            fi
            ;;
        http://* | https://*)
            [ -f /home/${_rootfs_location##*/} ] && rm -f /home/${_rootfs_location##*/}
            curl --insecure --output /home/${_rootfs_location##*/} --fail "$_rootfs_location"
            if [ -f /home/${_rootfs_location##*/} ]; then
                _file_format=$(file -L --brief --mime-type "/home/${_rootfs_location##*/}")
                check_format $_file_format
                if [ $? -eq 0 ]; then
                    ROOTFS_PATH="/home/${_rootfs_location##*/}"
                    ROOTFS_FMT="$_file_format"
                    ROOTFS_LOCATION=$_rootfs_location
                else
                    log_error "Invalid file format ($_file_format), $_rootfs_location."
                    rm -f /home/${_rootfs_location##*/}
                    do_exit 1
                fi
            else
                log_error "Failed to download remote rootfs file from $_rootfs_location."
                do_exit 1
            fi
            ;;
        *)
            log_error "Invalid rootfs location [$_rootfs_location], valid protocol: 'file:///', 'http://' or 'https://'."
            do_exit 1
            ;;
    esac

    log_info "Rootfs path is $ROOTFS_PATH."
}

function get_target_disk()
{
    local all_disks=($(lsblk -d -o NAME,TYPE | awk '$2 == "disk" {print $1}'))
    local _target_disk
    local _disk_size
    local old_IFS

    log_info "Begin to get installation disk."
    log_info2 $DIVIDER
    log_info2 "**************************** Installation Disk ********************************"

    # get target disk from install.cfg
    log_info2 "Getting target disk from [$INSTALL_CFG]..."
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^target_disk= ]]; then
            _target_disk=${param#target_disk=}
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    # get target disk from cmdline
    log_info2 "Getting target disk from [$CMDLINE]..."
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.target_disk= ]]; then
            _target_disk=${param#inst.target_disk=}
        fi
    done

    # use default target disk
    if [ -z "$_target_disk" ]; then
        log_warn2 "Failed to get target disk, using default target disk [/dev/${all_disks[0]}]."
        _target_disk="/dev/${all_disks[0]}"
    fi

    if [ -b $_target_disk ]; then
        _disk_size=$(lsblk -b -d -o SIZE $_target_disk | grep -v SIZE)
        if [ 0"$_disk_size" -ge 0"$MINIMAL_DISK_SIZE" ]; then
            TARGET_DISK="$_target_disk"
            DISK_SIZE=$_disk_size
        else
            log_error "The size of $_target_disk ($(($_disk_size/1024/1024))M) is less than the minimum requirement ($(($MINIMAL_DISK_SIZE/1024/1024))M)."
            do_exit 1
        fi
    else
        log_error "The disk $_target_disk not found."
        do_exit 1
    fi

    log_info "The OS will be installed on $TARGET_DISK."
}

function get_rootpart_size()
{
    local _rootpart_size
    local _unit
    local _size
    local old_IFS

    log_info "Begin to get root partition size."
    log_info2 $DIVIDER
    log_info2 "*************************** Root Partition Size *******************************"

    # get root partition size from install.cfg
    log_info2 "Getting root partition size from [$INSTALL_CFG]..."
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^rootpart_size= ]]; then
            _rootpart_size=${param#rootpart_size=}
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    # get root partition size from cmdline
    log_info2 "Getting root partition size from [$CMDLINE]..."
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.rootpart_size= ]]; then
            _rootpart_size=${param#inst.rootpart_size=}
        fi
    done

    # use default root partition size
    if [ -z "$_rootpart_size" ]; then
        log_warn2 "Failed to get root partition size, using default size $((${ROOTFS_SIZE}/1024/1024))M."
        _rootpart_size=${ROOTFS_SIZE}
    fi

    _unit=${_rootpart_size//[0-9]/}
    _size=${_rootpart_size//[!0-9]/}
    case $_unit in
        K | k)
            _rootpart_size=$((${_size}*1024))
            ;;
        M | m)
            _rootpart_size=$((${_size}*1024*1024))
            ;;
        G | g)
            _rootpart_size=$((${_size}*1024*1024*1024))
            ;;
        *)
            _rootpart_size=${_size}
            ;;
    esac

    if [ "$(echo "0$_rootpart_size < 0$ROOTFS_SIZE" | bc)" -eq 1 ]; then
        log_error "The size $(($_rootpart_size/1024/1024))M is less than $(($ROOTFS_SIZE/1024/1024))M."
        do_exit 1
    fi

    _size=$(($_rootpart_size*2+520*1024*1024))
    if [ "$(echo "0$_size > 0$DISK_SIZE" | bc)" -eq 1 ]; then
        log_error "The size ($((_rootpart_size/1024/1024))M * 2 + 520M) is greater than the disk size $(($DISK_SIZE/1024/1024))M."
        do_exit 1
    fi

    ROOTPART_SIZE=$_rootpart_size
    log_info "Root partition size is set to $(($ROOTPART_SIZE/1024/1024))M."
}

function user_setting()
{
    local old_IFS
    local old_IFS2
    declare -A _users

    log_info "Begin to set user."
    log_info2 $DIVIDER
    log_info2 "******************************* User Setting **********************************"

    # get users from install.cfg
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^user= ]]; then
        old_IFS2="$IFS"
        IFS=':' read -r username password is_encrypted <<< "${param#user=}"
        IFS="$old_IFS2"
        _users["$username"]="$password:$is_encrypted"
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    # get users from cmdline
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.user= ]]; then
            old_IFS="$IFS"
            IFS=':' read -r username password is_encrypted <<< "${param#inst.user=}"
            IFS="$old_IFS"
            _users["$username"]="$password:$is_encrypted"
        fi
    done

    mount ${DISKPART_PREFIX}2 $ROOTFS_DST
    check_ret "mount ${DISKPART_PREFIX}2 to $ROOTFS_DST" $?

    for username in ${!_users[@]}; do
        old_IFS="$IFS"
        IFS=':' read -r password is_encrypted <<< "${_users[$username]}"
        IFS="$old_IFS"
        if [[ "$is_encrypted" -eq 0 ]]; then
            # Add user if not root
            if [ "$username" != "root" ]; then
                chroot $ROOTFS_DST /bin/bash -c "useradd -m $username"
                if [ $? -ne 0 ]; then
                    log_warn2 "Failed to add user [$username]."
                else
                    log_info2 "User [$username] added."
                fi
            fi

            # Set password
            chroot $ROOTFS_DST /bin/bash -c "echo $username:$password | chpasswd"
            if [ $? -ne 0 ]; then
                log_warn2 "Failed to set password for [$username]."
            else
                log_info2 "Password set for [$username] successfully."
            fi
        else
            # Add user if not root
            if [ "$username" != "root" ]; then
                chroot $ROOTFS_DST /bin/bash -c "useradd -m $username"
                if [ $? -ne 0 ]; then
                    log_warn2 "Failed to add user [$username]."
                else
                    log_info2 "User [$username] added."
                fi
            fi

            # Write encrypted password to /etc/shadow
            chroot $ROOTFS_DST /bin/bash -c "sed -i 's|^$username:[^:]*|$username:$password|' /etc/shadow"
            if [ $? -ne 0 ]; then
                log_warn2 "Failed to set password for [$username]"
            else
                log_info2 "Password set for [$username] successfully."
            fi
        fi
    done

    umount $ROOTFS_DST
    sync
}

function network_setting()
{
    local old_IFS
    local old_IFS2
    local _network_manager_method
    declare -A _netinfos

    log_info "Begin to set network."
    log_info2 $DIVIDER
    log_info2 "***************************** Network Setting *********************************"

    # get network method from install.cfg
    log_info2 "Getting network method from [$INSTALL_CFG]..."
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^network_method ]]; then
            case ${param#network_method=} in
                n | network)
                    NETWORK_MANAGER_METHOD="network"
                    ;;
                sn | systemd-networkd)
                    NETWORK_MANAGER_METHOD="systemd-networkd"
                    ;;
                nm | networkmanager)
                    NETWORK_MANAGER_METHOD="networkmanager"
                    ;;
                *)
                    log_warn2 "Invalid network manager method [$param] from [$INSTALL_CFG]."
                    ;;
            esac
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    # get network method from cmdline
    log_info2 "Getting network method from [$CMDLINE]..."
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.network_method= ]]; then
            case ${param#inst.network_method=} in
                n | network)
                    NETWORK_MANAGER_METHOD="network"
                    ;;
                sn | systemd-networkd)
                    NETWORK_MANAGER_METHOD="systemd-networkd"
                    ;;
                nm | networkmanager)
                    NETWORK_MANAGER_METHOD="networkmanager"
                    ;;
                *)
                    log_warn2 "Invalid network manager method [$param] from [$CMDLINE]."
                    ;;
            esac
        fi
    done

    if [ -z "$NETWORK_MANAGER_METHOD" ]; then
        log_warn2 "Failed to get network manager method, using default method [$DEFAULT_NETWORK_METHOD]."
        NETWORK_MANAGER_METHOD="$DEFAULT_NETWORK_METHOD"
    else
        log_info2 "Network method set to [$NETWORK_MANAGER_METHOD]."
    fi

    # get network infos from install.cfg
    log_info2 "Getting network info from [$INSTALL_CFG]..."
    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^network= ]]; then
            old_IFS2="$IFS"
            IFS=':' read -r interface bootproto address_netmask gateway dnslist <<< "${param#network=}"
            dnslist=$(echo $dnslist | sed 's/;/ /g')
            check_netinfo "$interface" "$bootproto" "$address_netmask" "$gateway" "$dnslist"
            if [ $? -eq 0 ]; then
                _netinfos["$interface"]="$bootproto:$address_netmask:$gateway:$dnslist"
                log_info2 "Network info [$param] in [$INSTALL_CFG] added."
            else
                log_warn2 "Invalid network info [$param] in [$INSTALL_CFG]."
            fi
            IFS="$old_IFS2"
        fi
    done < $INSTALL_CFG
    old_IFS="$IFS"

    # get network infos from cmdline
    log_info2 "Getting network info from [$CMDLINE]..."
    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.network= ]]; then
            IFS=':' read -r interface bootproto address_netmask gateway dnslist <<< "${param#inst.network=}"
            dnslist=$(echo $dnslist | sed 's/;/ /g')
            check_netinfo "$interface" "$bootproto" "$address_netmask" "$gateway" "$dnslist"
            if [ $? -eq 0 ]; then
                _netinfos["$interface"]="$bootproto:$address_netmask:$gateway:$dnslist"
                log_info2 "Network info [$param] in [$CMDLINE] added."
            else
                log_warn2 "Invalid network info [$param] in [$CMDLINE]."
            fi
        fi
    done
    IFS="$old_IFS"

    mount ${DISKPART_PREFIX}2 $ROOTFS_DST
    check_ret "mount ${DISKPART_PREFIX}2 to $ROOTFS_DST" $?

    index=1
    for interface in ${!_netinfos[@]}; do
        IFS=':' read -r bootproto address_netmask gateway dnslist <<< "${_netinfos[$interface]}"
        case $NETWORK_MANAGER_METHOD in
            network)
                set_network "$interface" "$bootproto" "$address_netmask" "$gateway" "$dnslist"
                ;;
            systemd-networkd)
                set_systemd_networkd "$interface" "$bootproto" "$address_netmask" "$gateway" "$dnslist" "$index"
                index=$(($index+1))
                ;;
            networkmanager)
                set_networkmanager "$interface" "$bootproto" "$address_netmask" "$gateway" "$dnslist"
                ;;
            *)
                log_warn2 "Invalid network manager method [$NETWORK_MANAGER_METHOD]."
                ;;
        esac
    done
    IFS="$old_IFS"

    umount_dir $ROOTFS_DST
    sync
}

function keep_log()
{
    local old_IFS
    local _keeplog

    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^keeplog ]]; then
            _keeplog="true"
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.keeplog ]]; then
            _keeplog="true"
        fi
    done

    if [ "$_keeplog" == "true" ]; then
        mount ${DISKPART_PREFIX}2 $ROOTFS_DST
        cp -arp $LOGFILE $ROOTFS_DST/var/log
        umount_dir $ROOTFS_DST
        sync
    fi
}

function post_install()
{
    local _cmd
    local old_IFS

    old_IFS="$IFS"
    [ -f "$INSTALL_CFG" ] && while IFS= read -r param; do
        if [[ "$param" =~ ^reboot ]]; then
            echo "Begin to reboot to finish installation.."
            reboot -f
        fi
    done < $INSTALL_CFG
    IFS="$old_IFS"

    for param in $(cat $CMDLINE); do
        if [[ "$param" =~ ^inst\.reboot ]]; then
            echo "Begin to reboot to finish installation.."
            reboot -f
        fi
    done

    while true; do
        read -p "Please type 'reboot' to finish installation: " _cmd
        if [ "$_cmd" == "reboot" ]; then
            reboot -f
            if [ $? -ne 0 ]; then
                log_error "Failed to reboot."
            fi
            break
        fi
        echo "$(date +'%F %T') You entered [$_cmd]"
    done
}

function non_interactive_mode()
{
    get_install_cfg
    get_rootfs_location
    get_rootfs_size
    get_target_disk
    get_rootpart_size
    create_partition
    make_filesystem
    install_rootfs
    setup_persist_data
    user_setting
    network_setting
    keep_log
    do_clean
    post_install
}
