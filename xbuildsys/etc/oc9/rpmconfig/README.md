自定义本地YUM源
=================

# 添加RPM包到对应产品的本地yum源

1.目前xuanwan仅支持x86和ARM两种体系结构(其他体系结构需要定制化修改)
```
$ pwd
/root/xuanwan
$ cd etc/rpmconfig
$ ls
oc9-aarch64  oc9-x86_64 example-aarch64 example-x86_64 //将自定义产品增加到这里
$
```

2.etc/rpmconfig当中主要是用于配置本地yum源的RPM包列表, 其来源于配置为
```
$ pwd
/root/xuanwan
$ ls etc/yum.repos.d
oc9.repo   // 这个可以改为厂商自定义的
$
```
