自定义包
=================

# 添加自定义包

## 自定义包编写

1.在packages目录下面创建一个包目录,比如example(可以参考grub2修改)
```
$ pwd
/root/xuanwan
$ cd packages
$ mkdir example // 这里example改成自定义包的名字
$
```

2.在example目录里面添加工程文件
```
$ cd example
$ touch build.rs Cargo.toml example.spec pkg.rs
$
```

3.修改build.rs内容为如下,
```
$ cat build.rs
use std::process::{exit, Command};

fn main() -> Result<(), std::io::Error> {
    let ret = Command::new("txbuild").arg("generate-package").status()?;
    if !ret.success() {
        exit(1);
    }
    Ok(())
}
$
```

4.修改Cargo.toml内容如下,
```
$ cat Cargo.toml
[package]
name = "example"   // example改为对应的自定义包的名字
version = "0.1.0"  // 自定义包的版本号
edition = "2018"   // 这里目前可以不改动
build = "build.rs" // 这里的build.rs可以不改动,如果改动,需要将当前目录下build.rs同步修改

[lib]
path = "pkg.rs"    // 这里同build.rs, 可以不改动
$
```

5.请按照RPM spec规范,修改example.spec
```
$ cat example.spec
%undefine _debugsource_packages

Name: example
Version: 0.0
Release: 1%{?dist}
Summary: example        // 自定义包的来源可以是本地的提交的源码也可以从第三方URL下载

BuildRequires: libxxxx  // libxxxx为依赖包,如果编译容器中不存在,可以到etc/rpmconfig目录找到对应产品添加
%description
%{summary}.

%prep

%build

%install

%files

%changelog
$
```

>注意: pkg.rs可以不用改动,目前是保留项

## 添加到工程

1.将example添加到工程,需要修改顶层Cargo.toml文件
```
$ pwd
/root/xuanwan
$ cd packages
$ cat Cargo.toml
[workspace]
members = [
  "grub2",
  "kernel",
  "libstd-rust",
  "release",
  "example",     // 在members里面增加一项, 这里增加一个叫example的名字
]

[profile.dev]
debug = false
opt-level = 'z'

[profile.dev.build-override]
opt-level = 'z'
$
```

2.更新Cargo工程的保护锁
```
$ pwd
/root/xuanwan/packages
$ cargo update
$
```
此后,工程就已经被成功添加到玄湾.

# 注意事项

```
example被添加到玄湾后,还并不会被编译,需要修改${topdir}/targets/< product > /Cargo.toml文件, 详细请看${topdir}/README.md
```
