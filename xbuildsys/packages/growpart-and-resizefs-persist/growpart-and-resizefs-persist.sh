#!/bin/bash
set -e

part=$(lsblk -lno NAME,MOUNTPOINT | grep persist | awk '{print$1}')
echo $part

disk="/dev/"$(echo ${part} | sed 's/.$//')
num=$(echo ${part: -1})

/usr/bin/growpart -N ${disk} ${num} && /usr/bin/growpart ${disk} ${num} || exit 0

/sbin/resize2fs ${disk}${num} || exit 0
