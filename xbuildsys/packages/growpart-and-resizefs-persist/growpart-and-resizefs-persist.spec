Name: growpart-and-resizefs-persist
Summary:  Script for growing a partition
Requires: gawk
Requires: util-linux cloud-utils-growpart
Version:  1.0
Release: 1%{?dist}
License: GPLv3
URL: https://launchpad.net/cloud-utils/
BuildArch: noarch

SOURCE1: growpart-and-resizefs-persist.service
SOURCE2: 89-growpart-and-resizefs-persist.preset
SOURCE3: growpart-and-resizefs-persist.sh

%description
This package provides the growpart script for growing a partition. It is
primarily used in cloud images in conjunction with the dracut-modules-growroot
package to grow the root partition on first boot.

%prep

%build

%install
mkdir -p %{buildroot}%{_cross_bindir}
install -p -m 0755 %{S:3} %{buildroot}%{_cross_bindir}
mkdir -p %{buildroot}%{_cross_unitdir}/multi-user.target.wants
install -m 0644 %{S:1} %{buildroot}%{_cross_unitdir}
ln -sf %{_cross_unitdir}/growpart-and-resizefs-persist.service %{buildroot}%{_cross_unitdir}/multi-user.target.wants/growpart-and-resizefs-persist.service

install -d %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/
install -p -m 0644 %{S:2} %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/

%files
%{_cross_bindir}/growpart-and-resizefs-persist.sh
%{_cross_unitdir}/growpart-and-resizefs-persist.service
%{_cross_unitdir}/multi-user.target.wants/growpart-and-resizefs-persist.service
%{_cross_rootdir}%{_systemd_util_dir}/system-preset/*

%changelog
