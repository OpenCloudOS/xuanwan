# Disable LTO
%global _lto_cflags %{nil}
%global _configure_gnuconfig_hack 0
# record gnulib commit id
%global gnulibver 9f48fb992a3d7e96610c4ce8be969cff2d61a01b
# as gnulib ignores CFLAGS, have to declare CC
%global cc_force "CC=gcc -fPIE"

%global _libdir %{_exec_prefix}/lib
%global _binaries_in_noarch_packages_terminate_build 0
%global _configure ../configure

%undefine _hardened_build
%undefine _package_note_file
%undefine _missing_build_ids_terminate_build


# start flags customize
# drop cflags from default config
%global drop_cflag_rules \\\
 sed \\\
  -e 's/-O. //g' \\\
  -e 's/-fplugin=annobin//g' \\\
  -e 's,-specs=[[:alnum:]/_-]*annobin[[:alnum:]_-]*,,g' \\\
  -e 's/-fstack-protector[[:alpha:]-]\\+//g' \\\
  -e 's/-Wp,-D_FORTIFY_SOURCE=[[:digit:]]\\+//g' \\\
  -e 's/--param=ssp-buffer-size=4//g'  \\\
  -e 's/-mregparm=3/-mregparm=4/g'  \\\
  -e 's/-fexceptions//g'    \\\
  -e 's/-fcf-protection//g'   \\\
  -e 's/-fasynchronous-unwind-tables//g'  \\\
  -e 's/^/ -fno-strict-aliasing /'  \\\
  %{nil}

%global host_cflags %(echo %{build_cflags} %{?_hardening_cflags} | %{drop_cflag_rules})
%global legacy_host_cflags %(echo %{host_cflags} | sed -e 's/-m64//g' -e 's/-mcpu=power[[:alnum:]]\\+/-mcpu=power6/g')
%global efi_host_cflags %(echo %{host_cflags})

%global target_cflags %(echo %{build_cflags} | %{drop_cflag_rules})
%global legacy_target_cflags %(echo %{target_cflags} | sed -e 's/-m64//g' -e 's/-mcpu=power[[:alnum:]]\\+/-mcpu=power6/g')
%global efi_target_cflags %(echo %{target_cflags})

# drop ldflags from default config
%global drop_ldflags_rules sed -e 's,-specs=[[:alnum:]/_-]*annobin[[:alnum:]_-]*,,g' -e 's/^$//' \\\
%{nil}

%global host_ldflags %(echo %{build_ldflags} %{?_hardening_ldflags} | %{drop_ldflags_rules})
%global legacy_host_ldflags %(echo %{host_ldflags})
%global efi_host_ldflags %(echo %{host_ldflags})

%global target_ldflags %(echo %{build_ldflags} -static | %{drop_ldflags_rules})
%global legacy_target_ldflags %(echo %{target_ldflags})
%global efi_target_ldflags %(echo %{target_ldflags})
# end flags customize

# arch define start
%global with_efi_arch 0
%global with_legacy_arch 0
%global with_emu_arch 0
%global emuarch %{_arch}
%global grubefiarch %{nil}
%global grublegacyarch %{nil}

%ifarch ppc64le
# drop emu, failed to build on ppc64le
%global with_emu_arch 0
%global target_cpu_name %{_arch}
%global legacy_target_cpu_name powerpc
%global legacy_package_arch %{_arch}
%global platform ieee1275
%endif

%global efi_arch x86_64 aarch64 riscv64

%ifarch %{efi_arch}
%global with_efi_arch 1
%else
%global with_efi_arch 0
%endif

%ifarch aarch64 riscv64
%global efi_modules " "
%else
%global efi_modules " backtrace chain tpm usb usbserial_common usbserial_pl2303 usbserial_ftdi usbserial_usbdebug keylayouts at_keyboard connectefi "
%endif

%ifarch x86_64
%global efiarch x64
%global target_cpu_name %{_arch}
%global grub_target_name %{_arch}-efi
%global package_arch efi-x64

%global legacy_target_cpu_name i386
%global legacy_package_arch pc
%global platform pc
%endif

%ifarch aarch64
%global emuarch arm64
%global efiarch aa64
%global target_cpu_name aarch64
%global grub_target_name arm64-efi
%global package_arch efi-aa64
%endif

%ifarch riscv64
%global emuarch riscv64
%global efiarch riscv64
%global target_cpu_name riscv64
%global grub_target_name riscv64-efi
%global package_arch efi-riscv64
%endif


%ifarch %{efi_arch}
%global with_efi_arch 1
%global grubefiname grub%{efiarch}.efi
%global grubeficdname gcd%{efiarch}.efi
%global grubefiarch %{target_cpu_name}-efi
%global with_efi_modules 1
%endif

%ifnarch aarch64 riscv64
%global with_legacy_arch 1
%global grublegacyarch %{legacy_target_cpu_name}-%{platform}
%endif
# arch define end

%global verrel %{epoch}:%{version}-%{release}


Summary: A Multiboot boot loader with support for Linux
Name:  grub2
Epoch: 99
Version: 2.06
Release: 7%{?dist}.custom
License: GPLv3+
URL:  https://mirrors.opencloudos.tech/opencloudos/9.0/BaseOS/source/tree/Packages/grub2-%{version}-7.oc9.src.rpm
Source0: https://ftp.gnu.org/gnu/grub/grub-%{version}.tar.xz
Source1: gnulib-%{gnulibver}.tar.gz
Source2: 99-grub-mkconfig.install
Source3: http://unifoundry.com/pub/unifont/unifont-13.0.06/font-builds/unifont-13.0.06.pcf.gz
Source4: bootstrap
Source5: bootstrap.conf
Source6: 20-grub.install
Source7: grub.patches
Source8: sbat.csv.in
Source9: system-update-set
Source10: system-update-get
Source11: active-standby-switching.patch

%{!?buildsubdir: %global buildsubdir grub-%{version}}

BuildRequires: autoconf automake gcc git
BuildRequires: efi-srpm-macros device-mapper-devel
BuildRequires: flex bison binutils python3
BuildRequires: ncurses-devel xz-devel bzip2-devel
BuildRequires: freetype-devel fuse-devel
BuildRequires: rpm-devel rpm-libs
BuildRequires: freetype-devel gettext-devel
BuildRequires: texinfo help2man
BuildRequires: systemd
BuildRequires: gettext-common-devel

%if 0%{with_legacy_arch}
Requires: grub2-%{legacy_package_arch} = %{verrel}
%else
Requires: grub2-%{package_arch} = %{verrel}
%endif

%global desc \
This package provides the second version of the GRand Unified Bootloader (GRUB), \
which is a highly configurable and customizable bootloader with modular architecture. \
It supports a variety of computer architectures, hardware devices, kernel formats and \
file systems.\
%{nil}

%description
%{desc}

%package -n system-update-tools
Summary: System update tools
BuildArch: noarch

%description -n system-update-tools
System update tools

%package common
Summary: grub2 folder layout
BuildArch: noarch
Conflicts: grubby < 8.40-18
Requires(posttrans): util-linux-core grep coreutils

%description common
This package provides necessary directories which are required by other grub2
subpackages.

%package tools
Summary: Support tools for GRUB.
Requires: grub2-common = %{verrel}
Requires: gettext os-prober file
Requires(pre): dracut grep sed

%description tools
%{desc}
This subpackage provides grub2 tools for support of all platforms, 
such as grub2-install, grub2-mkimage and so on.

%ifarch x86_64
%package tools-efi
Summary: Support tools for GRUB.
Requires: gettext os-prober file
Requires: grub2-common = %{verrel}

%description tools-efi
%{desc}
This subpackage provides grub2 tools for support of x86_64 EFI platforms, 
containing grub2-glue-efi, grub2-render-label and grub2-macbless. 
%endif

%package tools-minimal
Summary: Support tools for GRUB.
Requires: gettext
Requires: grub2-common = %{verrel}

%description tools-minimal
%{desc}
This subpackage provides grub2 tools for support of all platforms, 
such as grub2-set-default, grub-editenv and so on.

%package tools-extra
Summary: Support tools for GRUB.
Requires: gettext os-prober file mtools
Requires: grub2-tools-minimal = %{verrel}
Requires: grub2-common = %{verrel}

%description tools-extra
%{desc}
This subpackage provides grub2 tools for support of all platforms.
such as grub2-fstest, grub2-mkfont and so on.

%if 0%{with_efi_arch}
%package %{package_arch}      
Summary: GRUB for EFI systems.     
Requires: efi-filesystem      
Requires: grub2-common = %{verrel}     
Requires: grub2-tools-minimal >= %{verrel}    
Requires: grub2-tools = %{verrel}     
Provides: grub2-efi = %{verrel}
%ifarch aarch64 riscv64
Provides: grub2 = %{verrel}
%endif
   
%description %{package_arch}      
%{desc}
This subpackage provides layout and configure for %{package_arch} systems.   

%if 0%{?with_efi_modules}     
%package %{package_arch}-modules     
Summary: Modules used to build custom grub.efi images
BuildArch: noarch       
Requires: grub2-common = %{verrel}     
Provides: grub2-efi-modules = %{verrel}    


%description %{package_arch}-modules     
%{desc}         
This subpackage provides Modules support for rebuilding your own grub.efi. 
%endif         


%package %{package_arch}-cdboot      
Summary: Files used to boot removeable media with EFI  
Requires: grub2-common = %{verrel}     
Provides: grub2-efi-cdboot = %{verrel}    

%description %{package_arch}-cdboot     
%{desc}         
This subpackage provides optional components of grub used for booting removeable media on %{package_arch} systems.
%endif


%if 0%{with_legacy_arch}
%package %{legacy_package_arch}
Summary: A legacy boot loader with support for Linux
Provides: grub2 = %{verrel}
Requires: grub2-common = %{verrel}
Requires: grub2-tools-minimal = %{verrel}
Requires: grub2-%{legacy_package_arch}-modules = %{verrel}
Requires: gettext which file
Requires: grub2-tools = %{verrel}
Requires(pre): dracut
Requires(post): dracut

%description %{legacy_package_arch}       
%{desc}         
This subpackage provides support for %{legacy_package_arch} systems.   


%package %{legacy_package_arch}-modules       
Summary: Modules used to build custom legacy grub images  
BuildArch: noarch       
Requires: grub2-common = %{verrel}    

%description %{legacy_package_arch}-modules      
%{desc}        
This subpackage provides support for rebuilding your own grub.efi. 
%endif

%if 0%{with_emu_arch}
%package emu
Summary: GRUB user-space emulation.
Requires: grub2-tools-minimal = %{verrel}

%description emu
%{desc}
This subpackage provides the GRUB user-space emulator of all platforms.

%package emu-modules
Summary: GRUB user-space emulator modules.
Requires: grub2-tools-minimal = %{verrel}

%description emu-modules
%{desc}
This subpackage provides the GRUB user-space emulator modules.
%endif

%prep
pushd %_sourcedir
wget %{URL}
rpm2cpio grub2-%{version}-7.oc9.src.rpm | cpio -divm
popd
%autosetup -N -Sgit_am -n grub-%{version}
rm -fv docs/*.info 
cp %{SOURCE4} bootstrap
cp %{SOURCE5} bootstrap.conf
cp %{SOURCE1} gnulib-%{gnulibver}.tar.gz
tar -zxf gnulib-%{gnulibver}.tar.gz 
mv gnulib-%{gnulibver} gnulib 
git add .
git commit -aqm "modify done"
for patch in `cat %{SOURCE7} | awk '{print $2}'`; do
    /usr/lib/rpm/rpmuncompress %_sourcedir/$patch | /usr/bin/git am --reject -q -p1
done
/usr/lib/rpm/rpmuncompress %{SOURCE11} | /usr/bin/git am --reject -q -p1
rm -f configure
rm -r .git
rm -r build-aux m4
./bootstrap


%if 0%{with_efi_arch}
mkdir grub-%{grubefiarch}-%{version}
cp %{SOURCE3} grub-%{grubefiarch}-%{version}/unifont.pcf.gz
sed -e "s,@@VERSION@@,%{version},g" -e "s,@@VERSION_RELEASE@@,%{version}-%{release},g" \
    %{SOURCE8} > grub-%{grubefiarch}-%{version}/sbat.csv
%endif

%if 0%{with_legacy_arch}
mkdir grub-%{grublegacyarch}-%{version}
cp %{SOURCE3} grub-%{grublegacyarch}-%{version}/unifont.pcf.gz
%endif

%if 0%{with_emu_arch}
mkdir grub-emu-%{version}
cp %{SOURCE3} grub-emu-%{version}/unifont.pcf.gz
%endif

%build
export TARGET_CFLAGS="%{target_cflags}"           \
       TARGET_CPPFLAGS="%{target_cflags}"         \
       TARGET_LDFLAGS="%{target_ldflags}"         \
       TARGET_NM="%{_cross_compile}nm"            \
       TARGET_CC="%{_cross_compile}gcc"           \
       TARGET_CPP="%{_cross_compile}gcc -E"       \
       TARGET_STRIP="%{_cross_compile}strip"      \
       TARGET_OBJCOPY="%{_cross_compile}objcopy"  \
export PATH=$PATH:%{_cross_sysroot}%{_bindir}:%{_cross_sysroot}%{_sbindir}

%if 0%{with_efi_arch}
cd grub-%{grubefiarch}-%{version}
%cross_configure \
 %{cc_force} \
 HOST_CFLAGS="%{efi_host_cflags}" \
 HOST_CPPFLAGS="-I$(pwd)" \
 HOST_LDFLAGS="%{efi_host_ldflags}" \
 TARGET_CFLAGS="%{efi_target_cflags}" \
 TARGET_CPPFLAGS="-I$(pwd)" \
 TARGET_LDFLAGS="%{efi_target_ldflags}" \
 --with-platform=efi \
 --with-utils=host \
 --target=%{_target_platform} \
 --with-grubdir=grub2 \
 --program-transform-name=s,grub,grub2, \
 --disable-werror || ( cat config.log ; exit 1 )
%make_build

GRUB_MODULES=" all_video boot bootswitch blscfg btrfs   \
  cat configfile cryptodisk   \
  echo efi_netfs efifwsetup efinet ext2 f2fs \
  fat font gcry_rijndael gcry_rsa gcry_serpent  \
  gcry_sha256 gcry_twofish gcry_whirlpool    \
  gfxmenu gfxterm gzio    \
  halt hfsplus http increment iso9660 jpeg \
  loadenv loopback linux lvm lsefi lsefimmap luks \
  luks2 mdraid09 mdraid1x minicmd net  \
  normal part_apple part_msdos part_gpt  \
  password_pbkdf2 pgp png read reboot  \
  regexp search search_fs_uuid search_fs_file \
  search_label serial sleep syslinuxcfg test tftp \
  version video xfs zstd "
GRUB_MODULES+=%{efi_modules}

%ifarch %{efi_arch}
./grub-mkimage -O %{grub_target_name} -o %{grubefiname} \
 -p /EFI/%{efi_vendor} -d grub-core ${GRUB_MODULES} \
 --sbat ././sbat.csv

./grub-mkimage -O %{grub_target_name} -o %{grubeficdname} \
 -p /EFI/BOOT -d grub-core ${GRUB_MODULES} \
 --sbat ././sbat.csv

%else
./grub-mkimage -O %{grub_target_name} -o %{grubefiname}  \
 -p /EFI/%{efi_vendor} -d grub-core ${GRUB_MODULES}
./grub-mkimage -O %{grub_target_name} -o %{grubeficdname}  \
 -p /EFI/BOOT -d grub-core ${GRUB_MODULES}
%endif
cd ..
%endif


%if 0%{with_legacy_arch}
cd grub-%{grublegacyarch}-%{version}
%cross_configure       \
 %{cc_force}      \
 HOST_CFLAGS="%{legacy_host_cflags}"   \
 HOST_CPPFLAGS="-I$(pwd)"    \
 HOST_LDFLAGS="%{legacy_host_ldflags}"   \
 TARGET_CFLAGS="%{legacy_target_cflags}"   \
 TARGET_CPPFLAGS="-I$(pwd)"    \
 TARGET_LDFLAGS="%{legacy_target_ldflags}"  \
 --with-platform=%{platform}    \
 --with-utils=host     \
 --target=%{_target_platform}    \
 --with-grubdir=grub2     \
 --program-transform-name=s,grub,grub2,  \
 --disable-werror || ( cat config.log ; exit 1 )
%make_build 
cd ..
%endif


%if 0%{with_emu_arch}
cd grub-emu-%{version}     \
%cross_configure       \
 %{cc_force}      \
 HOST_CFLAGS="%{legacy_host_cflags}"   \
 HOST_CPPFLAGS="-I$(pwd)"    \
 HOST_LDFLAGS="%{legacy_host_ldflags}"   \
 --with-platform=emu     \
 --with-grubdir=grub2     \
 --program-transform-name=s,grub,grub2,  \
 --disable-werror || ( cat config.log ; exit 1 )
%make_build ascii.h widthspec.h
%make_build -C grub-core/lib/gnulib
%make_build -C grub-core
cd ..
%endif


%install
set -e
rm -rf $RPM_BUILD_ROOT

install -d -m 0755 $RPM_BUILD_ROOT%{_datarootdir}/locale/en\@quot \
 $RPM_BUILD_ROOT%{_datarootdir}/locale/en $RPM_BUILD_ROOT%{_infodir}/
install -d -m 0700 ${RPM_BUILD_ROOT}%{efi_esp_dir}/
install -d -m 0700 ${RPM_BUILD_ROOT}/boot/grub2/
install -d -m 0700 ${RPM_BUILD_ROOT}/boot/loader/entries
install -d -m 0700 ${RPM_BUILD_ROOT}%{_sysconfdir}/default
install -d -m 0700 ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/default/grub
ln -sf ../default/grub ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/grub
touch ${RPM_BUILD_ROOT}/boot/grub2/grub.cfg
ln -s ../boot/grub2/grub.cfg ${RPM_BUILD_ROOT}%{_sysconfdir}/grub2.cfg
install -D -m 0755 %{SOURCE9} ${RPM_BUILD_ROOT}%{_bindir}/system-update-set
install -D -m 0755 %{SOURCE10} ${RPM_BUILD_ROOT}%{_bindir}/system-update-get


%if 0%{with_efi_arch}
cd grub-%{grubefiarch}-%{version}
%make_install
find $RPM_BUILD_ROOT -iname "*.module" -exec chmod a-x {} \;
ln -s ../boot/grub2/grub.cfg $RPM_BUILD_ROOT%{_sysconfdir}/grub2-efi.cfg
install -m 700 %{grubefiname} $RPM_BUILD_ROOT%{efi_esp_dir}/%{grubefiname}
install -m 700 %{grubeficdname} $RPM_BUILD_ROOT%{efi_esp_dir}/%{grubeficdname}
${RPM_BUILD_ROOT}/%{_bindir}/grub2-editenv ${RPM_BUILD_ROOT}/boot/grub2/grubenv create
touch grub2-%{package_arch}.conf
echo grub2-%{package_arch} > grub2-%{package_arch}.conf
install -d -m 755 ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
install -m 644 grub2-%{package_arch}.conf ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
rm -f grub2-%{package_arch}.conf
cd ..
%endif

%if 0%{with_legacy_arch}
cd grub-%{grublegacyarch}-%{version}
%make_install
if [ -f $RPM_BUILD_ROOT/%{_libdir}/grub/%{grublegacyarch}/grub2.chrp ]; then 
 mv $RPM_BUILD_ROOT/%{_libdir}/grub/%{grublegacyarch}/grub2.chrp \
    $RPM_BUILD_ROOT/%{_libdir}/grub/%{grublegacyarch}/grub.chrp
fi
if [ 0%{with_efi_arch} -eq 0 ]; then
 ${RPM_BUILD_ROOT}/%{_bindir}/grub2-editenv ${RPM_BUILD_ROOT}/boot/grub2/grubenv create
fi
touch grub2-%{legacy_package_arch}.conf
echo grub2-%{legacy_package_arch} > grub2-%{legacy_package_arch}.conf
install -d -m 755 ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
install -m 644 grub2-%{legacy_package_arch}.conf ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
rm -f grub2-%{legacy_package_arch}.conf
cd ..
%endif

%if 0%{with_emu_arch}
cd grub-emu-%{version}
%make_install -C grub-core
if [ -f $RPM_BUILD_ROOT/%{_libdir}/grub/%{package_arch}/grub2.chrp ]; then
 mv $RPM_BUILD_ROOT/%{_libdir}/grub/%{package_arch}/grub2.chrp
    $RPM_BUILD_ROOT/%{_libdir}/grub/%{package_arch}/grub.chrp
fi
cd ..
%endif

rm -f $RPM_BUILD_ROOT%{_infodir}/dir
ln -s grub2-set-password ${RPM_BUILD_ROOT}/%{_sbindir}/grub2-setpassword
echo '.so man8/grub2-set-password.8' > ${RPM_BUILD_ROOT}/%{_datadir}/man/man8/grub2-setpassword.8

%ifnarch x86_64
rm -vf ${RPM_BUILD_ROOT}/%{_bindir}/grub2-render-label
rm -vf ${RPM_BUILD_ROOT}/%{_sbindir}/grub2-bios-setup
rm -vf ${RPM_BUILD_ROOT}/%{_sbindir}/grub2-macbless
%endif

touch grub2-tools-minimal.conf
echo grub2-tools-minimal > grub2-tools-minimal.conf
install -d -m 755 ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
install -m 644 grub2-tools-minimal.conf ${RPM_BUILD_ROOT}/etc/dnf/protected.d/
rm -f grub2-tools-minimal.conf


%find_lang grub
install -d -m 0755 %{buildroot}%{_prefix}/lib/kernel/install.d/
install -m 0755 %{SOURCE2} %{buildroot}%{_prefix}/lib/kernel/install.d/
install -m 0755 %{SOURCE6} %{buildroot}%{_prefix}/lib/kernel/install.d/
install -d -m 0755 %{buildroot}%{_sysconfdir}/kernel/install.d/

install -d -m 0755 %{buildroot}%{_userunitdir} 
install -m 0755 docs/grub-boot-success.{timer,service} %{buildroot}%{_userunitdir} 
install -d -m 0755 %{buildroot}%{_userunitdir}/timers.target.wants
ln -s ../grub-boot-success.timer %{buildroot}%{_userunitdir}/timers.target.wants

install -d -m 0755 %{buildroot}%{_unitdir} 
install -m 0755 docs/grub-boot-indeterminate.service %{buildroot}%{_unitdir} 
install -d -m 0755 %{buildroot}%{_unitdir}/system-update.target.wants
install -d -m 0755 %{buildroot}%{_unitdir}/reboot.target.wants
ln -s ../grub-boot-indeterminate.service %{buildroot}%{_unitdir}/system-update.target.wants
ln -s ../grub2-systemd-integration.service %{buildroot}%{_unitdir}/reboot.target.wants

# only strip debuginfo of usr/{bin, sbin}
%global finddebugroot "%{_builddir}/%{?buildsubdir}/debug"
%global strip_info RPM_BUILD_ROOT=%{finddebugroot} %{__debug_install_post}
%define __debug_install_post ( \
 mkdir -p %{finddebugroot}/usr  \
 mv ${RPM_BUILD_ROOT}/usr/bin %{finddebugroot}/usr/bin  \
 mv ${RPM_BUILD_ROOT}/usr/sbin %{finddebugroot}/usr/sbin  \
 %{strip_info}  \
 install -m 0755 -d %{buildroot}/usr/lib/ %{buildroot}/usr/src/ \
 cp -al %{finddebugroot}/usr/lib/debug/ %{buildroot}/usr/lib/debug/ \
 cp -al %{finddebugroot}/usr/src/debug/ %{buildroot}/usr/src/debug/ ) \
 mv %{finddebugroot}/usr/bin %{buildroot}/usr/bin  \
 mv %{finddebugroot}/usr/sbin %{buildroot}/usr/sbin  \
 %{nil}

%undefine buildsubdir

%pre tools
if [ -f /boot/grub2/user.cfg ]; then
    if grep -q '^GRUB_PASSWORD=' /boot/grub2/user.cfg ; then
 sed -i 's/^GRUB_PASSWORD=/GRUB2_PASSWORD=/' /boot/grub2/user.cfg
    fi
elif [ -f %{efi_esp_dir}/user.cfg ]; then
    if grep -q '^GRUB_PASSWORD=' %{efi_esp_dir}/user.cfg ; then
 sed -i 's/^GRUB_PASSWORD=/GRUB2_PASSWORD=/' %{efi_esp_dir}/user.cfg
    fi
elif [ -f /etc/grub.d/01_users ] && \
 grep -q '^password_pbkdf2 root' /etc/grub.d/01_users ; then
    if [ -f %{efi_esp_dir}/grub.cfg ]; then

 grep '^password_pbkdf2 root' /etc/grub.d/01_users | \
  sed 's/^password_pbkdf2 root \(.*\)$/GRUB2_PASSWORD=\1/' > %{efi_esp_dir}/user.cfg
    fi
    if [ -f /boot/grub2/grub.cfg ]; then
 install -m 0600 /dev/null /boot/grub2/user.cfg
 chmod 0600 /boot/grub2/user.cfg
 grep '^password_pbkdf2 root' /etc/grub.d/01_users | \
  sed 's/^password_pbkdf2 root \(.*\)$/GRUB2_PASSWORD=\1/' > /boot/grub2/user.cfg
    fi
fi

%posttrans common
set -eu

GRUB_HOME=/boot/grub2
ESP_PATH=/boot/efi

if ! mountpoint -q ${ESP_PATH}; then
    exit 0
fi

if test ! -f %{efi_esp_dir}/grub.cfg; then
    grub2-mkconfig -o %{efi_esp_dir}/grub.cfg
fi

if grep -q "configfile" %{efi_esp_dir}/grub.cfg; then
    exit 0
fi

BOOT_UUID=$(grub2-probe --target=fs_uuid ${GRUB_HOME})
GRUB_DIR=$(grub2-mkrelpath ${GRUB_HOME})

cat << EOF > %{efi_esp_dir}/grub.cfg.stb
search --no-floppy --fs-uuid --set=dev ${BOOT_UUID}
set prefix=(\$dev)${GRUB_DIR}
export \$prefix
configfile \$prefix/grub.cfg
EOF

if test -f %{efi_esp_dir}/grubenv; then
    cp -a %{efi_esp_dir}/grubenv %{efi_esp_dir}/grubenv.rpmsave
    mv --force %{efi_esp_dir}/grubenv ${GRUB_HOME}/grubenv
fi

cp -a %{efi_esp_dir}/grub.cfg %{efi_esp_dir}/grub.cfg.rpmsave
cp -a %{efi_esp_dir}/grub.cfg ${GRUB_HOME}/
mv %{efi_esp_dir}/grub.cfg.stb %{efi_esp_dir}/grub.cfg

%files -n system-update-tools
%{_bindir}/system-update-set
%{_bindir}/system-update-get

%files common -f grub.lang
%license COPYING
%doc THANKS docs/font_char_metrics.png

%dir %{_libdir}/grub/
%attr(0700,root,root) %dir %{_sysconfdir}/grub.d
%{_prefix}/lib/kernel/install.d/20-grub.install
%{_prefix}/lib/kernel/install.d/99-grub-mkconfig.install
%dir %{_datarootdir}/grub
%exclude %{_datarootdir}/grub/*
%attr(0700,root,root) %dir /boot/grub2
%exclude /boot/grub2/*
%attr(0700,root,root) %dir %{efi_esp_dir}
%exclude %{efi_esp_dir}/*
%ghost %config(noreplace) %verify(not size mode md5 mtime) /boot/grub2/grubenv

%files tools-minimal
%{_sbindir}/grub2-get-kernel-settings
%{_sbindir}/grub2-probe
%attr(4755, root, root) %{_sbindir}/grub2-set-bootflag
%{_sbindir}/grub2-set-default
%{_sbindir}/grub2-set*password
%{_bindir}/grub2-editenv
%{_bindir}/grub2-mkpasswd-pbkdf2
%{_bindir}/grub2-mount
%attr(0644,root,root) %config(noreplace) /etc/dnf/protected.d/grub2-tools-minimal.conf

%{_datadir}/man/man3/grub2-get-kernel-settings*
%{_datadir}/man/man8/grub2-set-default*
%{_datadir}/man/man8/grub2-set*password*
%{_datadir}/man/man1/grub2-editenv*
%{_datadir}/man/man1/grub2-mkpasswd-*

%ifarch x86_64
%files tools-efi
%{_bindir}/grub2-glue-efi
%{_bindir}/grub2-render-label
%{_sbindir}/grub2-macbless
%{_datadir}/man/man1/grub2-glue-efi*
%{_datadir}/man/man1/grub2-render-label*
%{_datadir}/man/man8/grub2-macbless*
%endif

%files tools
%attr(0644,root,root) %ghost %config(noreplace) %{_sysconfdir}/default/grub
%config %{_sysconfdir}/grub.d/??_*
%{_sysconfdir}/grub.d/README
%{_userunitdir}/grub-boot-success.timer
%{_userunitdir}/grub-boot-success.service
%{_userunitdir}/timers.target.wants
%{_unitdir}/grub-boot-indeterminate.service
%{_unitdir}/system-update.target.wants
%{_unitdir}/grub2-systemd-integration.service
%{_unitdir}/reboot.target.wants
%{_unitdir}/systemd-logind.service.d
%{_datarootdir}/grub/*
%exclude %{_datarootdir}/grub/*.h
%{_datarootdir}/bash-completion/completions/grub
%{_sbindir}/grub2-install
%{_sbindir}/grub2-mkconfig
%{_sbindir}/grub2-switch-to-blscfg
%{_sbindir}/grub2-rpm-sort
%{_sbindir}/grub2-reboot
%{_bindir}/grub2-file
%{_bindir}/grub2-menulst2cfg
%{_bindir}/grub2-mkimage
%{_bindir}/grub2-mkrelpath
%{_bindir}/grub2-script-check
%{_libexecdir}/grub2
%{_datadir}/man/man?/*

%exclude %{_datadir}/man/man8/grub2-sparc64-setup*
%exclude %{_datadir}/man/man1/grub2-fstest*
%exclude %{_datadir}/man/man1/grub2-glue-efi*
%exclude %{_datadir}/man/man1/grub2-kbdcomp*
%exclude %{_datadir}/man/man1/grub2-mklayout*
%exclude %{_datadir}/man/man1/grub2-mknetdir*
%exclude %{_datadir}/man/man1/grub2-mkrescue*
%exclude %{_datadir}/man/man1/grub2-mkstandalone*
%exclude %{_datadir}/man/man1/grub2-syslinux2cfg*

%exclude %{_datadir}/man/man3/grub2-get-kernel-settings*
%exclude %{_datadir}/man/man8/grub2-set-default*
%exclude %{_datadir}/man/man8/grub2-set*password*
%exclude %{_datadir}/man/man1/grub2-editenv*
%exclude %{_datadir}/man/man1/grub2-mkpasswd-*
%exclude %{_datadir}/man/man8/grub2-macbless*
%exclude %{_datadir}/man/man1/grub2-render-label*

%if %{with_legacy_arch}
%{_sbindir}/grub2-install
%ifarch x86_64
%{_sbindir}/grub2-bios-setup
%else
%exclude %{_sbindir}/grub2-bios-setup
%exclude %{_datadir}/man/man8/grub2-bios-setup*
%endif
%exclude %{_sbindir}/grub2-sparc64-setup
%exclude %{_datadir}/man/man8/grub2-sparc64-setup*
%ifarch ppc64le
%{_sbindir}/grub2-ofpathname
%else
%exclude %{_sbindir}/grub2-ofpathname
%exclude %{_datadir}/man/man8/grub2-ofpathname*
%endif
%endif

%files tools-extra
%{_bindir}/grub2-fstest
%{_bindir}/grub2-kbdcomp
%{_bindir}/grub2-mklayout
%{_bindir}/grub2-mknetdir
%{_bindir}/grub2-mkrescue
%{_bindir}/grub2-mkstandalone
%{_bindir}/grub2-syslinux2cfg
%{_sysconfdir}/sysconfig/grub
%{_datadir}/man/man1/grub2-mkrescue*
%{_datadir}/man/man1/grub2-fstest*
%{_datadir}/man/man1/grub2-kbdcomp*
%{_datadir}/man/man1/grub2-mklayout*
%{_datadir}/man/man1/grub2-mknetdir*
%{_datadir}/man/man1/grub2-mkstandalone*
%{_datadir}/man/man1/grub2-syslinux2cfg*
%exclude %{_bindir}/grub2-glue-efi
%exclude %{_sbindir}/grub2-sparc64-setup
%exclude %{_sbindir}/grub2-ofpathname
%exclude %{_datadir}/man/man1/grub2-glue-efi*
%exclude %{_datadir}/man/man8/grub2-ofpathname*
%exclude %{_datadir}/man/man8/grub2-sparc64-setup*

%if 0%{with_efi_arch}
%files %{package_arch}
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/grub2.cfg
%config(noreplace) %{_sysconfdir}/grub2-efi.cfg
%attr(0700,root,root) %{efi_esp_dir}/%{grubefiname}
%attr(0700,root,root) %dir /boot/loader/entries
%attr(0700,root,root) %ghost %config(noreplace) /boot/grub2/grub.cfg
%attr(0700,root,root) %ghost %config(noreplace) %{efi_esp_dir}/grub.cfg
%config(noreplace) %verify(not size mode md5 mtime) /boot/grub2/grubenv
%attr(0644,root,root) %config(noreplace) /etc/dnf/protected.d/grub2-%{package_arch}.conf


%if 0%{?with_efi_modules}
%files %{package_arch}-modules
%defattr(-,root,root,-)
%dir %{_libdir}/grub/%{grub_target_name}/
%{_libdir}/grub/%{grub_target_name}/*
%exclude %{_libdir}/grub/%{grub_target_name}/*.module
%endif

%files %{package_arch}-cdboot
%defattr(-,root,root,-)
%attr(0700,root,root) %{efi_esp_dir}/%{grubeficdname}
%endif

%if 0%{with_legacy_arch}
%files %{legacy_package_arch}
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/grub2.cfg
%attr(0700,root,root) %ghost %config(noreplace) /boot/grub2/grub.cfg
%attr(0700,root,root) %dir /boot/loader/entries
%attr(0644,root,root) %config(noreplace) /etc/dnf/protected.d/grub2-%{legacy_package_arch}.conf

%files %{legacy_package_arch}-modules  
%defattr(-,root,root)
%dir %{_libdir}/grub/%{grublegacyarch}/
%{_libdir}/grub/%{grublegacyarch}/*
%exclude %{_libdir}/grub/%{grublegacyarch}/*.module
%exclude %{_libdir}/grub/%{grublegacyarch}/{boot,boot_hybrid,cdboot,diskboot,lzma_decompress,pxeboot}.image
%exclude %{_libdir}/grub/%{grublegacyarch}/*.o
%endif

%if 0%{with_emu_arch}
%files emu
%{_bindir}/grub2-emu*
%{_datadir}/man/man1/grub2-emu*

%files emu-modules
%{_libdir}/grub/%{emuarch}-emu/*
%exclude %{_libdir}/grub/%{emuarch}-emu/*.module
%endif

%changelog
* Thu Oct 19 2023 Yi Lin <nilusyi@tencent.com> - 2.06-7
- fix CVE-2023-4692 and CVE-2023-4693
- fix spec format typos

* Wed Oct 11 2023 wynnfeng <wynnfeng@tencent.com> - 2.06-6
- enable secure boot

* Tue Sep 19 2023 jackeyji <jackeyji@tencent.com> - 2.06-5
- fix CVE-2022-2601 and CVE-2022-3775

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.06-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.06-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.06-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jun 20 2022 Yi Lin <nilusyi@tencent.com> 2.06-1
- package init
