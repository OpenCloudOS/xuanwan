#!/bin/bash

NODE_HOSTNAME=""
MASTER_IP=""
MASTER_PORT=""
MASTER_TOKEN=""
CA_HASH=""

K8S_METADATA=/opt/k8s_metadata

function already_in_k8s()
{
   if [ -f /etc/kubernetes/kubelet.conf ]; then
       echo "The node have been already in the k8s."
       rm -rf $K8S_METADATA 
       exit 0
   fi
}

function check_param()
{
    if [ -z "$2" ]; then
        echo "$1 is empty"
	exit 1
    fi
}

function get_init_info()
{
    NODE_HOSTNAME=$(cat $K8S_METADATA/meta-data | awk -F'=' '/NODE_HOSTNAME/{print $2}')
    MASTER_IP=$(cat $K8S_METADATA/meta-data | awk -F'=' '/MASTER_IP/{print $2}')
    MASTER_PORT=$(cat $K8S_METADATA/meta-data | awk -F'=' '/MASTER_PORT/{print $2}')
    MASTER_TOKEN=$(cat $K8S_METADATA/meta-data | awk -F'=' '/MASTER_TOKEN/{print $2}')
    CA_HASH=$(cat $K8S_METADATA/meta-data | awk -F'=' '/CA_HASH/{print $2}')
    
    check_param "NODE_HOSTNAME" $NODE_HOSTNAME
    check_param "MASTER_IP" $MASTER_IP
    check_param "MASTER_PORT" $MASTER_PORT
    check_param "MASTER_TOKEN" $MASTER_TOKEN
    check_param "CA_HASH" $CA_HASH

    hostnamectl set-hostname $NODE_HOSTNAME
}

function join_k8s_cluster()
{
    kubeadm join $MASTER_IP:$MASTER_PORT --token $MASTER_TOKEN --discovery-token-ca-cert-hash $CA_HASH
    if [ $? -ne 0 ]; then
        echo "Failed to join k8s cluster $MASTER_IP:$MASTER_PORT"
	exit 1
    fi
}

function copy_kube_config()
{
    mkdir -p /root/.kube
    cp $K8S_METADATA/config /root/.kube/config -ar
}

get_init_info
already_in_k8s
join_k8s_cluster
copy_kube_config
rm -rf $K8S_METADATA 
