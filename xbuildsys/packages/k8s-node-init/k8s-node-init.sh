#!/bin/bash

NODE_HOSTNAME=""
NETDEV=""
NODE_IP=""
PREFIX=""
GATEWAY=""

K8S_METADATA=/opt/k8s_metadata
function check_param()
{
    if [ -z "$2" ]; then
        echo "$1 is empty"
	exit 1
    fi
}

function mount_cdrom()
{
    for i in `seq 1 10`; do
        mount -t iso9660 /dev/sr0 /media
	if [ $? -eq 0 ]; then
	    break
	fi
    done

    findmnt /media
    if [ $? -ne 0 ]; then
        echo "Failed to mount /dev/sr0"
	exit 1
    fi
    if [ -d "$K8S_METADATA" ]; then
        rm -rf $K8S_METADATA
    fi
    mkdir -p $K8S_METADATA
    cp -ar /media/* $K8S_METADATA
    umount /media
}

function get_init_info()
{
    NODE_HOSTNAME=$(cat $K8S_METADATA/meta-data | awk -F'=' '/NODE_HOSTNAME/{print $2}')
    NETDEV=$(cat $K8S_METADATA/meta-data | awk -F'=' '/NETDEV/{print $2}')
    NODE_IP=$(cat $K8S_METADATA/meta-data | awk -F'=' '/NODE_IP/{print $2}')
    PREFIX=$(cat $K8S_METADATA/meta-data | awk -F'=' '/PREFIX/{print $2}')
    GATEWAY=$(cat $K8S_METADATA/meta-data | awk -F'=' '/GATEWAY/{print $2}')
    
    check_param "NODE_HOSTNAME" $NODE_HOSTNAME
    check_param "NETDEV" $NETDEV
    check_param "NODE_IP" $NODE_IP
    check_param "PREFIX" $PREFIX
    check_param "GATEWAY" $GATEWAY
}

function set_hostname()
{
    local static_hostname=$(cat /etc/hostname)

    if [ x"$static_hostname" != x"$NODE_HOSTNAME" ]; then
        echo "Set /etc/hostname to $NODE_HOSTNAME"
        echo $NODE_HOSTNAME > /etc/hostname
    fi
}

function set_network()
{

    ip link set $NETDEV up
    if [ $? -ne 0 ]; then
        echo "Failed to up $NETDEV"
	exit 1
    fi

    ip addr add $NODE_IP/$PREFIX dev $NETDEV
    ip route add default via $GATEWAY dev $NETDEV
}

mount_cdrom
get_init_info
set_hostname
set_network
