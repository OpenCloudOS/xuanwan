Summary: K8S Node Init
Name: k8s-node-init
Version: 1.0
Release: 1%{?dist}
License: Apache-2.0 OR MIT
Source1: k8s-node-init.service 
Source2: join-k8s.service
Source3: k8s-node-init.sh
Source4: join-k8s.sh
Source5: 89-k8s-node-init.preset

%description
%{summary}.

%prep

%build

%install
install -d %{buildroot}%{_cross_unitdir}/multi-user.target.wants
install -p -m 0644 %{S:1} %{S:2} %{buildroot}%{_cross_unitdir}
ln -sf %{_cross_unitdir}/k8s-node-init.service %{buildroot}%{_cross_unitdir}/multi-user.target.wants/k8s-node-init.service
ln -sf %{_cross_unitdir}/join-k8s.service %{buildroot}%{_cross_unitdir}/multi-user.target.wants/join-k8s.service

install -d %{buildroot}%{_cross_sbindir}
install -p -m 0755 %{S:3} %{S:4} %{buildroot}%{_cross_sbindir}

install -d %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/
install -p -m 0644 %{S:5} %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/

%files
%{_cross_sbindir}/*
%{_cross_unitdir}/*
%dir %{_cross_unitdir}/multi-user.target.wants
%{_cross_unitdir}/multi-user.target.wants/*
%{_cross_rootdir}%{_systemd_util_dir}/system-preset/*

%changelog
