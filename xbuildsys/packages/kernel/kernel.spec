%global debug_package %{nil}
%global kernel_version 6.6.6-2401.0.1

Name: kernel
Epoch: 99
Version: 6.6.6
Release: 2401.0.1%{?dist}.custom
Summary: The Linux kernel
License: GPL-2.0 WITH Linux-syscall-note
URL: https://mirrors.opencloudos.tech/opencloudos/9.0/BaseOS/source/tree/Packages/kernel-%{kernel_version}.oc9.src.rpm
Source100: kernel_config

BuildRequires: bc
BuildRequires: elfutils-devel
BuildRequires: hostname
BuildRequires: kmod
BuildRequires: openssl-devel

# debuginfo
%undefine _unique_debug_names
%undefine _debugsource_packages
%undefine _debuginfo_subpackages
%undefine _enable_debug_packages
%undefine _include_minidebuginfo

%global subversion .oc9.%_arch
%global _find_debuginfo_opts -r
%global debuginfo_dir /usr/lib/debug

%global kernel_sourcedir %{_cross_usrsrc}/kernels

%description
%{summary}.

%package devel
Summary: Configured Linux kernel source for module building
Requires: filesystem

%description devel
%{summary}.

%package modules
Summary: Modules for the Linux kernel

%description modules
%{summary}.

%package headers
Summary: Header files for the Linux kernel for use by glibc

%description headers
%{summary}.

%package debuginfo
Summary: Debug information for package kernel
Requires: kernel-debuginfo-common-%{_target_cpu}
Provides: installonlypkg(kernel-debuginfo)
Provides: kernel-debuginfo = %{version}-%{release}

%description debuginfo
This package provides debug information including
vmlinux, System.map for package kernel

%prep
wget %{URL}
rpm2cpio kernel-%{kernel_version}.oc9.src.rpm | cpio -divm
tar -xf kernel-%{kernel_version}.tar.gz
%setup -TDn kernel-%{kernel_version}
%autopatch -p1

if [ %{_cross_arch} == "aarch64" ];then
    cp arch/%{_cross_karch}/configs/defconfig ../config-%{_cross_arch}
else
    cp arch/%{_cross_karch}/configs/%{_cross_arch}_defconfig ../config-%{_cross_arch}
fi

sed -i -e "s/^CONFIG_BUILD_SALT.*/CONFIG_BUILD_SALT=\"%{version}-%{release}\"/" %{SOURCE100}
KCONFIG_CONFIG="arch/%{_cross_karch}/configs/%{_cross_vendor}_defconfig" \
    ARCH="%{_cross_karch}" \
    scripts/kconfig/merge_config.sh ../config-%{_cross_arch} %{SOURCE100}
rm -f ../config-%{_cross_arch} ../*.patch

sed -i "/^EXTRAVERSION/cEXTRAVERSION = -%{release}" Makefile
sed -i "/^VESION/cVERSION = $(echo %{version} | cut -d '.' -f 1)" Makefile
sed -i "/^PATCHLEVEL/cPATCHLEVEL = $(echo %{version} | cut -d '.' -f 2)" Makefile
sed -i "/^SUBLEVEL/cSUBLEVEL = $(echo %{version} | cut -d '.' -f 3)" Makefile

%global kmake \
make -s\\\
  ARCH="%{_cross_karch}"\\\
  CROSS_COMPILE="%{_cross_compile}"\\\
  INSTALL_HDR_PATH="%{buildroot}%{_cross_prefix}"\\\
  INSTALL_MOD_PATH="%{buildroot}%{_cross_prefix}"\\\
  INSTALL_MOD_STRIP=1\\\
%{nil}

%build
%kmake mrproper
%kmake %{_cross_vendor}_defconfig
%kmake %{?_smp_mflags} %{_cross_kimage}
%kmake %{?_smp_mflags} modules

%global kernel_release "%{version}-%{release}%{subversion}"
%global kernel_libdir %{_cross_libdir}/modules/%{kernel_release}
%install
%kmake headers_install
%kmake modules_install

install -d %{buildroot}/boot
install -T -m 0755 arch/%{_cross_karch}/boot/%{_cross_kimage} %{buildroot}/boot/vmlinuz
install -m 0644 .config %{buildroot}/boot/config
install -m 0644 System.map %{buildroot}/boot/System.map
find %{buildroot}%{_cross_prefix} \
	-name "*.ko" -exec chmod +111 {} \;

find %{buildroot}%{_cross_prefix} \
   \( -name .install -o -name .check -o \
      -name ..install.cmd -o -name ..check.cmd \) -delete

# For out-of-tree kmod builds, we need to support the following targets:
#   make scripts -> make prepare -> make modules
#
# This requires enough of the kernel tree to build host programs under the
# "scripts" and "tools" directories.

# Any existing ELF objects will not work properly if we're cross-compiling for
# a different architecture, so get rid of them to avoid confusing errors.
find arch scripts tools -type f -executable \
  -exec sh -c "head -c4 {} | grep -q ELF && rm {}" \;

# We don't need to include these files.
find -type f \( -name \*.cmd -o -name \*.gitignore \) -delete

# Avoid an OpenSSL dependency by stubbing out options for module signing and
# trusted keyrings, so `sign-file` and `extract-cert` won't be built. External
# kernel modules do not have access to the keys they would need to make use of
# these tools.
sed -i \
  -e 's,$(CONFIG_MODULE_SIG_FORMAT),n,g' \
  -e 's,$(CONFIG_SYSTEM_TRUSTED_KEYRING),n,g' \
  scripts/Makefile

(
  find * \
    -type f \
    \( -name Build\* -o -name Kbuild\* -o -name Kconfig\* -o -name Makefile\* \) \
    -print

  find arch/%{_cross_karch}/ \
    -type f \
    \( -name module.lds -o -name vmlinux.lds.S -o -name Platform -o -name \*.tbl \) \
    -print

  find arch/%{_cross_karch}/{include,lib}/ -type f ! -name \*.o ! -name \*.o.d -print
  echo arch/%{_cross_karch}/kernel/asm-offsets.s
  echo lib/vdso/gettimeofday.c

  for d in \
    arch/%{_cross_karch}/tools \
    arch/%{_cross_karch}/kernel/vdso ; do
    [ -d "${d}" ] && find "${d}/" -type f -print
  done

  find include -type f -print
  find scripts -type f ! -name \*.l ! -name \*.y ! -name \*.o -print

  find tools/{arch/%{_cross_karch},include,objtool,scripts}/ -type f ! -name \*.o -print
  echo tools/build/fixdep.c
  find tools/lib/subcmd -type f -print
  find tools/lib/{ctype,string,str_error_r}.c

  echo kernel/bounds.c
  echo kernel/time/timeconst.bc
  echo security/selinux/include/classmap.h
  echo security/selinux/include/initial_sid_to_string.h

  echo .config
  echo Module.symvers
  echo System.map
) | sort -u > kernel_devel_files

mkdir -p %{buildroot}%{kernel_libdir}
mkdir -p %{buildroot}%{kernel_sourcedir}
mkdir -p %{buildroot}%{kernel_sourcedir}/%{version}-%{release}

tar c -T kernel_devel_files | tar x -C %{buildroot}%{kernel_sourcedir}/%{version}-%{release}
rm -f %{buildroot}%{kernel_libdir}/build %{buildroot}%{kernel_libdir}/source
ln -sf %{_usrsrc}/kernels/%{version}-%{release} %{buildroot}%{kernel_libdir}/build
ln -sf %{_usrsrc}/kernels/%{version}-%{release} %{buildroot}%{kernel_libdir}/source

# debuginfo
%global _find_debuginfo_opts %{_find_debuginfo_opts} --keep-section '.BTF'
%global _find_debuginfo_opts %{_find_debuginfo_opts} -p '.*\/usr\/src\/kernels.*/.*|XXX' -o ignored-debuginfo.list
%global _find_debuginfo_opts %{_find_debuginfo_opts} -p '/.*/%{version}.*|/.*%{version}(\.debug)?' -o debuginfo.list

mkdir -p %{buildroot}%{debuginfo_dir}%{kernel_libdir}
cp -rpf ./vmlinux %{buildroot}%{debuginfo_dir}%{kernel_libdir}/vmlinux

%__debug_install_post
%ifarch aarch64
rm -rf %{buildroot}/usr/src/debug/
%endif
rm -rf %{buildroot}%{debuginfo_dir}/usr/src

%files
%defattr(-,root,root)
%license COPYING LICENSES/preferred/GPL-2.0 LICENSES/exceptions/Linux-syscall-note
/boot/vmlinuz
/boot/config
/boot/System.map

%files modules
%defattr(-,root,root)
%dir %{_cross_libdir}/modules
%{_cross_libdir}/modules/*

%files headers
%defattr(-,root,root)
%dir %{_cross_includedir}/asm
%dir %{_cross_includedir}/asm-generic
%dir %{_cross_includedir}/drm
%dir %{_cross_includedir}/linux
%dir %{_cross_includedir}/misc
%dir %{_cross_includedir}/mtd
%dir %{_cross_includedir}/rdma
%dir %{_cross_includedir}/scsi
%dir %{_cross_includedir}/sound
%dir %{_cross_includedir}/video
%dir %{_cross_includedir}/xen
%{_cross_includedir}/asm/*
%{_cross_includedir}/asm-generic/*
%{_cross_includedir}/drm/*
%{_cross_includedir}/linux/*
%{_cross_includedir}/misc/*
%{_cross_includedir}/mtd/*
%{_cross_includedir}/rdma/*
%{_cross_includedir}/scsi/*
%{_cross_includedir}/sound/*
%{_cross_includedir}/video/*
%{_cross_includedir}/xen/*

%files devel
%defattr(-,root,root)
%dir %{kernel_sourcedir}
%{kernel_sourcedir}/%{version}-%{release}/*
%{kernel_sourcedir}/%{version}-%{release}/.config

# debuginfo
%files debuginfo -f debuginfo.list
%defattr(-,root,root)

%changelog
