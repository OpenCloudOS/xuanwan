%global debug_package %{nil}
%global _dwz_low_mem_die_limit 0

Name: operator
Version: 1.0
Release: 1%{?dist}
Summary: OS auto upgrade operator for xuanwanOS
License: Apache-2.0
#Source0: operator-bin-%{version}.tar.gz
Source1: 89-operator.preset

%description
%{summary}.

%prep
git clone https://gitee.com/OpenCloudOS/xuanwan.git

%build
pushd xuanwan/os-update-operator
make
popd

mv xuanwan/os-update-operator/mybin .
mv xuanwan/os-update-operator/up-executor.service .

%install
install -d %{buildroot}%{_cross_sbindir}
install -p -m 0755 mybin/up-executor %{buildroot}%{_cross_sbindir}
install -p -m 0755 mybin/update.sh %{buildroot}%{_cross_sbindir}

install -d %{buildroot}%{_cross_unitdir}
install -p -m 0644 up-executor.service %{buildroot}%{_cross_unitdir}/up-executor.service

install -d %{buildroot}%{_cross_unitdir}/multi-user.target.wants
ln -sf %{_cross_unitdir}/up-executor.service %{buildroot}%{_cross_unitdir}/multi-user.target.wants/up-executor.service

install -d %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/
install -p -m 0644 %{S:1} %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/

%files
%{_cross_sbindir}/up-executor
%{_cross_sbindir}/update.sh

%{_cross_unitdir}/up-executor.service

%dir %{_cross_unitdir}/multi-user.target.wants
%{_cross_unitdir}/multi-user.target.wants/*
%{_cross_rootdir}%{_systemd_util_dir}/system-preset/*

%changelog
