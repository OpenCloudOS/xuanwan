%global debug_package %{nil}
%global _dwz_low_mem_die_limit 0
#bcond_without local_compile

Summary: OS auto upgrade operator written in rust for xuanwanOS
Name: os-update-operator
Version: 1.0
Release: 1%{?dist}
License: Apache-2.0
Source1: 89-operator.preset
%if %{with local_compile}
Source2: os-update-operator.tar.gz
%endif

# BuildRequires: cargo >= 1.80.0 rust
# BuildRequires: protobuf-compiler

%description
%{summary}.

%prep
%if %{with local_compile}
mkdir -p xuanwan
pushd xuanwan/
tar -zxf %{SOURCE2}
popd
%else
git clone https://gitee.com/OpenCloudOS/xuanwan.git
%endif

%build
pushd xuanwan/os-update-operator
# only build the bin we need
cargo build --release --bin up-executor
# cargo build --release
popd

mv xuanwan/os-update-operator/tools .
mv xuanwan/os-update-operator/target/release/up-executor .
mv xuanwan/os-update-operator/configs/systemd/up-executor.service .

%install
install -d %{buildroot}%{_cross_sbindir}
install -p -m 0755 up-executor %{buildroot}%{_cross_sbindir}
install -p -m 0755 tools/update.sh %{buildroot}%{_cross_sbindir}

install -d %{buildroot}%{_cross_unitdir}
install -p -m 0644 up-executor.service %{buildroot}%{_cross_unitdir}/up-executor.service

install -d %{buildroot}%{_cross_unitdir}/multi-user.target.wants
ln -sf %{_cross_unitdir}/up-executor.service %{buildroot}%{_cross_unitdir}/multi-user.target.wants/up-executor.service

install -d %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/
install -p -m 0644 %{S:1} %{buildroot}%{_cross_rootdir}%{_systemd_util_dir}/system-preset/

%files
%{_cross_sbindir}/up-executor
%{_cross_sbindir}/update.sh

%{_cross_unitdir}/up-executor.service

%dir %{_cross_unitdir}/multi-user.target.wants
%{_cross_unitdir}/multi-user.target.wants/*
%{_cross_rootdir}%{_systemd_util_dir}/system-preset/*

%changelog
