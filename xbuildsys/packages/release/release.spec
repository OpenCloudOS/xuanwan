%global _cross_first_party 1

Name: release
Version: 0.0
Release: 0%{?dist}
Summary: OS release
License: Apache-2.0 OR MIT

Source97: release-sysctl.conf

Requires: ca-certificates
Requires: coreutils
Requires: e2fsprogs
Requires: libgcc
Requires: filesystem
Requires: glibc
Requires: iproute
Requires: chrony
Requires: systemd
Requires: systemd-libs
Requires: systemd-udev
Requires: systemd-networkd
%ifarch x86_64
Requires: grub2-pc
Requires: grub2-pc-modules
%endif
%ifarch aarch64
Requires: grub2-common
Requires: grub2-efi-aa64
Requires: shim
%endif

%description
%{summary}.

%prep

%build

%install
install -d %{buildroot}%{_cross_sysctldir}
install -p -m 0644 %{S:97} %{buildroot}%{_cross_sysctldir}/80-release.conf
%files
%{_cross_sysctldir}/80-release.conf

%changelog
