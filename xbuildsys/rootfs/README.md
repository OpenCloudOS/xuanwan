自定义产品
=================

# 添加自定义产品配置

1.在 rootfs 目录下面创建一个玄湾产品,比如叫 example (可以参考oc9修改)
```
$ pwd
/root/xuanwan
$ cd rootfs
$ mkdir example   // 这里example改成自定义产品的名字
$
```

2.在example目录里面添加 rootfs/example/etc/* 配置文件(可以参考oc9配置)
```
$ mkdir example/etc
$ cd  example/etc
$ #添加所需配置文件
$ 
```

3. 如果有 rootfs/example/etc/os-release 文件，注意修改内容:
```
$ cat rootfs/example/etc/os-release
NAME="OpenCloudOS"
VERSION="9.0"
ID="opencloudos"
ID_LIKE="opencloudos"
VERSION_ID="9.0"
PLATFORM_ID="platform:oc9.0"
PRETTY_NAME="OpenCloudOS 9.0"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:opencloudos:opencloudos:9.0"
HOME_URL="https://www.opencloudos.org/"
BUG_REPORT_URL="https://bugs.opencloudos.tech/"

$
```

此后, rootfs配置文件就已经被成功添加到玄湾.
