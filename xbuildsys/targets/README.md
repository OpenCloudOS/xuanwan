自定义产品
=================

# 添加自定义产品

## 自定义产品编写

1.在targets目录下面创建一个玄湾产品,比如叫example(可以参考oc9修改)
```
$ pwd
/root/xuanwan
$ cd targets
$ mkdir example   // 这里example改成自定义产品的名字
$
```

2.在example目录里面添加工程文件
```
$ cd example
$ touch build.rs Cargo.toml lib.rs
$
```

3.修改build.rs内容为如下,
```
$ cat build.rs
use std::process::{exit, Command};

fn main() -> Result<(), std::io::Error> {
    let ret = Command::new("txbuild").arg("generate-target").status()?;
    if !ret.success() {
        exit(1);
    }
    Ok(())
}
$
```

4.修改Cargo.toml内容如下,
```
$ cat Cargo.toml
[package]
build = "build.rs" // build.rs可以不修改,如果需要修改,需要同步修改build.rs文件
edition = "2018"
name = "example"   // 产品名字, 这里是example
version = "1.0.0" # cargo package version
# Don't rebuild crate just because of changes to README.
exclude = ["README.md"]

[package.metadata.build-target]
image-size="4G" // 设定最终生成的raw镜像磁盘大小,示例是4G
kernel-version="6.6.6-2401.0.1" // 使用的内核版本号

// included-kernels 这里只填写内核包在packages目录下面的名字
included-kernels = [
  "kernel",  //内核包名字
]

// included-packages这里填写需要安装到镜像的包名字
included-packages = [
  "kernel",   // 填写kernel,意味着在镜像生成的时候,会执行rpm -ivh kernel-xxxxx
  "kernel-modules",
  # 核心组件, 这些应用包可以是packages目录下面的包名,也可以来自yum仓库
  "bind-utils",
  "example", // 如果在packages自定义了一个example的包,并需要安装到镜像,需要在这里添加包名
  "cronie",
  "curl",
  "e2fsprogs",
  "findutils",
  "glibc-common",
  "grub2",
  "grub2-pc",
  "grub2-pc-modules",
  "iproute",
  "iproute-tc",
  "iptables",
  "lsof",
  "policycoreutils",
  "procps",
  "release",
  "rpm",
  "rsyslog",
  "selinux-policy",
  "selinux-policy-targeted",
  "system-update-tools",
  "strace",
  "tar",
  "tcpdump",
  "telnet",
  "util-linux",
  "vim-minimal",
  "which",  // 如果将which去掉,那么容器镜像中将不会包含which命令,可以根据需求增删应用包
]

[lib]
path = "lib.rs"
$
```

>注意: lib.rs可以不用改动,目前是保留项

## 更新Cargo工程的保护锁
```
$ pwd
/root/xuanwan/targets/example/
$ cargo update
$
```
此后,工程就已经被成功添加到玄湾.

# 注意事项

产品中需要的应用包主要有2个来源,

1.来自于packages/目录下的自定义应用包
```
$ pwd
/root/xuanwan/packages
$ ls
grub2 kernel libstd-rust release
$
```

2.来自于本地yum源,本地yum源当中的rpm包列表如下
```
$ pwd
/root/xuanwan/
$ cat etc/rpmconfig/example-x86_64  // example为产品名字, x86-64表示平台
glibc
glibc-common
libgcc
glibc-devel
systemd
systemd-udev
systemd-devel
systemd-libs
systemd-networkd
systemd-container
bash
sed
gawk
tree
tcpdump
readline
coreutils
e2fsprogs
net-tools
iproute
iptables
grep
iputils
libseccomp
libseccomp-devel
ncurses
util-linux
conntrack-tools
chrony
strace
procps
shadow-utils
acpid
nfs-utils
rsyslog
tar
passwd
telnet
hdparm
ethtool
wget
xz
lsof
bzip2
curl
acl
traceroute
cronie
psmisc
bind-utils
ipset
ipvsadm
iproute-tc
elfutils-devel
openssl-devel
bc
hostname
rpm-devel
flex
bison
gettext-devel
fuse-devel
fuse
libxml2-devel
libcurl-devel
nspr
nss-softokn-freebl
vim-minimal
libcap-devel
binutils-devel
elfutils-libelf-devel
xfsprogs
lvm2
runc
containerd
policycoreutils
selinux-policy
selinux-policy-targeted
which
autoconf
automake
bzip2-devel
dejavu-sans-fonts
device-mapper-devel
efi-srpm-macros
freetype-devel
gcc
git
help2man
ncurses-devel
texinfo    // packages目录下面的依赖包,也来自于这里,如果需要对应的包, 请添加到这里
gettext-common-devel // 可以增删这里的包来控制本地yum源的RPM数量
$
```
即本地yum源的rpm包是受限的.

3.本地yum源的RPM包,来自于etc/yum.repos.d, 通过修改这里, 即可改变上游yum源位置(后期这里会调整为产品相关).
